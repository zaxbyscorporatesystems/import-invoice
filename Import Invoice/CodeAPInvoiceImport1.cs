﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Import_Invoice
{
    class CodeAPInvoiceImport1
    {

        public int DoImportStep1()
        {

            /* Improvements to be done:

1.  Include ZFI item code as requirement for updates(?)
2.  Do not flag errors for following: **
	- Missing apvcust records - WARNING
	- Missing vendor code in Epicor AP - WARNING
	- Unbalanced transaction detail with header total - WARNING/ERROR?
	- Missing details when invoice amount is 0.00 - WARNING
	** Add warnings to error table AND remove these warning records when problem is addressed (such as suspense items)
3.  Incorporate test invoices by flagging as such
4.  Incorporate QCD invoices and vendors - Multiple distribution systems (Vendors = 'QCD_GROUP')
5.  Incorporate a system for including regular order guides with the item list setups
	- Notify Purchasing of items requiring ZFI item codes
6.  Do not bring ZAX company invoices into Epicor ON hold
7.  Do not store the import file name in the apinvhdr table; the ID instead
8.  Remove apinvhdr.status_comment AND apinvhdr.import_comment fields; make rec_status AND status_flag more meaningful
9.  Verify who receives the e-mail reports/notices for this process

WARNINGS:	apinvhdr.rec_status = 3
----------------------------------------------------------------------
-99	DBMEMO	COMPANY	Company not set up yet.	`	-- Just set this as an "INVOICE"
-99	INVOICE	COMPANY	Company not set up yet.
-96	INVOICE	CUSTOMER	Customer record '217886' does not exist in apvcust table.
-91	INVOICE	VENDOR	Vendor Code missing from company vendor table (or company was missing).
-98	INVOICE	DTL ACCOUNT	Test item from vendor; not valid
-97	INVOICE	DETAILREC	There are no details for this header record.

SUSPENSE ITEMS (No need for an error record for this)

-93	INVOICE	NULL	INV TOTAL	Invoice header total does not equal detail total.

ERRORS:		apinvhdr.rec_status = -99
----------------------------------------------------------------------
-94	INVOICE	INV LINES	Invoice detail line count does match summary validation line.

-98	INVOICE	DTL ACCOUNT	Test item from vendor; not valid
-97	INVOICE	DETAILREC	There are no details for this header recorD.
-97	INVOICE	DETAILREC	There are not details for this header recorD.
-96	INVOICE	CUSTOMER	Customer record '217886' does not exist in apvcust table.
-96	INVOICE	CUSTOMER	Customer record '30231' does not exist in apvcust table.
-96	INVOICE	CUSTOMER	Customer record does not exist in apvcust table.
-95	INVOICE	INV DET	No Detail Records for Invoice OR detail has no prices.
-94	INVOICE	INV LINES	Invoice detail line count does match summary validation line.
-93	INVOICE	INV TOTAL	Invoice header total does not equal detail total.

rec_status, import_flag
----------------------------------------------------------------------
-99	VALIDATION	Record failed validation testing and involved an error
1	VALIDATION	Record sent to import system
2	VALIDATION	Record for outside accounting and not sent to import system
3	VALIDATION	Record has warnings and is pending import to production system
4	VALIDATION	Record already exists in production system
5	VALIDATION	Record is for testing only
6	VALIDATION	Record is ignored because it is a zero dollar transaction

7	VALIDATION	Zero dollar transaction pending final record updates
8	VALIDATION	Zero dollar transaction with warnings

-99	IMPORT	Record failed import to production system
1	IMPORT	Record imported to production system
2	IMPORT	Record for outside accounting and not imported to production system
3	IMPORT	Record pending import to production system
4	IMPORT	Record already exists in production system
5	IMPORT	Record is for testing only

USE matrixdev
DROP PROCEDURE [apinvimp1_sp]

*/

//ALTER PROCEDURE [dbo].[apinvimp1_sp]

//AS

/**********************************************************************************************************************************************************
*
*	Name:			apinvimp1_sp - Load File Data from Temp Table into Permanent Tables
*	Author:			PJohnson
*	Date:			11/03/2005
*	Updated:		2/12/2014 (Ver. 5 for QCD/PFG/multiple distribution companies)
*	Description:	AP Invoice Imports,Procedure #1.
*					This procedure will load production tables apinvhdr and apinvdet from the
*					temp table ftp_tmp that contains the raw data from the vendors' invoice files.
*					These files were loaded into ftp_tmp via an ActiveX script in a SQL DTS package.
*
*	Parameters:		<No parameters>
*
*	Example:		EXEC @return_status = apinvimp1_sp
*
*	USE matrixdev DROP PROCEDURE apinvimp1_sp
***********************************************************************************************************************************************************/

//SET NOCOUNT ON

//DECLARE 
//    @Database VARCHAR(50),
//    @rec_count INT,
//    @DebugFlag TINYINT
            int rslt=0;
string database="matrixdev",
    sTemp=string.Empty;
            int rec_count=0;
            StringBuilder sbPrint=new StringBuilder();
            bool debugFlag=false;
//SET @DebugFlag = 0

//IF @DebugFlag = 1
//    BEGIN
//        PRINT 'BEGIN apinvimp1_sp.'
//        PRINT 'Time:  ' + CONVERT(VARCHAR(25), GETDATE(), 121)
//    END
        if(debugFlag)
        {
            sbPrint.Append("BEGIN apinvimp1_sp.");
            sbPrint.Append(string.Format("Time:  {0}",DateTime.Now));
        }
//INSERT IntegrationStaging.dbo._AdminProcessRunData (	-- SELECT * FROM IntegrationStaging.dbo._AdminProcessRunData
//    ServerName,ProcessName,ProcessDescription,ProcessStepID,ProcessStepName,ProcessStepDescription,
//    ProcessServerName,StepSourceServer,StepDestinationServer,ProcessStart,ProcessEnd,StepRowCount,ProcessStartOrEndStep,CurrentDateTime)
//SELECT @@SERVERNAME,'apinvimp1_sp','Invoice import step 1 - read file data from temp table',1,'Procedure start',NULL,
//    'HANSOLO','HANSOLO','HANSOLO',GETDATE(),NULL,NULL,'S',GETDATE()


//SET @Database = 'matrixdev'	-- database for processing
using(CodeSQL clsSql=new CodeSQL())
{
    
//IF (SELECT DB_NAME()) <> @Database
//    BEGIN
//        IF @DebugFlag = 1
//            BEGIN
//                PRINT 'Database should be ''' + @database + '''.  It is: ' + DB_NAME()
//                SELECT 'Database should be ''' + @database + '''.  It is: ' + DB_NAME()
//            END
//        RETURN -1
//    END
    DataSet dsImportFiles=new DataSet();
    StringBuilder sbSql=new StringBuilder();
     sbSql.Append("SELECT import_file_name FROM ftp_files WHERE file_status = 0 ");
     sbSql.Append("AND file_import_date IS NULL");
     sbSql.Append("AND import_type = 'APINVOICE' AND import_source IN ('PFG_GROUP','QCD_GROUP','MBROS')");
     //sbSql.Append("AND NOT EXISTS (SELECT field1 FROM ftp_tmp)");
    dsImportFiles=clsSql.CmdDataset(Properties.Settings.Default.conDev,sbSql.ToString());
    if(dsImportFiles.Tables.Count==0 && dsImportFiles.Tables[0].Rows.Count==0)
    {
        sbPrint.Append("No files listed in ftp_files table or no records in temporary record table ftp_tmp.  Moving on to next step.");
//IF NOT EXISTS
//        (SELECT import_file_name FROM ftp_files WHERE file_status = 0 
//            AND file_import_date IS NULL
//            AND import_type = 'APINVOICE' AND import_source IN ('PFG_GROUP','QCD_GROUP','MBROS'))
//        AND NOT EXISTS (SELECT field1 FROM ftp_tmp)
//    BEGIN
//        IF @DebugFlag = 1
//            BEGIN
//                PRINT 'No files listed in ftp_files table or no records in temporary record table ftp_tmp.  Moving on to next step.'
//                SELECT 'No files listed in ftp_files table or no records in temporary record table ftp_tmp.  Moving on to next step.'
//            END
//        RETURN 1
//    END
    }
        if(debugFlag)
        {
            int.TryParse(clsSql.CmdScalar(Properties.Settings.Default.conDev,"SELECT count(*) FROM ftp_tmp").ToString(),out rec_count);
            sbPrint.Append(string.Format("Total records found to process: {0}",rec_count));
//IF @DebugFlag = 1
//    BEGIN
//        SELECT @rec_count = count(*) FROM ftp_tmp	-- SELECT * FROM ftp_tmp
//        PRINT 'Total records found to process: ' + CAST(@rec_count as VARCHAR)
//    END
        }
        sTemp="DELETE FROM apinvhdr WHERE invoice_num IS NULL AND customer_num IS NULL AND date_invoice IS NULL AND invoice_amt IS NULL";   //	--AND import_file_id IS NULL 
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sTemp);
        sTemp="DELETE FROM apinvdet WHERE invoice_num IS NULL AND customer_num IS NULL AND vend_supplier_id IS NULL AND item_number IS NULL";   //	--AND import_file_id IS NULL 
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sTemp);
    
    sbSql.Clear();
    sbSql.Append("CREATE TABLE ##InvoiceHeader (rec_id INT,record_type VARCHAR(1),vend_supplier_id VARCHAR(5),vend_customer_num VARCHAR(15),customer_po VARCHAR(15),");
    sbSql.Append("debit_or_credit VARCHAR(1),date_added CHAR(8),added_by VARCHAR(10),invoice_num VARCHAR(7),date_delivered CHAR(8),");
    sbSql.Append("date_invoice CHAR(8),invoice_amt VARCHAR(11),invoice_tax VARCHAR(11),invoice_freight VARCHAR(11),credit_apply_to_num VARCHAR(8),");
    sbSql.Append("record_end_char VARCHAR(1),import_file_id INT,import_source VARCHAR(10),status_flag SMALLINT)");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());

    sbSql.Clear();
    sbSql.Append("CREATE TABLE ##InvoiceDetail (rec_id INT,record_type VARCHAR(1),invoice_num VARCHAR(7),sequence_id INT,vend_customer_num VARCHAR(15),vend_supplier_id VARCHAR(5),item_number VARCHAR(6),");
    sbSql.Append("item_desc VARCHAR(80),account_code VARCHAR(32),quantity DECIMAL(16,4),uom VARCHAR(2),price VARCHAR(11),item_tax VARCHAR(11),item_tax_percent VARCHAR(11),");
    sbSql.Append("custom_definitions VARCHAR(162),d_record_end_char VARCHAR(1),import_file_id INT,import_source VARCHAR(10),status_flag SMALLINT)");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());

    sbSql.Clear();
    sbSql.Append("CREATE TABLE ##InvoiceSummary (rec_id INT,record_type VARCHAR(1),invoice_num VARCHAR(7),vend_customer_num VARCHAR(15),vend_supplier_id VARCHAR(5),");
    sbSql.Append("total_invoice_lines VARCHAR(7),s_record_end_char VARCHAR(1),import_file_id INT,import_source VARCHAR(10))");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());

    string error_dir=string.Empty,
        import_source=string.Empty,
        file_path=string.Empty,
        import_file_name=string.Empty,
        error_filename=string.Empty,
        field1=string.Empty,
        message1=string.Empty,
		invoice_num =string.Empty,
		vend_customer_num =string.Empty,
		credit_apply_to_num =string.Empty,
		vend_supplier_id =string.Empty,
		record_type =string.Empty,
		total_invoice_lines =string.Empty,
		s_record_end_char =string.Empty,
        sql =string.Empty,
		cmd =string.Empty,
        process_dir=string.Empty;
    DateTime now_dt;
    int import_file_id =0,
		file_status =0,
		error_code =0,
		rec_id =0,
		PositionShift =0,
		seq_id =0,
		rowcount1 =0,
		rowcount2 =0;

    process_dir = @"c:\Invoices\";	//-- '\\metaframe\d$\file_process\pfg_invoices\process\'
    error_dir = @"c:\Invoices\";		//-- '\\metaframe\d$\file_process\pfg_invoices\errors\'
    now_dt = DateTime.Now;

    sbSql.Clear();
    sbSql.Append("UPDATE A");
    sbSql.Append("SET A.import_file_id = B.import_file_id");
    sbSql.Append("FROM ftp_tmp A");
    sbSql.Append("INNER JOIN ftp_files B ON B.import_file_name = A.import_file_name");
    sbSql.Append("WHERE A.import_file_id IS NULL AND B.import_source IN ('PFG_GROUP','QCD_GROUP','MBROS')");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());
    

    sbSql.Clear();
    sbSql.Append("SELECT import_file_id,");
    sbSql.Append("import_source,");
    sbSql.Append("file_path,");
    sbSql.Append("import_file_name,");
    sbSql.Append("file_status");
    sbSql.Append("FROM ftp_files WHERE file_status = 0");
    sbSql.Append("AND file_import_date IS NULL");
    sbSql.Append("AND import_type = 'APINVOICE'");
    sbSql.Append("AND import_source IN ('PFG_GROUP','QCD_GROUP','MBROS')");
    sbSql.Append("ORDER BY import_file_id");

    dsImportFiles=clsSql.CmdDataset(Properties.Settings.Default.conDev,sbSql.ToString());
//DECLARE FileCursor CURSOR FOR		--  SELECT * FROM ftp_files ORDER BY import_file_id desc
//    SELECT import_file_id,
//        import_source,
//        file_path,
//        import_file_name,
//        file_status
//    FROM ftp_files WHERE file_status = 0
//        AND file_import_date IS NULL
//        AND import_type = 'APINVOICE'
//        AND import_source IN ('PFG_GROUP','QCD_GROUP','MBROS')
//    ORDER BY import_file_id

//OPEN FileCursor


//FETCH NEXT FROM FileCursor INTO
//    @import_file_id,
//    @import_source,
//    @file_path,
//    @import_file_name,
//    @file_status
    foreach(DataRow drImportFile in dsImportFiles.Tables[0].Rows)
    {
//WHILE @@FETCH_STATUS = 0
//    BEGIN		-- File Process Loop
        import_source=drImportFile["import_source"].ToString().Trim();
		//IF RIGHT(@process_dir,1) <> '\' SET @process_dir = @process_dir + '\'
           process_dir+= (process_dir.EndsWith(@"\")) ? string.Empty:@"\";
        //IF @DebugFlag = 1 PRINT 'Perform file name/ID loop for invoices. Current file: ' + @import_file_name + ''
        //    if(debugFlag==1)
        //    {
                sbPrint.Append(string.Format("Perform file name/ID loop for invoices. Current file: {0}",drImportFile["import_file_name"]));
		//-- See if file has been already run (status = 1) or incompletely processed in a previous loop (status = -1)
        sTemp=string.Format("SELECT 1 FROM ftp_files WHERE import_file_name = '{0}'  AND import_source = '{1}' AND import_file_id <> '{2}' AND file_status in (-1,1)",drImportFile["import_file_name"],drImportFile["import_source"],drImportFile["import_file_id"]);
        DataSet dsTemp=clsSql.CmdDataset(Properties.Settings.Default.conDev,sTemp);
        if(dsTemp.Tables.Count>0 && dsTemp.Tables[0].Rows.Count>0)
        {
        //IF EXISTS(SELECT 1 FROM ftp_files WHERE import_file_name = @import_file_name AND import_source = @import_source AND import_file_id <> @import_file_id AND file_status in (-1,1))
        //    BEGIN
				error_code = 8888;
            sbSql.Clear();
            sbSql.Append("UPDATE ftp_files");
            sbSql.Append("SET file_status = -98,");
            sbSql.Append(string.Format("file_process_error = '{0}'",error_code));
            sbSql.Append(string.Format("file_import_date ='{0}'", now_dt));
            sbSql.Append(string.Format("WHERE import_file_id ='{0}'", drImportFile["import_file_id"]));
            error_filename = drImportFile["import_file_name"].ToString().Trim();
				error_filename=error_filename.Substring(0,error_filename.LastIndexOf("."));
            error_filename+=string.Format("_{0}{1}{2}.err",now_dt.Year,now_dt.Month,now_dt.Date);

            //SET @error_filename = LEFT(@import_file_name,(LEN(@import_file_name) - 4)) + '_' + CONVERT(VARCHAR(10),GETDATE(),112) + '.err'
            sbPrint.Append(string.Format("The following file {0} was already processed.  Skip and move it to the error directory.",drImportFile["import_file_name"])); 
           string fyleFrom=string.Format("{0}{1}",process_dir,drImportFile["import_file_name"]),
               fyleTo=string.Format("{0}{1}",error_dir,error_filename);
            File.Move(fyleFrom,fyleTo);
                //PRINT 'The following file was already processeD.  Skip and move it to the error directory: ' + @import_file_name
                //SET @cmd = 'move ' + @process_dir + @import_file_name + ' ' + @error_dir + @error_filename + ''
                //EXEC master..xp_cmdshell @cmd,no_output

            message1 = "The file named " + import_file_name + " had already been processed. Please check contents of file to determine if it is not a duplicate and then rename it to something more unique. A copy of the file can now be found here: //" + error_dir + error_filename;
            sbSql.Clear();
            sbSql.Append("EXEC master..xp_sendmail @recipients = 'pjohnson@zaxbys.com;cstolz@zaxbys.com;dcarter@zaxbys.com',");
            sbSql.Append("@subject = 'Error(s): Invoice File Duplication',");
            sbSql.Append(string.Format("@message = {0}",message1));
            clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
//                FETCH NEXT FROM FileCursor INTO
//                    @import_file_id,
//                    @import_source,
//                    @file_path,
//                    @import_file_name,
//                    @file_status

//                CONTINUE
//            END
        }
		else
        {

            //BEGIN	-- If file was not processed already and is ready to be processeD.
            //        -- Update the file table to set the record to -1, In Process:
            sbSql.Clear();
            sbSql.Append("UPDATE ftp_files");
            sbSql.Append("SET file_status = -1,");
            sbSql.Append(string.Format("file_import_date = '{0}'",now_dt));
            sbSql.Append(string.Format("WHERE import_file_id = {0}", drImportFile["import_file_id"]));
            clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());

            sbSql.Clear();
            sbSql.Append("SELECT rec_id,field1 FROM ftp_tmp");
            sbSql.Append(string.Format(" WHERE import_file_id = {0}", drImportFile["import_file_id"]));
            sbSql.Append("AND ISNULL(field1,'') <> ''");

            DataSet dsDatas=clsSql.CmdDataset(Properties.Settings.Default.conDev,sbSql.ToString());
            
            foreach(DataRow drDataLyne in dsDatas.Tables[0].Rows)
            {
                //DECLARE RecordCursor CURSOR FOR
                //    		-- SELECT * FROM ftp_tmp
                //   
                //        			-- Added for Ver 4
                //    ORDER BY rec_id

                //OPEN RecordCursor
                //FETCH NEXT FROM RecordCursor INTO @rec_id,@field1
                int.TryParse(drDataLyne["rec_id"].ToString() , out rec_id);
                field1=drDataLyne[""].ToString().Trim();
                if(field1.ToUpper().StartsWith("H"))
                {
                //WHILE @@FETCH_STATUS = 0 
                //    BEGIN

//                        IF UPPER(LEFT(@field1,1)) = 'H'
//                            BEGIN	-- header record

                                seq_id = 0;

//                                -- Remove leading zeros for QCD invoices:
                                invoice_num = field1.Substring(50, 7).Trim();
//                                    RTRIM(LTRIM(CASE WHEN @import_source = 'QCD_GROUP' THEN CAST(RTRIM(SUBSTRING(@field1,51,7)) AS INT)
//                                                    ELSE RTRIM(SUBSTRING(@field1,51,7)) END))
//--								SET @vend_customer_num = LTRIM(RTRIM(CONVERT(VARCHAR(10),CAST(SUBSTRING(@field1,7,10) AS INT))))
//                                -- QCD vendor uses 5-digit store IDs as customer number. Allow for this in CASE statement below:
                                //SET @vend_customer_num = RTRIM(LTRIM(CASE WHEN @import_source = 'QCD_GROUP' THEN SUBSTRING(@field1,12,5)
                                //                            ELSE LTRIM(RTRIM(CONVERT(VARCHAR(10),CAST(SUBSTRING(@field1,7,10) AS INT)))) END))
                    if(import_source == "QCD_GROUP")
                    {
                        vend_customer_num = field1.Substring(11,5).Trim();
                    }
                    else
                    {
                        vend_customer_num = field1.Substring(6,10).Trim();
                    }
                    credit_apply_to_num = field1.Substring(106, 7).Trim();        // LTRIM(RTRIM(ISNULL(SUBSTRING(@field1,107,7),'')))
                    vend_supplier_id =field1.Substring(1,5).ToUpper().Trim();       // UPPER(RTRIM(LTRIM(SUBSTRING(@field1,2,5))))
                    sbSql.Clear();
                    sbSql.Append("INSERT ##InvoiceHeader (rec_id,record_type,vend_supplier_id,vend_customer_num,customer_po,");
                    sbSql.Append("debit_or_credit,date_added,added_by,invoice_num,date_delivered,date_invoice,");
                    sbSql.Append("invoice_amt,invoice_tax,invoice_freight,credit_apply_to_num,record_end_char,");
                    sbSql.Append("import_file_id,import_source,status_flag");
                    sbSql.Append(") VALUES (");
                    sbSql.Append(string.Format("{0}",rec_id));
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(0,1).ToUpper()));
                    sbSql.Append(string.Format(",'{0}'",vend_supplier_id));
                    sbSql.Append(string.Format(",'{0}'",vend_customer_num));
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(16,15)));

                    sbSql.Append(string.Format(",'{0}'", field1.Substring(31, 1))); //SUBSTRING(@field1,32,1)
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(32, 8))); //SUBSTRING(@field1,33,8)
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(40, 10))); //SUBSTRING(@field1,41,10)
                    sbSql.Append(string.Format(",'{0}'", invoice_num)); //,@invoice_num,
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(57, 8))); //SUBSTRING(@field1,58,8)
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(65, 8))); //,SUBSTRING(@field1,66,8),
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(73, 11).Trim())); //LTRIM(RTRIM(SUBSTRING(@field1,74,11)))
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(84, 11))); //,LTRIM(RTRIM(SUBSTRING(@field1,85,11)))
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(95, 11))); //,LTRIM(RTRIM(SUBSTRING(@field1,96,11)))
                    sbSql.Append(string.Format(",'{0}'", credit_apply_to_num)); //,@credit_apply_to_num
                    sbSql.Append(string.Format(",'{0}'", field1.Substring(113, 1))); //,SUBSTRING(@field1,114,1),
                    sbSql.Append(string.Format(",{0}", import_file_id)); // @import_file_id,
                    sbSql.Append(string.Format(",'{0}'", import_source)); //@import_source,0)
                    sbSql.Append(")");
                    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());


                    //--								SET @custom = SUBSTRING(@field1,107,1)

//                                FETCH NEXT FROM RecordCursor INTO @rec_id,@field1

//                                CONTINUE

//                            END	-- header record
                }
                else if(field1.ToUpper().StartsWith("D"))
                {
                        //IF UPPER(LEFT(@field1,1)) = 'D'
                        //    BEGIN 	-- detail record

                        seq_id++;
                        //        -- Adjust for problematic detail quantities:
                        //        -- Expected is that field is 11 characters long. Corrupt records have 12 characters. Could there ever be 13 or more?
                        //        SELECT @PositionShift = CASE 
                        //            WHEN ISNUMERIC(LTRIM(RTRIM(SUBSTRING(@field1,88,15)))) = 1 THEN 4
                        //            WHEN ISNUMERIC(LTRIM(RTRIM(SUBSTRING(@field1,88,14)))) = 1 THEN 3
                        //            WHEN ISNUMERIC(LTRIM(RTRIM(SUBSTRING(@field1,88,13)))) = 1 THEN 2
                        //            WHEN ISNUMERIC(LTRIM(RTRIM(SUBSTRING(@field1,88,12)))) = 1 THEN 1
                        //            ELSE 0 END
                    PositionShift=0;
                    sbSql.Clear();
                    sbSql.Append("INSERT ##InvoiceDetail (rec_id,record_type,invoice_num,sequence_id,vend_customer_num,vend_supplier_id,item_number,item_desc,/* account_code,*/ quantity,uom,price,");
                    sbSql.Append("item_tax,item_tax_percent,custom_definitions,d_record_end_char,import_file_id,import_source,status_flag");    
                    sbSql.Append(") VALUES (");
                    sbSql.Append(string.Format("{0}",rec_id));
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(0,1).ToUpper()));//,UPPER(LEFT(@field1,1)),
                    sbSql.Append(string.Format(",'{0}'",invoice_num));//@invoice_num,
                    sbSql.Append(string.Format(",{0}",seq_id));//@seq_id
                    sbSql.Append(string.Format(",'{0}'",vend_customer_num));//,@vend_customer_num
                    sbSql.Append(string.Format(",'{0}'",vend_supplier_id));//,@vend_supplier_id
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(1,6).Trim()));//,LTRIM(RTRIM(SUBSTRING(@field1,2,6)))
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(7,80).Trim()));//,LTRIM(RTRIM(SUBSTRING(@field1,8,80))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(87+PositionShift,11).Trim()));//LTRIM(RTRIM(SUBSTRING(@field1,88 + @PositionShift,11))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(98+PositionShift,2).Trim()));//LTRIM(RTRIM(SUBSTRING(@field1,99 + @PositionShift,2))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(100+PositionShift,11).Trim()));//LTRIM(RTRIM(SUBSTRING(@field1,101 + @PositionShift,11))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(111+PositionShift,11).Trim()));//LTRIM(RTRIM(SUBSTRING(@field1,112 + @PositionShift,11))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(122+PositionShift,11).Trim()));//LTRIM(RTRIM(RTRIM(SUBSTRING(@field1,123 + @PositionShift,11)))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(133+PositionShift,20).Trim()));//LTRIM(RTRIM(SUBSTRING(@field1,134 + @PositionShift,20))),
                    sbSql.Append(string.Format(",'{0}'",field1.Substring(153+PositionShift,1).Trim()));//LTRIM(RTRIM(SUBSTRING(@field1,154 + @PositionShift,1)))
                    sbSql.Append(string.Format(",{0}",import_file_id));//,@import_file_id,
                    sbSql.Append(string.Format(",'{0}'",import_source));//@import_source,
                    sbSql.Append(string.Format(",{0}",0));//0)
                    sbSql.Append(")");
                    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

                        //        FETCH NEXT FROM RecordCursor INTO @rec_id,@field1

                        //        CONTINUE

                        //    END 	-- detail record
                }
                else if(field1.ToUpper().StartsWith("S"))
                {
                        //IF UPPER(LEFT(@field1,1)) = 'S'
                        //    BEGIN 	-- summary record
                        record_type =field1.Substring(0,1).ToUpper();       // UPPER(LEFT(@field1,1))
                        total_invoice_lines =field1.Substring(1,7).Trim();      // RTRIM(LTRIM(SUBSTRING(@field1,2,7)))
                        s_record_end_char =field1.Substring(8,1);       // SUBSTRING(@field1,9,1)
                    sbSql.Clear();
                        sbSql.Append("INSERT ##InvoiceSummary (rec_id,record_type,invoice_num,vend_customer_num,vend_supplier_id,");
                        sbSql.Append("total_invoice_lines,s_record_end_char,import_file_id,import_source");
                    sbSql.Append(") VALUES (");
                    sbSql.Append(string.Format("{0}",rec_id));//        @rec_id
                    sbSql.Append(string.Format(",'{0}'",record_type));//       ,@record_type,
                    sbSql.Append(string.Format(",'{0}'",invoice_num));//       @invoice_num,
                    sbSql.Append(string.Format(",'{0}'",vend_customer_num));//       @vend_customer_num,
                    sbSql.Append(string.Format(",'{0}'",vend_supplier_id));//       @vend_supplier_id,
                    sbSql.Append(string.Format(",'{0}'",total_invoice_lines));//       @total_invoice_lines,
                    sbSql.Append(string.Format(",'{0}'",s_record_end_char));//       @s_record_end_char,
                    sbSql.Append(string.Format(",{0}",import_file_id));//       @import_file_id,
                    sbSql.Append(string.Format(",'{0}'",import_source));//       @import_source)
                    sbSql.Append(")");
                    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
                        //        FETCH NEXT FROM RecordCursor INTO @rec_id,@field1

                        //        CONTINUE

                        //    END 	-- summary record
                }
						//FETCH NEXT FROM RecordCursor INTO @rec_id,@field1		-- this happens if record_type not in ('H','D','S')

					//END 	-- record cursor loop
            }
				//-- Update the file table to set the record to 1, successfully imported to temp tables:
				sbPrint.Append("Update the current file status");
            sbSql.Clear();
            sbSql.Append("UPDATE ftp_files");
            sbSql.Append("SET file_status = 1,");
            sbSql.Append(string.Format("file_import_date = '{0}'",now_dt));
            sbSql.Append(string.Format("WHERE import_file_id = {0}",import_file_id));
            sbSql.Append("AND file_status = -1");
            clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

				//CLOSE RecordCursor
				//DEALLOCATE RecordCursor

			//END		-- If no Errors encountered during bulk copy loop
        }
        //FETCH NEXT FROM FileCursor INTO
        //    @import_file_id,
        //    @import_source,
        //    @file_path,
        //    @import_file_name,
        //    @file_status

	//END		-- File Process Loop
    }
//CLOSE FileCursor
//DEALLOCATE FileCursor

//INSERT IntegrationStaging.dbo._AdminProcessRunData (	--ProcessRunPK,
//    ServerName,ProcessName,ProcessDescription,ProcessStepID,ProcessStepName,ProcessStepDescription,
//    ProcessServerName,StepSourceServer,StepDestinationServer,ProcessStart,ProcessEnd,StepRowCount,ProcessStartOrEndStep,CurrentDateTime)
//SELECT @@SERVERNAME,'apinvimp1_sp','Invoice import step 1 - read file data from temp table',2,'Finished line item detail loop',NULL,
//    'HANSOLO','HANSOLO','HANSOLO',GETDATE(),NULL,NULL,NULL,GETDATE()

//-- Eliminate the possibility of null records getting into the production tables:
    sTemp="DELETE FROM ##InvoiceDetail WHERE invoice_num IS NULL AND vend_customer_num IS NULL AND item_number IS NULL AND item_desc IS NULL";
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sTemp);

    sTemp="DELETE FROM ##InvoiceHeader WHERE invoice_num IS NULL AND vend_supplier_id IS NULL AND vend_customer_num IS NULL AND date_invoice IS NULL";
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sTemp);

//-- Remove all zeros from this field if needed:
    sTemp="UPDATE ##InvoiceHeader SET credit_apply_to_num = '' WHERE RTRIM(LTRIM(credit_apply_to_num)) = '*'";
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sTemp);

    sTemp="UPDATE ##InvoiceHeader SET credit_apply_to_num = '' WHERE ISNUMERIC(credit_apply_to_num) = 1 AND RTRIM(LTRIM(credit_apply_to_num)) <> '' AND CAST(ISNULL(credit_apply_to_num,'0') AS INT) = 0";  //		-- '0000000'
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sTemp);

//-- Remove all "9"s from this field (9999999):
    sTemp="UPDATE ##InvoiceHeader SET customer_po = '' WHERE RTRIM(LTRIM(customer_po)) = '9999999' OR ISNULL(RTRIM(LTRIM(customer_po)),'') = ''";
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sTemp);

//-- Rounding found to be a problem for at least one QCD invoice. See if adjusting it here fixes this and make sure it doesn't throw off
//-- other invoices (testing only):
//--UPDATE ##InvoiceDetail SET price = LTRIM(STR(CAST(ISNULL(RTRIM(LTRIM(price)),0.0000) AS MONEY),10,2))


//-- **************************** Add Ver 4 code **************************** --
//IF @DebugFlag = 1 PRINT 'Check for incomplete files or incorrect line counts:'
    if(debugFlag)
    {
        sbPrint.Append("Check for incomplete files or incorrect line counts:");
    }
//-- Check here for accuracy of # of detail lines:  (Below statement looks at summary record line count and then a verification count on the
//-- number of actual detail lines for each specific invoice in the file. Only checks invoices that don't already exist in production table.)
    sbSql.Clear();
    sbSql.Append("SELECT DISTINCT A.invoice_num");       //	--,A.vend_supplier_id,A.vend_customer_num,A.total_invoice_lines,B.DetailCount
    sbSql.Append("FROM ##InvoiceSummary A INNER JOIN ");
    sbSql.Append("(SELECT invoice_num,import_source,vend_customer_num,COUNT(*) AS DetailCount ");
    sbSql.Append("FROM ##InvoiceDetail ");
    sbSql.Append("GROUP BY invoice_num,import_source,vend_customer_num) AS B ON B.import_source = A.import_source AND B.invoice_num = A.invoice_num AND B.vend_customer_num = A.vend_customer_num");
    sbSql.Append("WHERE CAST(A.total_invoice_lines AS SMALLINT) <> B.DetailCount");
    sbSql.Append("AND A.invoice_num NOT IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = A.vend_customer_num AND H.vend_supplier_id = A.vend_supplier_id)");
    DataSet dsTemp2=clsSql.CmdDataset(Properties.Settings.Default.conDev,sbSql.ToString());
    if(dsTemp2.Tables.Count>0 && dsTemp2.Tables[0].Rows.Count>0)
    {
//    BEGIN

//        PRINT 'Incorrect line count(s) found.'
        sbPrint.Append("Incorrect line count(s) found.");
sbSql.Clear();
        sbSql.Append("UPDATE #H");
        sbSql.Append("SET #H.status_flag = -1");
        sbSql.Append("FROM ##InvoiceHeader #H");
        sbSql.Append("WHERE #H.invoice_num IN ");
        sbSql.Append("(SELECT DISTINCT A.invoice_num FROM ##InvoiceSummary A ");
        sbSql.Append("INNER JOIN");
        sbSql.Append("(SELECT invoice_num,import_source,vend_customer_num,COUNT(*) AS DetailCount FROM ##InvoiceDetail GROUP BY invoice_num,import_source,vend_customer_num) AS B ON B.import_source = A.import_source AND B.invoice_num = A.invoice_num AND B.vend_customer_num = A.vend_customer_num");
        sbSql.Append("WHERE CAST(A.total_invoice_lines AS SMALLINT) <> B.DetailCount AND A.import_source = #H.import_source)");
        sbSql.Append("AND #H.invoice_num NOT IN ");
        sbSql.Append("(SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id)");

//        SELECT @rec_count = @@ROWCOUNT		-- Invoices where summary line count does not equal actual detail line count in apinvdet
        rec_count=clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());
//        SELECT TOP 1 @message1 = 'Invoice or File Validation Error. An inconsistent detail line count error was encountered while processing data files for the daily invoices. Please review import file ID ' + RTRIM(LTRIM(CAST(import_file_id AS VARCHAR(10)))) + ', invoice number ' + ISNULL(invoice_num,'') + '. There may be other problem files/invoices. Total problem invoice records involved in this file: ' + RTRIM(LTRIM(CAST(@rec_count AS VARCHAR(3)))) + ''
//        FROM ##InvoiceHeader	
//        WHERE status_flag = -1

            message1 = string.Format("Invoice File Validation Error.  An inconsistent detail line count error was encountered while processing data files for daily invoices.  Please review import file {0}, invoice number {1}.'",import_file_name,invoice_num);
        sbSql.Clear();
        sbSql.Append("EXEC master..xp_startmail ");
        sbSql.Append("EXEC master..xp_stopmail ");
        sbSql.Append("EXEC master..xp_startmail ");     //--'MS Exchange Settings', 'SQLSRVRFIN'  -- Will come FROM SQLSRVRFIN@zaxbys.com
        sbSql.Append("EXEC master..xp_sendmail @recipients = 'pjohnson@zaxbys.com;cstolz@zaxbys.com;dcarter@zaxbys.com',");
        sbSql.Append("@subject = 'Error(s): Invoice or File Validation (Line Count)','");
        sbSql.Append(string.Format("@message = {0}",message1));        // = 'Invoice File Validation Error.  An inconsistent detail line count error was encountered while processing data files for daily invoices.  Please review import file ' + @import_file_name + ', invoice number ' + @invoice_num + '.'
        clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());
        sbSql.Clear();
        sbSql.Append("INSERT apinvhdr (rec_id,invoice_num,import_file_id,apply_to_num,vendor_code,company_code,customer_num,vend_supplier_id,po_num,");
        sbSql.Append("debit_credit,date_added,added_by,date_delivered,date_invoice,invoice_amt,invoice_tax,");
        sbSql.Append("invoice_freight_amt,rec_status,status_date,import_flag,import_date)");
        sbSql.Append("SELECT DISTINCT #H.rec_id,#H.invoice_num,#H.import_file_id,#H.credit_apply_to_num,'',RTRIM(LTRIM(ISNULL(C.comp_code,''))),#H.vend_customer_num,#H.vend_supplier_id,#H.customer_po,");
        sbSql.Append("#H.debit_or_credit,CONVERT(VARCHAR(10),#H.date_added,101),#H.added_by,CONVERT(VARCHAR(10),#H.date_delivered,101),");
        sbSql.Append("CONVERT(VARCHAR(10),#H.date_invoice,101),CAST(#H.invoice_amt AS MONEY),CAST(#H.invoice_tax AS MONEY),");
        sbSql.Append("CAST(#H.invoice_freight AS MONEY),-99,GETDATE(),0,NULL"); //		-- 0 = record not validated, 0 = record not imported
        sbSql.Append("FROM ##InvoiceHeader #H");
        sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #H.vend_customer_num AND C.vend_supplier_id = #H.vend_supplier_id AND C.inactive = 0");
        sbSql.Append("WHERE #H.status_flag = -1");
        sbSql.Append(" AND NOT EXISTS (SELECT 1 FROM apinvhdr H WHERE H.invoice_num = #H.invoice_num AND H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id)");
        sbSql.Append(" ORDER BY #H.rec_id");
        clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

//        -- NOTE:  Detail record insert (apinvdet) is further below for these header records.

//        -- Scaled down version of the error table: (Ver. 5)
        sbSql.Clear();
        sbSql.Append("INSERT apimp_err (rec_type,vendor_group,invoice_num,customer_num,vend_supplier_id,company_code,date_invoice,error_code,error_date)");
        sbSql.Append("SELECT DISTINCT 'INVOICE',RTRIM(#H.import_source),RTRIM(#H.invoice_num),RTRIM(#H.vend_customer_num),RTRIM(#H.vend_supplier_id),RTRIM(LTRIM(c.comp_code)),");
        sbSql.Append("CONVERT(VARCHAR(10),#H.date_invoice,101),-94,GETDATE()");     //	-- Error -94 = mismatched detail line count with invoice summary record
        sbSql.Append("FROM ##InvoiceHeader #h");
        sbSql.Append("INNER JOIN apvcust c ON c.pfg_cust_num = RTRIM(#H.vend_customer_num) AND c.vend_supplier_id = RTRIM(#H.vend_supplier_id) AND c.inactive = 0");
        sbSql.Append("WHERE #H.status_flag = -1");
        clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
//    END
    }
//-- **************************** END Ver 4 code **************************** --

//-- Update the account codes in this temp table and identify unknown item numbers.
//-- This will also remove leading zeros from integer item codes.
//-- (Finding unknown items is easier here than in the large apinvdet table.)
    if(debugFlag)
    {
        sbPrint.Append("Check for and update item codes...");
        sbPrint.Append(string.Format("Time: {0}",DateTime.Now));
    }
//IF @DebugFlag = 1
//    BEGIN
//        PRINT 'Check for and update item codes...'
//        PRINT 'Time:  ' + CONVERT(VARCHAR(25),GETDATE(),121)
//    END

    sbSql.Clear();
    sbSql.Append("UPDATE ##InvoiceDetail");
    sbSql.Append("SET item_number = (SELECT CASE ");
    sbSql.Append("WHEN ISNUMERIC(item_number) = 1 THEN CAST(CAST(item_number AS INT) AS VARCHAR(8))");
    sbSql.Append("WHEN ISNUMERIC(item_number) = 0 THEN LTRIM(RTRIM(item_number))");     // -- Ver 4: added trim functions
    sbSql.Append("ELSE CAST(CAST(item_number AS INT) AS VARCHAR(8))");
    sbSql.Append("END)");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());

    sbSql.Clear();
    sbSql.Append("UPDATE ##InvoiceDetail");
    sbSql.Append("SET account_code = i.seg1_code");
    sbSql.Append("FROM apvitems i");
    sbSql.Append("WHERE ##InvoiceDetail.item_number = RTRIM(i.item_code)");
    sbSql.Append("AND i.vendor_short IN ('PFG_GROUP','QCD_GROUP','MBROS')");
    sbSql.Append("AND i.vendor_short = ##InvoiceDetail.import_source");
    sbSql.Append("AND i.item_list_id = 1");
    rowcount1=clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());
//SELECT @rowcount1 = @@rowcount
    if(debugFlag)
    {
        sbPrint.Append(string.Format("Field account_code updates count: {0}",rowcount1));
    }
//IF @DebugFlag = 1 PRINT 'Field account_code updates count: ' + CAST(@rowcount1 as VARCHAR)

//-- Historical expense accounts used prior to fiscal 2010: (only for PFG vendors)
//IF EXISTS (SELECT 1 FROM ##InvoiceHeader WHERE CAST(CONVERT(VARCHAR(10),date_invoice,101) AS DATETIME) < CAST('12/28/2009' AS DATETIME))
//    BEGIN
//        UPDATE ##InvoiceDetail
//        SET account_code = i.seg1_code
//        FROM apvitems_OLD i
//        WHERE RTRIM(##InvoiceDetail.invoice_num) IN
//                (SELECT RTRIM(#H.invoice_num) FROM ##InvoiceHeader #H
//                WHERE CAST(CONVERT(VARCHAR(10),#H.date_invoice,101) AS DATETIME) < CAST('12/28/2009' AS DATETIME)
//                    AND #H.vend_customer_num = ##InvoiceDetail.vend_customer_num AND #H.vend_supplier_id = ##InvoiceDetail.vend_supplier_id)
//            AND ##InvoiceDetail.item_number = RTRIM(i.item_code)
//            AND i.vendor_short IN ('PFG_GROUP')
//            AND i.item_list_id = 1

//        SELECT @rowcount1 = @@rowcount
//        PRINT 'Field account_code updates OLD INVOICES count: ' + CAST(@rowcount1 AS VARCHAR)
//    END

//-- Modified below for multiple distributors: (Ver. 5)
//-- Add new items to the item table:
    sbSql.Clear();
    sbSql.Append("INSERT apvitems (vendor_short,vendor_long,item_list_id,seg1_code,seg1_desc,item_code,zfi_item_code,item_unit,item_desc,");
    sbSql.Append("item_price,item_inactive,item_status,status_date,status_user,item_note)");
    sbSql.Append("SELECT #D.import_source,");
    sbSql.Append("CASE WHEN #D.import_source = 'MBROS' THEN 'Manning Brothers' WHEN #D.import_source = 'PFG_GROUP' THEN 'PFG - All distributors' WHEN #D.import_source = 'QCD_GROUP' THEN 'QCD - All distributors' ELSE '?' END,");
    sbSql.Append("1,null,null,LTRIM(RTRIM(#D.item_number)),NULL AS zfi_item_code,'',MAX(RTRIM(#D.item_desc)),MAX(CAST(#D.price AS DECIMAL(12,2))),0,0,GETDATE(),'FTPIMPORT',''");
    sbSql.Append("FROM ##InvoiceDetail #D");
    sbSql.Append("WHERE NOT EXISTS");
    sbSql.Append("(SELECT 1 FROM apvitems i WHERE i.item_code = #D.item_number AND i.vendor_short = #D.import_source)");
    sbSql.Append("GROUP BY #D.import_source,");
    sbSql.Append("CASE WHEN #D.import_source = 'MBROS' THEN 'Manning Brothers' WHEN #D.import_source = 'PFG_GROUP' THEN 'PFG - All distributors' WHEN #D.import_source = 'QCD_GROUP' THEN 'QCD - All distributors' ELSE '?' END,");
    sbSql.Append("LTRIM(RTRIM(#D.item_number))");
    sbSql.Append("ORDER BY MAX(RTRIM(#D.item_desc))");
    rowcount1=clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
//SELECT @rowcount1 = @@rowcount
    if(debugFlag)
    {
        sbPrint.Append(string.Format("Insert new items count: {0}",rowcount1));
    }
//IF @DebugFlag = 1 PRINT 'Insert new items count: ' + CAST(@rowcount1 as VARCHAR)
    sbSql.Clear();
    sbSql.Append("INSERT IntegrationStaging.dbo._AdminProcessRunData (");	//--ProcessRunPK,
    sbSql.Append("ServerName,ProcessName,ProcessDescription,ProcessStepID,ProcessStepName,ProcessStepDescription,");
    sbSql.Append("ProcessServerName,StepSourceServer,StepDestinationServer,ProcessStart,ProcessEnd,StepRowCount,ProcessStartOrEndStep,CurrentDateTime)");
    sbSql.Append("SELECT @@SERVERNAME,'apinvimp1_sp','Invoice import step 1 - read file data from temp table',3,'Finished working with apvitem records','Update accounts and insert new items. Rowcount is new items',");
    sbSql.Append("'HANSOLO','HANSOLO','HANSOLO',GETDATE(),NULL,@rowcount1,NULL,GETDATE()");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

//-- **************************** UPDATE 10/23/2009 **************************** --
//-- **************************** UPDATE 12/19/2013 (Ver. 5) ******************* --

//-- Ver. 5 - Zero dollar transactions:	-- SELECT * FROM ##InvoiceHeader ORDER BY invoice_num
    sbSql.Clear();
    sbSql.Append("INSERT apinvdet (rec_id,invoice_num,seq_id,customer_num,vend_supplier_id,item_number,item_desc,");
    sbSql.Append("account_code,account_desc,quantity,uom,price,item_tax,item_tax_percent,custom_definitions)");
    sbSql.Append("SELECT #D.rec_id,#D.invoice_num,#D.sequence_id,#D.vend_customer_num,UPPER(RTRIM(LTRIM(#D.vend_supplier_id))),LTRIM(RTRIM(#D.item_number)),LTRIM(RTRIM(#D.item_desc)),");
    sbSql.Append("#D.account_code,G.seg1_desc,#D.quantity,#D.uom,CAST(#D.price AS DECIMAL(6,2)),CAST(#D.item_tax AS DECIMAL(6,2)),CAST(#D.item_tax_percent AS DECIMAL(6,2)),LEFT(#D.custom_definitions,10)");
    sbSql.Append("FROM ##InvoiceDetail #D");
    sbSql.Append("INNER JOIN ##InvoiceHeader #H ON #H.invoice_num = #D.invoice_num AND #H.vend_customer_num = #D.vend_customer_num AND #H.vend_supplier_id = #D.vend_supplier_id AND #H.import_source = #D.import_source");
    sbSql.Append("LEFT OUTER JOIN apglaccts G ON G.seg1_code = #D.account_code");
    sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #D.vend_customer_num AND C.vend_supplier_id = #D.vend_supplier_id AND C.inactive = 0");
    sbSql.Append("WHERE #H.status_flag = 0 AND CAST(#H.invoice_amt AS MONEY) = 0.00");
//    -- This line below will make sure invoices that are not already in these tables are not re-imported:
    sbSql.Append("AND NOT EXISTS");
    sbSql.Append("(SELECT 1 FROM apinvdet D WHERE D.invoice_num = #D.invoice_num AND D.customer_num = #D.vend_customer_num AND D.vend_supplier_id = #D.vend_supplier_id)");
    sbSql.Append("ORDER BY #D.rec_id");
    rowcount2=clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
//SELECT @rowcount2 = @@rowcount

    sbSql.Clear();
    sbSql.Append("INSERT apinvhdr (rec_id,invoice_num,import_file_id,apply_to_num,vendor_code,company_code,customer_num,vend_supplier_id,po_num,");
    sbSql.Append("debit_credit,date_added,added_by,date_delivered,date_invoice,invoice_amt,invoice_tax,");
    sbSql.Append("invoice_freight_amt,rec_status,status_date,import_flag,import_date)");
    sbSql.Append("SELECT DISTINCT #H.rec_id,#H.invoice_num,#H.import_file_id,#H.credit_apply_to_num,'',RTRIM(LTRIM(ISNULL(C.comp_code,''))),#H.vend_customer_num,#H.vend_supplier_id,#H.customer_po,");
    sbSql.Append("#H.debit_or_credit,CONVERT(VARCHAR(10),#H.date_added,101),#H.added_by,CONVERT(VARCHAR(10),#H.date_delivered,101),");
    sbSql.Append("CONVERT(VARCHAR(10),#H.date_invoice,101),CAST(#H.invoice_amt AS MONEY),CAST(#H.invoice_tax AS MONEY),");
    sbSql.Append("CAST(#H.invoice_freight AS MONEY),26,GETDATE(),0,NULL		-- status = 6 for zero-dollar transactions, 0 = record not imported");
    sbSql.Append("FROM ##InvoiceHeader #H ");
    sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #H.vend_customer_num AND C.vend_supplier_id = #H.vend_supplier_id AND C.inactive = 0");
    sbSql.Append("WHERE #H.status_flag = 0 AND CAST(#H.invoice_amt AS MONEY) = 0.00");
    sbSql.Append("AND #H.invoice_num NOT IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id)");
    sbSql.Append("ORDER BY #H.rec_id");
    rowcount1=clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
//SELECT @rowcount1 = @@rowcount

//-- Prevent these temp table records from being tagged as duplicates by changing the status_flag value:
    sbSql.Clear();
    sbSql.Append("UPDATE #H");
    sbSql.Append("SET #H.status_flag = 26");
    sbSql.Append("FROM ##InvoiceHeader #H");
    sbSql.Append("WHERE #H.status_flag = 0 AND CAST(#H.invoice_amt AS MONEY) = 0.00");
    sbSql.Append("AND #H.invoice_num IN ");
    sbSql.Append("(SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id");
    sbSql.Append("AND H.rec_status = 26)");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
//-- Get detail rows for possible problem records from above where line counts did not match: (Header table, apinvhdr, was already inserted)
    sbSql.Clear();
    sbSql.Append("INSERT apinvdet (rec_id,invoice_num,seq_id,customer_num,vend_supplier_id,item_number,item_desc,");
    sbSql.Append("account_code,account_desc,quantity,uom,price,item_tax,item_tax_percent,custom_definitions)");
    sbSql.Append("SELECT #D.rec_id,#D.invoice_num,#D.sequence_id,#D.vend_customer_num,UPPER(RTRIM(LTRIM(#D.vend_supplier_id))),LTRIM(RTRIM(#D.item_number)),LTRIM(RTRIM(#D.item_desc)),");
    sbSql.Append("#D.account_code,G.seg1_desc,#D.quantity,#D.uom,CAST(#D.price AS DECIMAL(6,2)),CAST(#D.item_tax AS DECIMAL(6,2)),CAST(#D.item_tax_percent AS DECIMAL(6,2)),LEFT(#D.custom_definitions,10)");
    sbSql.Append("FROM ##InvoiceDetail #D");
    sbSql.Append("INNER JOIN ##InvoiceHeader #H ON #H.invoice_num = #D.invoice_num AND #H.vend_customer_num = #D.vend_customer_num AND #H.vend_supplier_id = #D.vend_supplier_id AND #H.import_source = #D.import_source");
    sbSql.Append("LEFT OUTER JOIN apglaccts G ON G.seg1_code = #D.account_code");
    sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #D.vend_customer_num AND C.vend_supplier_id = #D.vend_supplier_id AND C.inactive = 0");
    sbSql.Append("WHERE #H.status_flag = -1");
//    -- This line below will make sure invoices that are not already in these tables are not re-imported:
    sbSql.Append("AND NOT EXISTS");
    sbSql.Append("(SELECT 1 FROM apinvdet D WHERE D.invoice_num = #D.invoice_num AND D.customer_num = #D.vend_customer_num AND D.vend_supplier_id = #D.vend_supplier_id)");
    sbSql.Append("ORDER BY #D.rec_id");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());

//-- This update is for header records that have no detail lines, but have a non-zero invoice amount (i.e., orphaned headers)
//-- Updated to ignore zero dollar invoices, Ver. 5
    sbSql.Clear();
//IF EXISTS(
    sbSql.Append("SELECT 1 FROM ##InvoiceHeader #H WHERE CAST(#H.invoice_amt AS MONEY) <> 0.00");
    sbSql.Append("AND NOT EXISTS");//	-- No detail lines
    sbSql.Append("(SELECT 1 FROM ##InvoiceDetail #D WHERE #D.invoice_num = #H.invoice_num AND #D.vend_customer_num = #H.vend_customer_num AND #D.vend_supplier_id = #H.vend_supplier_id)");
    sbSql.Append("AND #H.invoice_num NOT IN	"); //-- Not already in production table
    sbSql.Append("(SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id)");
    dsTemp2=clsSql.CmdDataset(Properties.Settings.Default.conDev, sbSql.ToString());
    if(dsTemp2.Tables.Count>0 && dsTemp2.Tables[0].Rows.Count>0)
    {
//    BEGIN
        sbSql.Clear();
        sbSql.Append("UPDATE ##InvoiceHeader");
        sbSql.Append("SET status_flag = -99");
        sbSql.Append("WHERE invoice_amt <> 0.00");
        sbSql.Append("AND NOT EXISTS ");
        sbSql.Append("(SELECT 1 FROM ##InvoiceDetail #D ");
        sbSql.Append("WHERE #D.invoice_num = ##InvoiceHeader.invoice_num");
        sbSql.Append(" AND #D.vend_customer_num = ##InvoiceHeader.vend_customer_num AND #D.vend_supplier_id = ##InvoiceHeader.vend_supplier_id)");
        sbSql.Append("AND invoice_num NOT IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = ##InvoiceHeader.vend_customer_num AND H.vend_supplier_id = ##InvoiceHeader.vend_supplier_id)");
        clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());
        
        sbSql.Clear();
        sbSql.Append("INSERT apimp_err (rec_type,vendor_group,invoice_num,customer_num,vend_supplier_id,company_code,date_invoice,error_code,error_date)");
        sbSql.Append("SELECT 'INVOICE',RTRIM(#H.import_source),RTRIM(#H.invoice_num),RTRIM(#H.vend_customer_num),");
        sbSql.Append("RTRIM(#H.vend_supplier_id),RTRIM(LTRIM(ISNULL(C.comp_code,''))),#H.date_invoice,-97,GETDATE()");  // -- -97 = 'DETAILREC','There are no details for this header record.',
        sbSql.Append("FROM ##InvoiceHeader #H ");
        sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = RTRIM(#H.vend_customer_num) AND C.vend_supplier_id = RTRIM(#H.vend_supplier_id) AND C.inactive = 0");
        sbSql.Append("WHERE #H.status_flag = -99");
        clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

//        -- Put each record into the production table as an error:
        sbSql.Clear();
        sbSql.Append("INSERT apinvhdr (rec_id,invoice_num,import_file_id,apply_to_num,vendor_code,company_code,customer_num,vend_supplier_id,po_num,");
        sbSql.Append("debit_credit,date_added,added_by,date_delivered,date_invoice,invoice_amt,invoice_tax,");
        sbSql.Append("invoice_freight_amt,rec_status,status_date,import_flag,import_date)");
        sbSql.Append("SELECT DISTINCT #H.rec_id,#H.invoice_num,#H.import_file_id,#H.credit_apply_to_num,'',RTRIM(LTRIM(ISNULL(C.comp_code,''))),#H.vend_customer_num,#H.vend_supplier_id,#H.customer_po,");
        sbSql.Append("#H.debit_or_credit,CONVERT(VARCHAR(10),#H.date_added,101),#H.added_by,CONVERT(VARCHAR(10),#H.date_delivered,101),");
        sbSql.Append("CONVERT(VARCHAR(10),#H.date_invoice,101),CAST(#H.invoice_amt AS MONEY),CAST(#H.invoice_tax AS MONEY),");
        sbSql.Append("CAST(#H.invoice_freight AS MONEY),#H.status_flag,GETDATE(),0,NULL	"); //	-- 0 = record not validated, 0 = record not imported
        sbSql.Append("FROM ##InvoiceHeader #H ");
        sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #H.vend_customer_num AND C.vend_supplier_id = #H.vend_supplier_id AND C.inactive = 0");
        sbSql.Append("WHERE #H.status_flag = -99 AND #H.invoice_amt <> 0.00");
        sbSql.Append("AND NOT EXISTS ");
        sbSql.Append("(SELECT 1 FROM ##InvoiceDetail #D ");
        sbSql.Append("WHERE #D.invoice_num = #H.invoice_num AND #D.vend_customer_num = #H.vend_customer_num AND #D.vend_supplier_id = #H.vend_supplier_id)");
        sbSql.Append("AND #H.invoice_num NOT IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id)");
        sbSql.Append("ORDER BY #H.rec_id");
           clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

//        SELECT @@ROWCOUNT AS 'MissingDetailLines'

//    END
    }

///**  Move the header and detail records from the temp tables to the history tables  **/
    if(debugFlag)
    {
        sbPrint.Append("Move recs from loading to history tables...");
        sbPrint.Append(string.Format("Time: {0}",DateTime.Now));
    }
//IF @DebugFlag = 1
//    BEGIN
//        PRINT 'Move recs from loading to history tables...'
//        PRINT 'Time:  ' + CONVERT(VARCHAR(25),GETDATE(),121)
//    END

//-- Check to see if any recycled invoice numbers were used. Process them as usual.	-- Not sure if vend_supplier_id needs to be evaluated??
    sTemp="SELECT 1 FROM ##InvoiceHeader #H WHERE #H.status_flag = 0 AND #H.invoice_num IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num <> #H.vend_customer_num)";
    dsTemp2=clsSql.CmdDataset(Properties.Settings.Default.conDev, sTemp);

    // tohere----------------------------------------------------------------------------------------
//IF EXISTS (SELECT 1 FROM ##InvoiceHeader #H WHERE #H.status_flag = 0 AND #H.invoice_num IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num <> #H.vend_customer_num))
//    BEGIN
//        PRINT 'Recycled invoice numbers detected:'
//        SELECT 'Recycled invoice numbers detected:'
//        SET @sql = 'SELECT ''RECYCLED'' AS Type,* FROM ##InvoiceHeader #H WHERE #H.status_flag = 0 AND #H.invoice_num IN (SELECT H.invoice_num FROM ' + @database + '..apinvhdr H WHERE H.customer_num <> #H.vend_customer_num) ORDER BY #H.rec_id'
//        EXEC(@sql)
//        SET @cmd = 'bcp "' + @sql + '" queryout "\\hansolo\APInvoicesPFG$\Errors\RecycledInvoiceNumbers_' + CONVERT(VARCHAR(8),GETDATE(),112) + '_' + replace(CONVERT(VARCHAR(8),GETDATE(),108),':','') + '.tab" -c -t\t -Shansolo -Usa -P2@x@dm1n'
//        EXEC master..xp_cmdshell @cmd

//    END

//-- Check to see if any of these current invoices were already received:  (Delete the original ones only if they are flagged as errors and then re-insert.)
//IF EXISTS (SELECT 1 FROM ##InvoiceHeader #H WHERE #H.status_flag = 0 AND #H.invoice_num IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id))
//    BEGIN
//        PRINT 'Duplicate invoice numbers detected!'
//        SELECT 'Duplicate invoice numbers detected:'
//        SET @sql = 'SELECT ''DUPLICATE'' AS Type,* FROM ##InvoiceHeader #H WHERE #H.status_flag = 0 AND #H.invoice_num IN (SELECT H.invoice_num FROM ' + @database + '..apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id) ORDER BY #H.rec_id'
//        EXEC(@sql)
//        SET @cmd = 'bcp "' + @sql + '" queryout "\\hansolo\APInvoicesPFG$\Errors\DuplicateInvoiceNumbers_' + CONVERT(VARCHAR(8),GETDATE(),112) + '_' + replace(CONVERT(VARCHAR(8),GETDATE(),108),':','') + '.tab" -c -t\t -Shansolo -Usa -P2@x@dm1n'
//        EXEC master..xp_cmdshell @cmd

//        -- Delete any production table invoices that are errors that had previously been received:
//        IF EXISTS (SELECT 1 FROM ##InvoiceHeader #H WHERE #H.status_flag = 0 AND #H.invoice_num in (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id AND H.rec_status = -99))
//            BEGIN
//                SELECT 'Invoice details to be deleted:'
//                SET @sql = 'SELECT ''DUP-DEL DET WITH ERR'' AS Type,D.* FROM ' + @database + '..apinvdet D INNER JOIN ' + @database + '..apinvhdr H ON D.invoice_num = H.invoice_num AND D.customer_num = H.customer_num AND D.vend_supplier_id = H.vend_supplier_id INNER JOIN ##InvoiceHeader #H ON #H.invoice_num = H.invoice_num AND #H.vend_customer_num = H.customer_num AND #H.vend_supplier_id = H.vend_supplier_id WHERE H.rec_status = -99 ORDER BY D.invoice_num'
//                EXEC(@sql)
//                SET @cmd = 'bcp "' + @sql + '" queryout "\\hansolo\APInvoicesPFG$\Errors\DuplicateErrInvoiceDetails_' + CONVERT(VARCHAR(8),GETDATE(),112) + '_' + replace(CONVERT(VARCHAR(8),GETDATE(),108),':','') + '.tab" -c -t\t -Shansolo -Usa -P2@x@dm1n'
//                EXEC master..xp_cmdshell @cmd

//                DELETE apimp_err 
//                FROM apimp_err e
//                    INNER JOIN apinvhdr H ON e.invoice_num = H.invoice_num AND e.customer_num = H.customer_num AND e.vend_supplier_id = H.vend_supplier_id
//                    INNER JOIN ##InvoiceHeader #h ON #H.invoice_num = e.invoice_num AND #H.vend_customer_num = e.customer_num AND #H.vend_supplier_id = e.vend_supplier_id
//                WHERE H.rec_status = -99

//                DELETE apinvdet
//                FROM apinvdet D 
//                    INNER JOIN apinvhdr H ON D.invoice_num = H.invoice_num AND D.customer_num = H.customer_num AND D.vend_supplier_id = H.vend_supplier_id
//                    INNER JOIN ##InvoiceHeader #h ON #H.invoice_num = H.invoice_num AND #H.vend_customer_num = H.customer_num AND #H.vend_supplier_id = H.vend_supplier_id AND #H.date_invoice = H.date_invoice
//                WHERE H.rec_status = -99

//                SELECT 'Invoice headers to be deleted:'
//                SET @sql = 'SELECT ''DUP-DEL HDR WITH ERR'' AS Type,H.* FROM ' + @database + '..apinvhdr H INNER JOIN ##InvoiceHeader #h ON #H.invoice_num = H.invoice_num AND #H.vend_customer_num = H.customer_num WHERE H.rec_status = -99 ORDER BY H.invoice_num'
//                EXEC(@sql)
//                SET @cmd = 'bcp "' + @sql + '" queryout "\\hansolo\APInvoicesPFG$\Errors\DuplicateErrInvoiceHeaders_' + CONVERT(VARCHAR(8),GETDATE(),112) + '_' + replace(CONVERT(VARCHAR(8),GETDATE(),108),':','') + '.tab" -c -t\t -Shansolo -Usa -P2@x@dm1n'
//                EXEC master..xp_cmdshell @cmd

//                DELETE FROM apinvhdr 
//                FROM apinvhdr h
//                    INNER JOIN ##InvoiceHeader #h ON #H.invoice_num = H.invoice_num AND #H.vend_customer_num = H.customer_num AND #H.vend_supplier_id = H.vend_supplier_id AND #H.date_invoice = H.date_invoice
//                WHERE H.rec_status = -99 
//            END

//    END


//-- Update: Remove vendor_code and company_code from this table.  Not needed. Do I need import_source for this table?
//-- Go ahead and insert production records:
    sbSql.Clear();
    sbSql.Append("INSERT apinvdet (rec_id,invoice_num,seq_id,customer_num,vend_supplier_id,item_number,item_desc,");
    sbSql.Append("account_code,account_desc,quantity,uom,price,item_tax,item_tax_percent,custom_definitions)");
    sbSql.Append("SELECT #D.rec_id,#D.invoice_num,#D.sequence_id,#D.vend_customer_num,UPPER(RTRIM(LTRIM(#D.vend_supplier_id))),LTRIM(RTRIM(#D.item_number)),LTRIM(RTRIM(#D.item_desc)),");
    sbSql.Append("#D.account_code,g.seg1_desc,#D.quantity,#D.uom,CAST(#D.price AS DECIMAL(6,2)),CAST(#D.item_tax AS DECIMAL(6,2)),CAST(#D.item_tax_percent AS DECIMAL(6,2)),LEFT(#D.custom_definitions,10)");
    sbSql.Append("FROM ##InvoiceDetail #d");
    sbSql.Append("INNER JOIN ##InvoiceHeader #h ON #H.invoice_num = #D.invoice_num AND #H.vend_customer_num = #D.vend_customer_num AND #H.vend_supplier_id = #D.vend_supplier_id AND #H.import_source = #D.import_source");
    sbSql.Append("LEFT OUTER JOIN apglaccts g ON g.seg1_code = #D.account_code");
    sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #D.vend_customer_num AND C.vend_supplier_id = #D.vend_supplier_id AND C.inactive = 0");
    sbSql.Append("WHERE #H.status_flag = 0 -- AND #D.invoice_num in (SELECT invoice_num FROM ##InvoiceHeader WHERE status_flag = 0)");
//    -- This line below will make sure invoices that are not already in these tables are not re-imported:
    sbSql.Append(" AND NOT EXISTS");
    sbSql.Append("(SELECT 1 FROM apinvdet D WHERE D.invoice_num = #D.invoice_num AND D.customer_num = #D.vend_customer_num AND D.vend_supplier_id = #D.vend_supplier_id)");
//--	#D.invoice_num NOT IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #D.vend_customer_num AND H.vend_supplier_id = #D.vend_supplier_id)
    sbSql.Append("ORDER BY #D.rec_id");
    rowcount2=clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());
//SELECT @rowcount2 = @@rowcount

//-- Remove comment fields for status and import (Ver. 5)
    sbSql.Clear();
    sbSql.Append("INSERT apinvhdr (rec_id,invoice_num,import_file_id,apply_to_num,vendor_code,company_code,customer_num,vend_supplier_id,po_num,");
    sbSql.Append("debit_credit,date_added,added_by,date_delivered,date_invoice,invoice_amt,invoice_tax,");
    sbSql.Append("invoice_freight_amt,rec_status,status_date,import_flag,import_date)");
    sbSql.Append("SELECT #H.rec_id,#H.invoice_num,#H.import_file_id,#H.credit_apply_to_num,ISNULL(C.zfi_vendor_code,''),RTRIM(LTRIM(ISNULL(C.comp_code,''))),#H.vend_customer_num,#H.vend_supplier_id,#H.customer_po,");
    sbSql.Append("#H.debit_or_credit,CONVERT(VARCHAR(10),#H.date_added,101),#H.added_by,CONVERT(VARCHAR(10),#H.date_delivered,101),");
    sbSql.Append("CONVERT(VARCHAR(10),#H.date_invoice,101),CAST(#H.invoice_amt AS MONEY),CAST(#H.invoice_tax AS MONEY),");
    sbSql.Append("CAST(#H.invoice_freight AS MONEY),#H.status_flag,GETDATE(),0,NULL	"); //	-- 0 = record not validated, 0 = record not imported
    sbSql.Append("FROM ##InvoiceHeader #h ");
    sbSql.Append("LEFT OUTER JOIN apvcust C ON C.pfg_cust_num = #H.vend_customer_num AND C.vend_supplier_id = #H.vend_supplier_id AND C.inactive = 0");
    sbSql.Append("WHERE #H.status_flag = 0 AND #H.invoice_num NOT IN (SELECT H.invoice_num FROM apinvhdr H WHERE H.customer_num = #H.vend_customer_num AND H.vend_supplier_id = #H.vend_supplier_id)");
//    -- The line above will make sure invoices that are not already in these tables are not re-imported.
    sbSql.Append("ORDER BY #H.rec_id");
    rowcount1=clsSql.CmdNonQuery(Properties.Settings.Default.conDev,sbSql.ToString());
//SELECT @rowcount1 = @@rowcount
    if(debugFlag)
    {
//IF @DebugFlag = 1
//    BEGIN
//        PRINT 'Rows sent to apinvhdr: ' + CAST(@rowcount1 AS VARCHAR)
        sbPrint.Append(string.Format("Rows sent to apinvhdr: {0}",rowcount1));
//        PRINT 'Rows sent to apinvdet: ' + CAST(@rowcount2 AS VARCHAR)
        sbPrint.Append(string.Format("Rows sent to apinvdet: {0}",rowcount2));
//    END
    }
//-- Adjust numeric sign for credit invoices:
    sbSql.Clear();
    sbSql.Append("UPDATE apinvhdr");
    sbSql.Append("SET invoice_amt = -(ABS(invoice_amt)),");
    sbSql.Append("invoice_tax = -(ABS(invoice_tax))");
    sbSql.Append("WHERE rec_status = 0 AND debit_credit = 'C'	AND (invoice_amt > 0.00 OR invoice_tax > 0.00)");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev, sbSql.ToString());

    sbSql.Clear();
    sbSql.Append("UPDATE D");
    sbSql.Append("SET D.quantity = -(ABS(D.quantity)),");
    sbSql.Append("D.price = ABS(D.price),");
    sbSql.Append("D.item_tax = ABS(D.item_tax)");
    sbSql.Append("FROM apinvdet D");
    sbSql.Append("INNER JOIN apinvhdr H ON H.invoice_num = D.invoice_num AND H.customer_num = D.customer_num AND H.vend_supplier_id = D.vend_supplier_id");
    sbSql.Append("WHERE H.rec_status = 0 AND H.debit_credit = 'C' AND (D.quantity > 0 OR D.price < 0.00 OR D.item_tax > 0.00)");

    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,"DROP TABLE ##InvoiceHeader");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,"DROP TABLE ##InvoiceDetail");
    clsSql.CmdNonQuery(Properties.Settings.Default.conDev,"DROP TABLE ##InvoiceSummary");

//-- DELETE FROM ftp_tmp
    if(debugFlag)
    {
//IF @DebugFlag = 1
//    BEGIN
//        PRINT 'Finished loading history tables. Move on to the next step.'
        sbPrint.Append("Finished loading history tables. Move on to the next step.");
//        PRINT 'Time:  ' + CONVERT(VARCHAR(25),GETDATE(),121)
        sbPrint.Append(string.Format("Time: {0}",DateTime.Now));
//    END
    }
    sbSql.Clear();
    sbSql.Append("INSERT IntegrationStaging.dbo._AdminProcessRunData (");   //	--ProcessRunPK,
    sbSql.Append("ServerName,ProcessName,ProcessDescription,ProcessStepID,ProcessStepName,ProcessStepDescription,");
    sbSql.Append("ProcessServerName,StepSourceServer,StepDestinationServer,ProcessStart,ProcessEnd,StepRowCount,ProcessStartOrEndStep,CurrentDateTime)");
    sbSql.Append("SELECT @@SERVERNAME,'apinvimp1_sp','Invoice import step 1 - read file data (Row count = apinvhdr)',4,'Procedure end',NULL,");
    sbSql.Append("'HANSOLO','HANSOLO','HANSOLO',GETDATE(),NULL,@rowcount1,'E',GETDATE()");

    sbSql.Clear();
    sbSql.Append("SELECT import_file_name FROM ftp_files WHERE file_status <> 1 AND CONVERT(VARCHAR(8),file_import_date,112) = CONVERT(VARCHAR(8),GETDATE(),112)");
    dsTemp2=clsSql.CmdDataset(Properties.Settings.Default.conDev, sbSql.ToString());
    rslt=(dsTemp2.Tables.Count==0 || dsTemp2.Tables[0].Rows.Count==0)?-2:1;
//IF EXISTS (SELECT import_file_name FROM ftp_files WHERE file_status <> 1 
//            AND CONVERT(VARCHAR(8),file_import_date,112) = CONVERT(VARCHAR(8),GETDATE(),112))
//    RETURN -2
//ELSE RETURN 0

//--SELECT 'All detail records in temp table:'
//--SELECT * FROM ##InvoiceDetail ORDER BY invoice_num, rec_id
//--SELECT * FROM ##InvoiceHeader ORDER BY invoice_num, rec_id
}




            return rslt;
        }
    }




}
