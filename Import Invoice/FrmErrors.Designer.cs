﻿namespace Import_Invoice
{
    partial class FrmErrors
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxErrors = new System.Windows.Forms.ListBox();
            this.lblErrCount = new System.Windows.Forms.Label();
            this.cmdSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbxErrors
            // 
            this.lbxErrors.FormattingEnabled = true;
            this.lbxErrors.Location = new System.Drawing.Point(12, 56);
            this.lbxErrors.Name = "lbxErrors";
            this.lbxErrors.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbxErrors.Size = new System.Drawing.Size(698, 459);
            this.lbxErrors.TabIndex = 0;
            // 
            // lblErrCount
            // 
            this.lblErrCount.AutoSize = true;
            this.lblErrCount.ForeColor = System.Drawing.Color.Red;
            this.lblErrCount.Location = new System.Drawing.Point(72, 27);
            this.lblErrCount.Name = "lblErrCount";
            this.lblErrCount.Size = new System.Drawing.Size(35, 13);
            this.lblErrCount.TabIndex = 1;
            this.lblErrCount.Text = "label1";
            // 
            // cmdSave
            // 
            this.cmdSave.Location = new System.Drawing.Point(510, 12);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Size = new System.Drawing.Size(75, 38);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save to Text File";
            this.cmdSave.UseVisualStyleBackColor = true;
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // FrmErrors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 575);
            this.Controls.Add(this.cmdSave);
            this.Controls.Add(this.lblErrCount);
            this.Controls.Add(this.lbxErrors);
            this.Name = "FrmErrors";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Invoice Errors";
            this.Load += new System.EventHandler(this.FrmErrors_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxErrors;
        private System.Windows.Forms.Label lblErrCount;
        private System.Windows.Forms.Button cmdSave;


    }
}