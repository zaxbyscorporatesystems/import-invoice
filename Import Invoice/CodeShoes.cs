﻿using System;
using System.IO;
using System.Data;
using System.Text;
using Microsoft.VisualBasic.FileIO;

namespace Import_Invoice
{
    class CodeShoes : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        private enum FyleItems
        {
            Type,
            Invoice,
            Date,
            Unit,
            Employee,
            Employeeid,
            Style,
            Size,
            Qty,
            Price,
            Shipping,
            Tax ,
            D, 
            A,
            Total,
            Ref,
            Account,
            Dept
        };

        // properties
        public string InputFile
        {
            set;
            get;
        }
        public string ImportSource
        {
            set;
            get;
        }
        public int NumberRecords
        {
            set;
            get;
        }

        public DataTable DtHeader
        {
            set;
            get;
        }
        public DataTable DtDetail
        {
            set;
            get;
        }
        public DataTable DtSummary
        {
            set;
            get;
        }

        // methods
        public string VerifyFile()
        {
            string rsltVerify = string.Empty,
                type=string.Empty,
                invoice = string.Empty,
                date = string.Empty,
                unit=string.Empty,
                employee= string.Empty,
                employeeId = string.Empty,
                style=string.Empty,
                size=string.Empty,
                adj=string.Empty,
                iref=string.Empty,
                account=string.Empty,
                dept=string.Empty,
                // header lines items-------------------------------------
                zfiCustNum = string.Empty,
                custPoNum = string.Empty,
                debitCredit = string.Empty,
                dteAdded = string.Empty,
                createdBy = string.Empty,
                invoiceNum = string.Empty,
                dteDelivery = string.Empty,
                dteInvoice = string.Empty,
                invTotal = string.Empty,
                invTax = string.Empty,
                invFreight = string.Empty,
                creditApplyToNum = string.Empty,
                // detail line items-------------------------------------- 
                dItemNum = string.Empty,
                dItemDesc = string.Empty,
                dQuantity = string.Empty,
                dUnitOfMeasure = string.Empty,
                dPrice = string.Empty,
                dItemTax = string.Empty,
                dOtherNumeric = string.Empty,
                dOptIdentifier = string.Empty,
                dCcustomDefinition = string.Empty,
                dEndOfRecord = string.Empty,
                // summary line items-------------------------------------
                sTotInvoiceLines = string.Empty,
                sOptionalInfo = string.Empty,
                sEndOfRecord = string.Empty;
            decimal quantity = 0,
                price = 0,
                shipping=0,
                tax= 0,
                down=0,
                total = 0,
                totalGross=0,
                totalTax=0,
                totalShipping=0;

            DataTable dtFyleData = new DataTable();
            dtFyleData.Columns.Add("type");
            dtFyleData.Columns.Add("invoice");
            dtFyleData.Columns.Add("date");
            dtFyleData.Columns.Add("unit");
            dtFyleData.Columns.Add("employee");
            dtFyleData.Columns.Add("employeeID");
            dtFyleData.Columns.Add("style");
            dtFyleData.Columns.Add("size");
            dtFyleData.Columns.Add("quantity", typeof(decimal));
            dtFyleData.Columns.Add("price", typeof(decimal));
            dtFyleData.Columns.Add("shipping", typeof(decimal));
            dtFyleData.Columns.Add("tax", typeof(decimal));
            dtFyleData.Columns.Add("down", typeof(decimal));
            dtFyleData.Columns.Add("adj");
            dtFyleData.Columns.Add("total", typeof(decimal));
            dtFyleData.Columns.Add("ref");
            dtFyleData.Columns.Add("account");
            dtFyleData.Columns.Add("dept");

            DataTable dtInvoiceHeader = new DataTable();
            dtInvoiceHeader.Columns.Add("rec_id", typeof(int));
            dtInvoiceHeader.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceHeader.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("customer_po");                   // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("debit_or_credit");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("date_added");                    // CHAR(8),
            dtInvoiceHeader.Columns.Add("added_by");                      // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceHeader.Columns.Add("date_delivered");                // CHAR(8),
            dtInvoiceHeader.Columns.Add("date_invoice");                  // CHAR(8),
            dtInvoiceHeader.Columns.Add("invoice_amt");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_tax");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_freight");               // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("credit_apply_to_num");           // VARCHAR(8),
            dtInvoiceHeader.Columns.Add("record_end_char");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceHeader.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("status_flag", typeof(int));      // SMALLINT

            DataTable dtInvoiceDetail = new DataTable();
            dtInvoiceDetail.Columns.Add("rec_id", typeof(int));           // INT,
            dtInvoiceDetail.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceDetail.Columns.Add("sequence_id", typeof(int));      // INT,
            dtInvoiceDetail.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceDetail.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceDetail.Columns.Add("item_number");                   // VARCHAR(6),
            dtInvoiceDetail.Columns.Add("item_desc");                     // VARCHAR(80),
            dtInvoiceDetail.Columns.Add("account_code");                  // VARCHAR(32),
            dtInvoiceDetail.Columns.Add("quantity", typeof(decimal));     // DECIMAL(16,4),
            dtInvoiceDetail.Columns.Add("uom");                           // VARCHAR(2),
            dtInvoiceDetail.Columns.Add("price");                         // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax");                      // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax_percent");              // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("custom_definitions");            // VARCHAR(162),
            dtInvoiceDetail.Columns.Add("d_record_end_char");             // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceDetail.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceDetail.Columns.Add("status_flag", typeof(int));      // SMALLINT)

            DataTable dtInvoiceSummary = new DataTable();
            dtInvoiceSummary.Columns.Add("rec_id", typeof(int));          // INT,
            dtInvoiceSummary.Columns.Add("record_type");                  // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("invoice_num");                  // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("vend_customer_num");            // VARCHAR(15),
            dtInvoiceSummary.Columns.Add("vend_supplier_id");             // VARCHAR(5),
            dtInvoiceSummary.Columns.Add("total_invoice_lines");          // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("s_record_end_char");            // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("import_file_id", typeof(int));  // INT,
            dtInvoiceSummary.Columns.Add("import_source");                // VARCHAR(10))

            if (InputFile.Trim().Length > 0 && File.Exists(InputFile))
            {
                //string[] fyleLynes = File.ReadAllLines(InputFile);
                int recId = 0,
                    numRecs = 0;
                //ImportSource = InputFile.Substring(InputFile.LastIndexOf("\\"));
                ImportSource = "SHOES";
                bool pastFirst = false;
                DataTable dtExcel = new DataTable();
                using (TextFieldParser parser = new TextFieldParser(InputFile))
                {
                    parser.Delimiters = new string[] { "," };
                    parser.HasFieldsEnclosedInQuotes = true;
                    while (true)
                    {
                        string[] parts = parser.ReadFields();
                        if (parts == null)
                        {
                            break;
                        }
                        if (pastFirst)
                        {
                            dtExcel.Rows.Add(parts);
                        }
                        else
                        {
                            foreach (string cHead in parts)
                            {
                                dtExcel.Columns.Add(cHead, typeof(string));
                            }
                            pastFirst = true;
                        }
                    }
                }

                recId = 0;
                foreach (DataRow dRow in dtExcel.Rows)       // (string fLyne in fyleLynes)
                {
                    string[] datas = new string[dtExcel.Columns.Count];
                    int cntElem = 0;
                    foreach (object oElement in dRow.ItemArray)
                    {
                        datas[cntElem] = $"{oElement}";
                        cntElem++;
                    }
                    recId++;

                    if (pastFirst)
                    {
                        type = datas[(int)FyleItems.Type].Trim();
                        invoice = datas[(int)FyleItems.Invoice].Trim();
                        date = DteToEight(datas[(int)FyleItems.Date].Trim());
                        unit = datas[(int)FyleItems.Unit].Trim();
                        employee = datas[(int)FyleItems.Employee].Trim();
                        employeeId = datas[(int)FyleItems.Employeeid].Trim();
                        style = datas[(int)FyleItems.Style].Trim();
                        size = datas[(int)FyleItems.Size].Trim();
                        quantity = decimal.TryParse(datas[(int)FyleItems.Qty].Trim(), out quantity) ? quantity : 0;
                        price = decimal.TryParse(datas[(int)FyleItems.Price].Trim(), out price) ? price : 0;
                        shipping = decimal.TryParse(datas[(int)FyleItems.Shipping].Trim(), out shipping) ? shipping : 0;
                        tax = decimal.TryParse(datas[(int)FyleItems.Tax].Trim(), out tax) ? tax : 0;
                        down = decimal.TryParse(datas[(int)FyleItems.D].Trim(), out down) ? down : 0;
                        adj = datas[(int)FyleItems.A].Trim();
                        total = decimal.TryParse(datas[(int)FyleItems.Total].Trim(), out total) ? total : 0;
                        iref = datas[(int)FyleItems.Ref].Trim();
                        
                        account = datas[(int)FyleItems.Account].Trim();
                        dept = datas[(int)FyleItems.Dept].Trim();
                        dtFyleData.Rows.Add(
                            type
                            , invoice
                            , date
                            , unit
                            , employee
                            , employeeId
                            ,style
                            ,size
                            , quantity
                            , price
                            , shipping
                            , tax
                            , down
                            ,adj
                            , total
                            , iref
                            ,account
                            ,dept
                            );
                    }
                    //NumberRecords
                    pastFirst = true;
                }
                // get the invoice numbers
                DataTable dtInvoices = new DataTable();
                dtInvoices = dtFyleData.DefaultView.ToTable(true, "invoice");
                // for each invoice, create a header rec 
                recId = 0;
                foreach (DataRow drInvNum in dtInvoices.Rows)
                {
                    recId++;
                    invoice = drInvNum["invoice"].ToString().Trim();
                    DataRow[] drLineItems = dtFyleData.Select($"invoice='{invoice}'");
                    // get the invoice totals
                    totalShipping = 0;
                    totalTax = 0;
                    totalGross = 0;
                    foreach (DataRow drLyneItem in drLineItems)
                    {
                        totalTax += tax;
                        totalGross += total;
                        totalShipping += shipping;
                    }

                    // header rec to datatable
                    custPoNum = string.Empty;
                    dtInvoiceHeader.Rows.Add(
                        recId
                        , "H"
                        , ImportSource
                        , drLineItems[0]["unit"].ToString().Trim()
                        , custPoNum      //custPONum.Trim()
                        , "I"       //debitCredit
                        , drLineItems[0]["date"].ToString().Trim()       //string.Format("{0:0000}{1:00}{2:00}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)        //dteAdded
                        , String.Empty      //createdBy
                        , invoice
                        , drLineItems[0]["date"].ToString().Trim()       // dteDelivery
                        , drLineItems[0]["date"].ToString().Trim()       // dteInvoice
                        , totalGross.ToString()
                        , totalTax.ToString()
                        ,totalShipping.ToString()            //invFreight
                        , string.Empty      //creditApplyToNum
                        , string.Empty      //lyneItem
                        , 0
                        , ImportSource
                        , 0
                        );

                    // load detail line for invoice...
                    numRecs = 0;
                    string desc = string.Empty;
                    foreach (DataRow drLyneItem in drLineItems)
                    {
                        numRecs++;
                        desc = drLyneItem["style"].ToString().Trim() +" Size:"+ drLyneItem["size"].ToString().Trim();
                        dtInvoiceDetail.Rows.Add(
                                recId
                                , "D"
                                , drLyneItem["invoice"].ToString().Trim()      //invoiceNum
                                , numRecs
                                , drLyneItem["unit"].ToString().Trim()      //  zfiCustNum.Trim()
                                , ImportSource
                                , drLyneItem["style"].ToString().Trim()      //dItemNum.Trim()
                                , desc      //dItemDesc.Trim()
                                , "account_code"
                                , quantity      // makeDecimal(drLyneItem["quantity"].ToString().Trim())
                                , "EA"                      //dUnitOfMeasure
                                , price     // makeDecimal(drLyneItem["price"].ToString().Trim())     //dPrice
                                , tax       //makeDecimal(drLyneItem["tax"].ToString().Trim())     //dItemTax
                                , shipping
                                , dept
                                , string.Empty      //dEndOfRecord
                                , 0
                                , ImportSource
                                , 0
                                );

                    }
                    dtInvoiceSummary.Rows.Add(
                                recId
                                , "S"
                                , drLineItems[0]["invoice"].ToString().Trim()      //invoiceNum
                                , drLineItems[0]["unit"].ToString().Trim()      //zfiCustNum.Trim()
                                , ImportSource
                                , numRecs.ToString()
                                , string.Empty      //sEndOfRecord
                                , 0
                                , ImportSource
                                );
                }
            }

            DtHeader = dtInvoiceHeader;
            DtDetail = dtInvoiceDetail;
            DtSummary = dtInvoiceSummary;
            return rsltVerify;
        }

        string FrmtDateMdy(object oDtein)
        {
            string rslt = string.Empty,
                sTmp = string.Empty;
            try
            {
                sTmp = oDtein.ToString();
                rslt = $"{sTmp.Substring(4, 2):00}/{sTmp.Substring(6):00}/{sTmp.Substring(0, 4):0000}";
                if (rslt.Trim().Length < 8)
                {
                    rslt = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
        public string DteToEight(object oDte)
        {
            string rslt = string.Empty;
            try
            {
                rslt = oDte.ToString();
                if (rslt.Contains("/"))
                {
                    string[] dtes = rslt.Split('/');
                    rslt = dtes[2].PadLeft(4, '0') + dtes[0].PadLeft(2, '0') + dtes[1].PadLeft(2, '0');
                }
                else
                {
                    rslt = "19000101";
                }
            }
            catch
            {
                rslt = string.Empty;
            }

            return rslt;
        }
        decimal MakeDecimal(string decIn)
        {
            decimal rslt = 0;
            try
            {
                string tmp = decIn.Replace("$", "");
                tmp = tmp.Replace(",", "");
                rslt = decimal.TryParse(tmp, out rslt) ? rslt : 0;
            }
            catch
            {
            }
            return rslt;
        }

    }
}
