﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Import_Invoice
{
    public partial class FrmErrors : Form
    {
        public StringBuilder SbErrors
        { set; get; }

        public string InFyle
        { set; get; }

        public FrmErrors()
        {
            InitializeComponent();
        }

        private void FrmErrors_Load(object sender, EventArgs e)
        {
            string[] errLynes = SbErrors.ToString().Split('\n');
            lblErrCount.Text = $"Number of errors found {errLynes.GetUpperBound(0)}";
            foreach (string lyne in errLynes)
            {
                lbxErrors.Items.Add(lyne);
            }
            this.Update();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            string outFyle = InFyle.Trim().ToUpper();
            if (outFyle.Contains("MOOD"))
            {
                outFyle = outFyle.Replace(".IN", " error list.err");    //outFyle.Substring(0, outFyle.LastIndexOf("\\") + 1) + "error list.txt";
            }
            else
            {
                outFyle = outFyle.Replace(".TXT", " error list.err");    //outFyle.Substring(0, outFyle.LastIndexOf("\\") + 1) + "error list.txt";
            }
            if (File.Exists(outFyle))
            {
                File.Delete(outFyle);
            }
            FileStream fStream = new FileStream(outFyle, FileMode.Append);
            StreamWriter twfPath = new StreamWriter(fStream);
            foreach (string str in lbxErrors.Items)
            {
                twfPath.WriteLine(str);
            }
            twfPath.Close();
            fStream.Close();
            MessageBox.Show($"Location of error file: {outFyle}", "File Written", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
        }
    }
}
