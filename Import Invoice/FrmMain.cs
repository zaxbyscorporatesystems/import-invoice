﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Diagnostics;


namespace Import_Invoice
{
    public partial class FrmMain : Form
    {
        string _fyleIn = string.Empty,
            _ftpPath =string.Empty;      // @"i:\external\VendorInvoices\";
        int _curPos = 0;
        bool _isLoad = false;
        DataTable _dtHeaders = new DataTable(),
            _dtDetails = new DataTable(),
            _dtSummary=new DataTable();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            connectToDMZ();
            _ftpPath = Properties.Settings.Default.ftpPath;
            PopulateInputFiles();
        }


        private void connectToDMZ()
        {
            try
            {

                System.Diagnostics.Process objProcess = new System.Diagnostics.Process();
                string _mydir = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                string _mypath = _mydir + @"\connect.bat";
                objProcess.StartInfo.FileName = _mypath;
                objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;    // to hide the command window popping up
                objProcess.Start();
                objProcess.WaitForExit();    // Gives time for the process to complete operation.
                                             // After code is executed, call the dispose() method
                objProcess.Dispose();

                string[] theFolders = Directory.GetDirectories(@"\\goober\ftpusers");
            }
            catch (Exception ex)
            {
                string _errmsg = "Import Invoices job failed in ConnectToDMZ():  " + ex.Message.ToString();
                using (CodeEmail clsEmail = new CodeEmail())
                {

                    clsEmail.ErrMsg = _errmsg;
                    bool _sendmail=clsEmail.SendEmail();


                }
                System.Environment.Exit(0);
            }


        }



        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlgFyleIn = new OpenFileDialog
            {
                RestoreDirectory = true,
                Filter = @"Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };
            if (dlgFyleIn.ShowDialog() == DialogResult.OK)
            {
                _fyleIn = dlgFyleIn.FileName.Trim();
                tbxImportFile.Text = _fyleIn;
            }
            cmdLoadFyle.Visible = (_fyleIn.Length > 0 && File.Exists(_fyleIn));
            _isLoad = true;
            dgrdDetail.DataSource = null;
            dgrdHeader.DataSource = null;
            dgrdSummary.DataSource = null;
            _isLoad = false;
        }

        private void cmdLoadFyle_Click(object sender, EventArgs e)
        {
            int numRecords = 0;
            // for invoice
            string importSource = string.Empty,
                 zfiCustNum = string.Empty,
                 custPoNum = string.Empty,
                 debitCredit = string.Empty,
                 dteAdded = string.Empty,
                 createdBy = string.Empty,
                 invoiceNum = string.Empty,
                 dteDelivery = string.Empty,
                 dteInvoice = string.Empty,
                 invTotal = string.Empty,
                 invTax = string.Empty,
                 invFreight = string.Empty,
                 creditApplyToNum = string.Empty;

            _fyleIn = tbxImportFile.Text.Trim();
            if (_fyleIn.Length == 0)
            {
                return;
            }
            Cursor = Cursors.WaitCursor;
            // verify file
            if (tbxImportFile.Text.Trim().ToUpper().Contains("ECO"))
            {
                using (CodeEcoLab clsCode = new CodeEcoLab())
                {
                    clsCode.InputFile = tbxImportFile.Text.Trim();
                    string status = clsCode.VerifyFileFormat();
                    if (status.Trim().Length > 0)
                    {
                        // error in file format
                        Cursor = Cursors.Default;
                        MessageBox.Show(status, @"Format Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _isLoad = true;
                    numRecords = clsCode.NumberRecords;
                    importSource = clsCode.ImportSource;
                    _dtHeaders = clsCode.DtHeader;
                    _dtDetails = clsCode.DtDetail;
                    _dtSummary = clsCode.DtSummary;
                    _isLoad = false;
                }
            }
            else if (tbxImportFile.Text.Trim().ToUpper().Contains("NUCO2"))
            {
                using (CodeNuco2 clsCode = new CodeNuco2())
                {
                    clsCode.InputFile = tbxImportFile.Text.Trim();
                    string status = clsCode.VerifyFile();
                    if (status.Trim().Length > 0)
                    {
                        // error in file format
                        Cursor = Cursors.Default;
                        MessageBox.Show(status, @"Format Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _isLoad = true;
                    importSource = "NUCO2";
                    _dtHeaders = clsCode.DtHeader;
                    _dtDetails = clsCode.DtDetail;
                    _dtSummary = clsCode.DtSummary;
                    numRecords = _dtHeaders.Rows.Count + _dtDetails.Rows.Count + _dtSummary.Rows.Count;
                    _isLoad = false;
                }
            }
            else if (tbxImportFile.Text.Trim().ToUpper().Contains("SHOES"))
            {
                using (CodeShoes clsCode = new CodeShoes())
                {
                    clsCode.InputFile = tbxImportFile.Text.Trim();
                    string status = clsCode.VerifyFile();
                    if (status.Trim().Length > 0)
                    {
                        // error in file format
                        Cursor = Cursors.Default;
                        MessageBox.Show(status, @"Format Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _isLoad = true;
                    importSource = "SHOES";
                    _dtHeaders = clsCode.DtHeader;
                    _dtDetails = clsCode.DtDetail;
                    _dtSummary = clsCode.DtSummary;
                    numRecords = _dtHeaders.Rows.Count + _dtDetails.Rows.Count + _dtSummary.Rows.Count;
                    _isLoad = false;
                }
            }
            else if (tbxImportFile.Text.Trim().ToUpper().Contains("CINTA"))
            {
                using (CodeCintas clsCode = new CodeCintas())
                {
                    clsCode.InputFile = tbxImportFile.Text.Trim();
                    string status = clsCode.VerifyFile();
                    if (status.Trim().Length > 0)
                    {
                        // error in file format
                        Cursor = Cursors.Default;
                        MessageBox.Show(status, @"Format Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _isLoad = true;
                    numRecords = clsCode.NumberRecords;
                    importSource = "CINTA";
                    _dtHeaders = clsCode.DtHeader;
                    _dtDetails = clsCode.DtDetail;
                    _dtSummary = clsCode.DtSummary;
                    _isLoad = false;

                }
            }
            else if (
                tbxImportFile.Text.Trim().ToUpper().Contains("IFH")
               || tbxImportFile.Text.Trim().ToUpper().Contains("PFS-FOODSTAR")
                || tbxImportFile.Text.Trim().ToUpper().Contains("PFG")
                || tbxImportFile.Text.Trim().ToUpper().Contains("LITTLEROCK")
                || tbxImportFile.Text.Trim().ToUpper().Contains("TEMPLE")
                )
            {
                using (CodeIfh clsCode = new CodeIfh())
                {
                    clsCode.InputFile = tbxImportFile.Text.Trim();
                    string status = clsCode.VerifyFileFormat();
                    if (status.Trim().Length > 0)
                    {
                        // error in file format
                        Cursor = Cursors.Default;
                        MessageBox.Show(status, @"Format Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _isLoad = true;
                    numRecords = clsCode.NumberRecords;
                    importSource = tbxImportFile.Text.Trim().ToUpper().Contains("IFH") ? "PFG_IFH" : "PFG_GROUP";
                    _dtHeaders = clsCode.DtHeader;
                    _dtDetails = clsCode.DtDetail;
                    _dtSummary = clsCode.DtSummary;
                    _isLoad = false;
                }
            }
            else
            {
                using (CodeFile clsCode = new CodeFile())
                {
                    clsCode.InputFile = tbxImportFile.Text.Trim();
                    string status = clsCode.VerifyFileFormat();
                    if (status.Trim().Length > 0)
                    {
                        // error in file format
                        Cursor = Cursors.Default;
                        MessageBox.Show(status, @"Format Errors Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _isLoad = true;
                    numRecords = clsCode.NumberRecords;
                    importSource = clsCode.ImportSource;
                    _dtHeaders = clsCode.DtHeader;
                    _dtDetails = clsCode.DtDetail;
                    _dtSummary = clsCode.DtSummary;
                    _isLoad = false;
                }
            }
            _isLoad = true;
            dgrdHeader.DataSource = _dtHeaders;
            dgrdDetail.DataSource = _dtDetails;
            dgrdSummary.DataSource = _dtSummary;
            _isLoad = false;
            lblHeader.Text = $"Header records: {dgrdHeader.Rows.Count}";
            lblDetail.Text = $"Detail records: {dgrdDetail.Rows.Count}";
            lblSummary.Text = $"Summary records: {dgrdSummary.Rows.Count}";
            // copy files
            StringBuilder sbSql = new StringBuilder();

            string fyleUsed = tbxImportFile.Text.Trim();
            //fylePath = fyleUsed.Substring(0, fyleUsed.LastIndexOf("\\", StringComparison.Ordinal));
            string fylePath = Path.GetDirectoryName(fyleUsed);
            //fyleUsedFullName =fyleUsed.Substring(fyleUsed.LastIndexOf("\\", StringComparison.Ordinal)+1);
            string fyleUsedFullName = Path.GetFileName(fyleUsed);
            fyleUsedFullName = fyleUsedFullName.Replace("'", "");
            string strCompleteFyle = string.Format(@"{1}\loaded\{0}", fyleUsedFullName, fylePath.Substring(0, fylePath.LastIndexOf("\\", StringComparison.Ordinal)));  //      '\\hansolo\APInvoicesPFG$\Completed\
            FileInfo fylInfo = new FileInfo(fyleUsed);

            #region commented ftp inserts
            // load to matrixdev
            // load ftp_files
            int fyleId;

            if (!cbxNoSQL.Checked)
            {
                object oTmp;
                //string conSql = rdoToMatrix.Checked ? Properties.Settings.Default.conProdView : Properties.Settings.Default.conDev;
                string conSql = rdoToMatrix.Checked ? Properties.Settings.Default.conProdView : Properties.Settings.Default.conProdView;
                using (CodeSql clsSql = new CodeSql())
                {
                // purge ftp_tmp
                    sbSql.Clear();
                    if (!rdoToMatrix.Checked)
                    {
                        //sbSql.Append("DELETE ftp_tmp FROM ftp_tmp A");
                        //sbSql.Append(" INNER JOIN ftp_files B ON B.import_file_id = A.import_file_id");
                        //sbSql.Append(" WHERE A.import_file_id IS NOT NULL AND B.file_status = 1 AND B.file_import_date IS NOT NULL");
                        //clsSql.CmdNonQuery(conSql, sbSql.ToString());
                    }
                    
                    if (importSource.ToUpper().Trim() == "NUCO2"
                        || importSource.ToUpper().Trim() == "CINTA"
                        || importSource.ToUpper().Trim() == "ECLAB")
                    {
                        numRecords = _dtSummary.Rows.Count + _dtHeaders.Rows.Count + _dtDetails.Rows.Count;
                    }
                    sbSql.Clear();
                    sbSql.Append("INSERT INTO [ftp_files] (");
                    sbSql.Append("[import_type]");
                    sbSql.Append(",[import_source]");
                    sbSql.Append(",[store_id]");
                    sbSql.Append(",[import_file_name]");
                    sbSql.Append(",[file_path]");
                    sbSql.Append(",[file_create_date]");
                    sbSql.Append(",[file_modify_date]");
                    sbSql.Append(",[file_num_records]");
                    sbSql.Append(",[file_detect_date]");
                    sbSql.Append(",[file_status]");
                    //sbSql.Append(",[file_process_error]");
                    //sbSql.Append(",[file_import_date]");
                    sbSql.Append(") VALUES (");
                    sbSql.Append("'APINVOICE'");      //      <import_type, varchar(15),>
                    sbSql.Append($",'{importSource}'");  //      ,<import_source, varchar(10),>
                    sbSql.Append(",'None'");       //      ,<store_id, char(5),>
                    sbSql.Append($",'{fyleUsedFullName}'");        //      ,<import_file_name, varchar(100),>
                    sbSql.Append($",'{fylePath}'");       //      ,<file_path, varchar(1000),>
                    sbSql.Append($",'{fylInfo.CreationTime}'");       //      ,<file_create_date, datetime,>
                    sbSql.Append($",'{fylInfo.LastAccessTime}'");       //      ,<file_modify_date, datetime,>
                    sbSql.Append($",{numRecords}");    //      ,<file_num_records, int,>
                    sbSql.Append($",'{DateTime.Now}'");      //      ,<file_detect_date, datetime,>
                    sbSql.Append($",{0}");     //      ,<file_status, smallint,>
                    //      ,<file_process_error, int,>
                    //sbSql.Append(string.Format(",'{0}'", null));       //      ,<file_import_date, datetime,>
                    sbSql.Append(")");
                    if (clsSql.CmdNonQuery(conSql, sbSql.ToString()) == 1)
                    {
                        // get file [import_file_id]
                        oTmp = clsSql.CmdScalar(conSql,
                            $"select [import_file_id] FROM [ftp_files] where [import_file_name]='{fyleUsedFullName}'");
                        int.TryParse(oTmp.ToString(), out fyleId);
                        if (fyleUsed.Trim().Length > 0 && File.Exists(fyleUsed))
                        {
                            if (importSource.ToUpper().Trim() == "NUCO2"
                                || importSource.ToUpper().Trim() == "CINTA"
                                || importSource.ToUpper().Trim() == "ECLAB")
                            {
                                StringBuilder sbNwLyne = new StringBuilder();
                                // create flyne from grids
                                foreach (DataRow dHrow in _dtHeaders.Rows)
                                {
                                    // H rec from dgrdHeader
                                    sbNwLyne.Clear();
                                    sbNwLyne.Append("H");
                                    sbNwLyne.Append($"{dHrow["vend_supplier_id"]}".PadLeft(5, ' '));              // VARCHAR(5),
                                    sbNwLyne.Append($"{dHrow["vend_customer_num"]}".PadLeft(20, ' '));         // VARCHAR(20),
                                    sbNwLyne.Append($"{dHrow["customer_po"]}".PadLeft(5, ' '));                   // VARCHAR(5),
                                    sbNwLyne.Append($"{dHrow["debit_or_credit"]}".PadLeft(1, ' '));//""               // VARCHAR(1),
                                    if (importSource.ToUpper().Trim() == "CINTA")
                                    {
                                        sbNwLyne.Append($"{dHrow["date_added"]}");//""                 // CHAR(8),
                                        sbNwLyne.Append($"{dHrow["added_by"]}".PadLeft(10, ' '));//""                    // VARCHAR(10),
                                        sbNwLyne.Append($"{dHrow["invoice_num"]}".PadLeft(20, ' '));//""                 // VARCHAR(20),
                                        sbNwLyne.Append($"{dHrow["date_delivered"]}");//""                // CHAR(8),
                                        sbNwLyne.Append($"{dHrow["date_invoice"]}");//""                  // CHAR(8),
                                    }
                                    else
                                    {
                                        using (CodeNuco2 clsTemp = new CodeNuco2())
                                        {
                                            sbNwLyne.Append($"{clsTemp.DteToEight(dHrow["date_added"])}".PadLeft(8, ' '));//""                 // CHAR(8),
                                            sbNwLyne.Append($"{dHrow["added_by"]}".PadLeft(10, ' '));//""                    // VARCHAR(10),
                                            sbNwLyne.Append($"{dHrow["invoice_num"]}".PadLeft(20, ' '));//""                 // VARCHAR(20),
                                            sbNwLyne.Append($"{clsTemp.DteToEight(dHrow["date_delivered"])}".PadLeft(8, ' '));//""                // CHAR(8),
                                            sbNwLyne.Append($"{clsTemp.DteToEight(dHrow["date_invoice"])}".PadLeft(8, ' '));//""                  // CHAR(8),
                                        }
                                    }
                                    sbNwLyne.Append($"{dHrow["invoice_amt"]}".PadLeft(11, ' '));//""                   // VARCHAR(11),
                                    sbNwLyne.Append($"{dHrow["invoice_tax"]}".PadLeft(11, ' '));//""                   // VARCHAR(11),
                                    sbNwLyne.Append($"{dHrow["invoice_freight"]}".PadLeft(11, ' '));//""               // VARCHAR(11),
                                    sbNwLyne.Append($"{dHrow["credit_apply_to_num"]}".PadLeft(20, ' '));//""           // VARCHAR(20),
                                    sbNwLyne.Append("*");
                                    //insert
                                    sbSql.Clear();
                                    sbSql.Append("INSERT INTO [ftp_tmp] (");
                                    sbSql.Append("[import_file_id]");
                                    sbSql.Append(",[import_file_name]");
                                    sbSql.Append(",[field1]");
                                    sbSql.Append(") VALUES (");
                                    sbSql.Append($"{fyleId}");      //      <import_file_id, int,>
                                    sbSql.Append($",'{fyleUsedFullName}'");      //      ,<import_file_name, varchar(100),>
                                    sbSql.Append($",'{sbNwLyne}'");   //clsCode.MakeOldLineFormat(fLyne)));      //      ,<field1, varchar(8000),>
                                    sbSql.Append(")");
                                    if (clsSql.CmdNonQuery(conSql, sbSql.ToString()) == 1)
                                    {

                                        // D recs from dgrdDetail
                                        string expression = $"invoice_num = '{dHrow["invoice_num"].ToString().Trim()}'";
                                        DataRow[] foundRows = _dtDetails.Select(expression);
                                        foreach (DataRow dDRow in foundRows)
                                        {
                                            sbNwLyne.Clear();
                                            sbNwLyne.Append("D");
                                            sbNwLyne.Append($"{dDRow["item_number"]}".PadLeft(30, ' '));//""                   // VARCHAR(6),
                                            sbNwLyne.Append($"{dDRow["item_desc"]}".PadLeft(80, ' '));//"item_desc"                     // VARCHAR(80),
                                            sbNwLyne.Append($"{dDRow["quantity"]}".PadLeft(11, ' '));//"", typeof(decimal)     // DECIMAL(16,4),
                                            sbNwLyne.Append($"{dDRow["uom"]}".PadLeft(2, ' '));//""                          // VARCHAR(2),
                                            sbNwLyne.Append($"{dDRow["price"]}".PadLeft(11, ' '));//""                         // VARCHAR(11),
                                            sbNwLyne.Append($"{dDRow["item_tax"]}".PadLeft(11, ' '));//""                      // VARCHAR(11),
                                            sbNwLyne.Append($"{dDRow["item_tax_percent"]}".PadLeft(11, ' '));//""              // VARCHAR(11),
                                            sbNwLyne.Append($"{dDRow["custom_definitions"]}".PadLeft(30, ' '));//""            // VARCHAR(162),
                                            sbNwLyne.Append($"{string.Empty}".PadLeft(10, ' '));//""            // VARCHAR(162),
                                            sbNwLyne.Append("*");
                                            sbSql.Clear();
                                            sbSql.Append("INSERT INTO [ftp_tmp] (");
                                            sbSql.Append("[import_file_id]");
                                            sbSql.Append(",[import_file_name]");
                                            sbSql.Append(",[field1]");
                                            sbSql.Append(") VALUES (");
                                            sbSql.Append($"{fyleId}");      //      <import_file_id, int,>
                                            sbSql.Append($",'{fyleUsedFullName}'");      //      ,<import_file_name, varchar(100),>
                                            sbSql.Append($",'{sbNwLyne.ToString().Replace("'", " ")}'");   //clsCode.MakeOldLineFormat(fLyne)));      //      ,<field1, varchar(8000),>
                                            sbSql.Append(")");
                                            if(clsSql.CmdNonQuery(conSql, sbSql.ToString())!=1)
                                            {
                                                MessageBox.Show(sbSql.ToString(), @"Detail not inserted", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                        }
                                        // Use the Select method to find all rows matching the filter.
                                        DataRow[] foundSummary = _dtSummary.Select(expression);
                                        foreach (DataRow dSRow in foundSummary)
                                        {
                                            // S rec from dgrdSummary
                                            sbNwLyne.Clear();
                                            sbNwLyne.Append("S");
                                            sbNwLyne.Append($"{dSRow["total_invoice_lines"]}".PadLeft(7, ' '));//""                   // VARCHAR(6),
                                            sbNwLyne.Append($"{string.Empty}".PadLeft(10, ' '));//""            // VARCHAR(162),
                                            sbNwLyne.Append("*");
                                            sbSql.Clear();
                                            sbSql.Append("INSERT INTO [ftp_tmp] (");
                                            sbSql.Append("[import_file_id]");
                                            sbSql.Append(",[import_file_name]");
                                            sbSql.Append(",[field1]");
                                            sbSql.Append(") VALUES (");
                                            sbSql.Append($"{fyleId}");      //      <import_file_id, int,>
                                            sbSql.Append($",'{fyleUsedFullName}'");      //      ,<import_file_name, varchar(100),>
                                            sbSql.Append($",'{sbNwLyne}'");   //clsCode.MakeOldLineFormat(fLyne)));      //      ,<field1, varchar(8000),>
                                            sbSql.Append(")");
                                            if (clsSql.CmdNonQuery(conSql, sbSql.ToString()) != 1)
                                            {
                                                MessageBox.Show(sbSql.ToString(), @"Summary not inserted", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bool cokeCredit = false;
                                string[] fyleLynes = File.ReadAllLines(fyleUsed);
                                using (CodeFile clsCode = new CodeFile())
                                {
                                    string dItemNum = string.Empty,
                                        hCustNum = string.Empty,
                                        hinvoiceNum = string.Empty,
                                        nwLyne = string.Empty;
                                    foreach (string fLyne in fyleLynes)
                                    {
                                        // 6/17/2014 may have to force file line into 'old' file format for H, D and S lines......
                                        // load ftp_tmp
                                        if (fLyne.Trim().Length > 0)
                                        {
                                            string firstChar = fLyne.Substring(0, 1).ToUpper();
                                            if (fLyne.Trim().Length > 0
                                                && (firstChar == "H" || firstChar == "D" || firstChar == "S"))
                                            {
                                                nwLyne = fLyne.Replace("'", " ");
                                                if (importSource.ToUpper().Trim() == "TRIMK"
                                                    || importSource.ToUpper().Trim() == "MBROS"
                                                    || importSource.ToUpper().Trim() == "HPI"
                                                    || importSource.ToUpper().Trim() == "FLOWR"
                                                    || importSource.ToUpper().Trim() == "COKE"
                                                   || importSource.ToUpper().Trim() == "MOODM")
                                                {
                                                    // remove leading 0 in header customer or detail product
                                                   // check moodmedia for empty fields
                                                   if (importSource.ToUpper().Trim() == "MOODM")
                                                   {
                                                      if (nwLyne.ToUpper().StartsWith("H"))
                                                      {
                                                         nwLyne = nwLyne.Substring(0, 86) + CheckForEmptyNumber(nwLyne.Substring(86, 11)) + CheckForEmptyNumber(nwLyne.Substring(97, 11)) + CheckForEmptyNumber(nwLyne.Substring(108, 11)) + nwLyne.Substring(119);
                                                      }
                                                      else if (nwLyne.ToUpper().StartsWith("D"))
                                                      {
                                                         nwLyne = nwLyne.Substring(0, 124) + CheckForEmptyNumber(nwLyne.Substring(124, 11)) + CheckForEmptyNumber(nwLyne.Substring(135, 11)) + CheckForEmptyNumber(nwLyne.Substring(146, 11)) + nwLyne.Substring(157);
                                                      }
                                                   }
                                                   if (nwLyne.ToUpper().StartsWith("H"))
                                                    {
                                                       // coke move minus to beginning of string and credit
                                                        // also set cokeCredit bool to examine in detail line

                                                        if (importSource.ToUpper().Trim() == "COKE"
                                                            && nwLyne.Substring(31,1).ToUpper()=="C")
                                                        {
                                                            cokeCredit = true;
                                                            if (nwLyne.Substring(86, 22).Contains("-"))
                                                            {
                                                                nwLyne = MoveMinusSigh(nwLyne, 86, 11);     // invoice amount
                                                                nwLyne = MoveMinusSigh(nwLyne, 97, 11);     // invoice total
                                                            }
                                                        }
                                                        // leading zeros in invoice, cust num
                                                        hCustNum = nwLyne.Substring(6, 20);
                                                        hCustNum = hCustNum.TrimStart('0');
                                                        hCustNum = hCustNum.PadRight(20, ' ');
                                                        hinvoiceNum = nwLyne.Substring(50, 20);
                                                        hinvoiceNum = hinvoiceNum.TrimStart('0');
                                                        if (importSource.ToUpper().Trim() == "FLOWR")
                                                        {
                                                            hCustNum = nwLyne.Substring(6, 20);
                                                            if (hCustNum.Trim().Length < 5)
                                                            {
                                                                hCustNum = hCustNum.Trim().PadLeft(5, '0');
                                                                hCustNum = hCustNum.PadRight(20, ' ');
                                                            }
                                                            // alpha year month to invoice number
                                                            string yrInv = nwLyne.Substring(78, 4);
                                                            string mthInv = nwLyne.Substring(82, 2);
                                                            string mthYr = string.Empty;
                                                            switch (yrInv)
                                                            {
                                                                case "2014":
                                                                    mthYr = "A";
                                                                    break;
                                                                case "2015":
                                                                    mthYr = "B";
                                                                    break;
                                                                case "2016":
                                                                    mthYr = "C";
                                                                    break;
                                                                case "2017":
                                                                    mthYr = "D";
                                                                    break;
                                                                case "2018":
                                                                    mthYr = "E";
                                                                    break;
                                                                case "2019":
                                                                    mthYr = "F";
                                                                    break;
                                                                case "2020":
                                                                    mthYr = "G";
                                                                    break;
                                                                case "2021":
                                                                    mthYr = "H";
                                                                    break;
                                                                case "2022":
                                                                    mthYr = "I";
                                                                    break;
                                                                case "2023":
                                                                    mthYr = "J";
                                                                    break;
                                                                case "2024":
                                                                    mthYr = "K";
                                                                    break;
                                                                default:
                                                                    mthYr = "x";
                                                                    break;
                                                            }
                                                            switch (mthInv)
                                                            {
                                                                case "01":
                                                                    mthYr += "A";
                                                                    break;
                                                                case "02":
                                                                    mthYr += "B";
                                                                    break;
                                                                case "03":
                                                                    mthYr += "C";
                                                                    break;
                                                                case "04":
                                                                    mthYr += "D";
                                                                    break;
                                                                case "05":
                                                                    mthYr += "E";
                                                                    break;
                                                                case "06":
                                                                    mthYr += "F";
                                                                    break;
                                                                case "07":
                                                                    mthYr += "G";
                                                                    break;
                                                                case "08":
                                                                    mthYr += "H";
                                                                    break;
                                                                case "09":
                                                                    mthYr += "I";
                                                                    break;
                                                                case "10":
                                                                    mthYr += "J";
                                                                    break;
                                                                case "11":
                                                                    mthYr += "K";
                                                                    break;
                                                                case "12":
                                                                    mthYr += "L";
                                                                    break;
                                                                default:
                                                                    mthYr += "x";
                                                                    break;
                                                            }
                                                            hinvoiceNum = hinvoiceNum.Trim() + mthYr;
                                                        }
                                                        hinvoiceNum = hinvoiceNum.PadRight(20, ' ');
                                                        nwLyne = nwLyne.Substring(0, 6) + hCustNum + nwLyne.Substring(26, 24) + hinvoiceNum + nwLyne.Substring(70);
                                                    }
                                                    else if (fLyne.ToUpper().StartsWith("D"))
                                                    {
                                                        if (cokeCredit)
                                                        {
                                                            nwLyne = MoveMinusSigh(nwLyne, 111, 11);
                                                        }
                                                        // leading zeros in item code
                                                        dItemNum = nwLyne.Substring(1, 30);              //item num
                                                        dItemNum = dItemNum.TrimStart('0');
                                                        dItemNum = dItemNum.PadRight(30, ' ');
                                                        nwLyne = nwLyne.Substring(0, 1) + dItemNum + nwLyne.Substring(31);
                                                    }
                                                    else if (fLyne.ToUpper().StartsWith("S"))
                                                    {
                                                        // if Summary line set cokeCredit to false
                                                        cokeCredit = false;
                                                    }
                                                }
                                                nwLyne=nwLyne.Replace("'"," ");
                                                sbSql.Clear();
                                                sbSql.Append("INSERT INTO [ftp_tmp] (");
                                                sbSql.Append("[import_file_id]");
                                                sbSql.Append(",[import_file_name]");
                                                sbSql.Append(",[field1]");
                                                sbSql.Append(") VALUES (");
                                                sbSql.Append($"{fyleId}");      //      <import_file_id, int,>
                                                sbSql.Append($",'{fyleUsedFullName}'");      //      ,<import_file_name, varchar(100),>
                                                sbSql.Append($",'{nwLyne}'");   //clsCode.MakeOldLineFormat(fLyne)));      //      ,<field1, varchar(8000),>
                                                sbSql.Append(")");
                                                clsSql.CmdNonQuery(conSql, sbSql.ToString());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // file name not found
                        }
                    }
                    else
                    {
                        // file not inserted
                    }
                    // run stored procedures
                    //clsSql.ExeStoredProc(conSql, "apinvimp1_sp");
                    //clsSql.ExeStoredProc(conSql, "apinvimp2_sp");
                    //clsSql.ExeStoredProc(conSql, "apinvimp3_sp");
                    //clsSql.ExeStoredProc(conSql, "apinvimp4_sp");
                    //clsSql.ExeStoredProc(conSql, "apinvimp5_sp");
                }
                if (!cbxNoSQL.Checked)    // rdoToMatrix.Checked)
                {
                    if (File.Exists(strCompleteFyle))
                    {
                        File.Delete(strCompleteFyle);
                    }
                    if (!rdoToMatrixDev.Checked)
                    {
                       File.Move(tbxImportFile.Text.Trim(), strCompleteFyle);
                    }
                }
                PopulateInputFiles();
            }
            #endregion

           string rwInvoice,
                errors,
                invoiceDte;
            StringBuilder sbErrors = new StringBuilder();
            foreach (DataRow drHead in _dtHeaders.Rows)
            {
                rwInvoice = drHead["invoice_num"].ToString().Trim();
                invoiceDte = drHead["date_invoice"].ToString().Trim();
                if (invoiceDte.Length < 6)
                {
                    invoiceDte = $"{DateTime.Now.Year}{DateTime.Now.Month:00}{DateTime.Now.Day:00}";
                }
                invoiceDte = $"{invoiceDte.Substring(4, 2)}/{invoiceDte.Substring(6, 2)}/{invoiceDte.Substring(0, 4)}";
                object oTemp = drHead["invoice_amt"];
                decimal amtHead = decimal.TryParse(oTemp.ToString(), out amtHead) ? amtHead : 0;
                oTemp = drHead["invoice_tax"];
                decimal amtTax=decimal.TryParse(oTemp.ToString(), out amtTax) ? amtTax : 0;
                oTemp = drHead["invoice_freight"];
                decimal amtFrt=decimal.TryParse(oTemp.ToString(), out amtFrt) ? amtFrt : 0;
                DataRow[] dtRows = _dtDetails.Select($"invoice_num='{rwInvoice}'");
                RecordToDebug($"{drHead["vend_supplier_id"]}\t{drHead["vend_customer_num"]}");

                decimal amtDetail = 0;
                foreach (DataRow dtDRow in dtRows)
                {
                    oTemp = dtDRow["quantity"];
                    decimal qty = decimal.TryParse(oTemp.ToString(), out qty) ? qty : 0;
                    oTemp = dtDRow["price"];
                    decimal amtUnitPrice=(decimal.TryParse(oTemp.ToString(), out amtUnitPrice) ? amtUnitPrice : 0);
                    //oTemp = dtDRow["item_tax"];
                    //amtIFHLineAdj = (decimal.TryParse(oTemp.ToString(), out amtIFHLineAdj) ? amtIFHLineAdj : 0);
                    oTemp = dtDRow["item_tax_percent"];
                    decimal amtOther=(decimal.TryParse(oTemp.ToString(), out amtOther) ? amtOther : 0);
                    decimal amtExt=(Math.Round(amtUnitPrice * qty, 2)) + amtOther;
                    if (tbxImportFile.Text.Trim().ToUpper().Contains("NUCO2"))
                    {
                        amtExt = (Math.Round(amtUnitPrice * qty, 2, MidpointRounding.AwayFromZero)) + amtOther;
                    }
                    else if (tbxImportFile.Text.Trim().ToUpper().Contains("SHOES"))
                    {
                        amtExt = (Math.Round(amtUnitPrice * qty, 2, MidpointRounding.AwayFromZero));    // +amtOther;
                    }
                    amtDetail += amtExt;
                }
                if (tbxImportFile.Text.Trim().ToUpper().Contains("MIL")
                   || tbxImportFile.Text.Trim().ToUpper().Contains("FOODSTAR")
                   || tbxImportFile.Text.Trim().ToUpper().Contains("LESTER"))
                {
                   amtDetail -= amtTax;
                }
                if ((amtHead - amtTax-amtFrt) != amtDetail || amtHead == 0 || amtDetail == 0)
                {
                    if (drHead["debit_or_credit"].ToString().Trim() == "C")
                    {
                        sbErrors.Append(string.Format("Error in Credit: {0}\tdated {4}\tinvoice amt={1}\tdetail amt={2}\tdifference={3}\n", rwInvoice, amtHead - amtTax, amtDetail, amtHead - amtTax - amtDetail, invoiceDte));
                    }
                    else
                    {
                        sbErrors.Append(string.Format("Error in Invoice: {0}\tdated {4}\tinvoice amt={1}\tdetail amt={2}\tdifference={3}\n", rwInvoice, amtHead - amtTax, amtDetail,amtHead - amtTax-amtDetail, invoiceDte));
                    }
                }
            }
            errors = sbErrors.ToString().Trim();
            if (errors.Length > 0)
            {
                FrmErrors frmErr = new FrmErrors
                {
                    InFyle = _fyleIn,
                    SbErrors = sbErrors
                };
                frmErr.ShowDialog();
            }
            // delete the incoming file since processed???????
            tbxImportFile.Text = string.Empty;
            
            Cursor = Cursors.Default;
        }

        string GetData(string sLyne, int length)
        {
            string sData = string.Empty;
            try
            {
                sData = sLyne.Substring(_curPos, length);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            _curPos += length;
            // remove leading zeros
            sData = sData.Trim().TrimStart('0').PadLeft(length, ' ');
            return sData;
        }

        string MakeDate(string ymd)
        {
            string rslt = string.Empty;
            try
            {
               int y = 0,
                   m = 0,
                   d = 0;
               int.TryParse(ymd.Substring(0, 4), out y);
                int.TryParse(ymd.Substring(4, 2), out m);
                int.TryParse(ymd.Substring(6), out d);
                DateTime dteOut = new DateTime(y, m, d);
                rslt = dteOut.ToString();
                if (rslt.Trim().Length < 8)
                {
                    rslt = string.Empty;
                }
            }
            catch
            {
            }
            return rslt;
        }

        string FmtDecimal(decimal num, int len)
        {
            string rslt = $"{num:0.00}";
            rslt = rslt.PadLeft(len, ' ');
            return rslt;
        }

        decimal MakeDecimal(string decIn)
        {
            decimal rslt = 0;
            try
            {
                string tmp = decIn.Replace("$", "");
                tmp = tmp.Replace(",", "");
                rslt = decimal.TryParse(tmp, out rslt) ? rslt : 0;
            }
            catch
            {
            }
            return rslt;
        }

        private void dgrdHeader_SelectionChanged(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                return;
            }
            Cursor = Cursors.WaitCursor;
            lblSelectedAmts.Visible = true;
            int selRow = dgrdHeader.CurrentCell.RowIndex,
                dtRowCnt = 0,
                selRwsVisable = 0;
            string sId = dgrdHeader.Rows[selRow].Cells["invoice_num"].Value.ToString().Trim();
            decimal invAmt = 0,
                hdrAmt = ObjToDecimal(dgrdHeader.Rows[selRow].Cells["invoice_amt"].Value),
                hdrTax = ObjToDecimal(dgrdHeader.Rows[selRow].Cells["invoice_tax"].Value),
                hdrFrt = ObjToDecimal(dgrdHeader.Rows[selRow].Cells["invoice_freight"].Value);
            
            CurrencyManager currencyMgr = (CurrencyManager)BindingContext[dgrdDetail.DataSource];
            currencyMgr.SuspendBinding();
            
            foreach (DataGridViewRow dgRow in dgrdDetail.Rows)
            {
                if (dgRow.Cells["invoice_num"].Value.ToString().Trim() == sId)
                {
                    selRwsVisable++;
                    dgrdDetail.Rows[dtRowCnt].Visible=true;
                    invAmt += Math.Round((ObjToDecimal(dgRow.Cells["price"].Value) * ObjToDecimal(dgRow.Cells["quantity"].Value)), 2);
                    invAmt += ObjToDecimal(dgRow.Cells["item_tax_percent"].Value);
                }
                else
                {
                    dgrdDetail.Rows[dtRowCnt].Visible = false;
                }
                dtRowCnt++;
            }
            lblDetail.Text = $"Selected Detail records: {selRwsVisable}";
            lblSelectedAmts.Text = string.Format("Invoice # {3}    Header amount:{1:c} tax:{2:c}      Detail amount:{0:c}", invAmt, (hdrAmt - hdrTax - hdrFrt), hdrTax, sId);
            lblSelectedAmts.ForeColor = (invAmt != (hdrAmt - hdrTax - hdrFrt) ? System.Drawing.Color.Red : System.Drawing.Color.Blue);
            Update();

            currencyMgr.ResumeBinding();
            currencyMgr = (CurrencyManager)BindingContext[dgrdSummary.DataSource];
            currencyMgr.SuspendBinding();
            dtRowCnt = 0;
            foreach (DataGridViewRow dgRow in dgrdSummary.Rows)
            {
                dgrdSummary.Rows[dtRowCnt].Visible = dgRow.Cells["invoice_num"].Value.ToString().Trim() == sId;
                dtRowCnt++;
            }
            currencyMgr.ResumeBinding();
            Cursor = Cursors.Default;
            Update();
        }
        string CheckForEmptyNumber(string inVal)
        {
           string rslt = inVal;
           if (rslt.Trim().Length == 0)
           {
              rslt = "0".PadRight(11, ' ');
           }
           return rslt;
        }

        decimal ObjToDecimal(object objIn)
        {
            decimal rslt = decimal.TryParse(objIn.ToString().Trim(), out rslt) ? rslt : 0;
            return rslt;
        }

        private void dgrdHeader_CursorChanged(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
            Close();
            Application.Exit();
        }

        private void RecordToDebug(string strToWryte)
        {
            string fyle = @"c:\it\debug file.txt";
            FileStream fStream = new FileStream(fyle, FileMode.Append);
            StreamWriter twfPath = new StreamWriter(fStream);
            twfPath.WriteLine(strToWryte);
            twfPath.Close();
            fStream.Close();
        }

        private void cbxNoSQL_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxNoSQL.Checked)
            {
                rdoToMatrixDev.Checked = true;
                rdoToMatrixDev.Enabled = false;
                rdoToMatrix.Enabled = false;
            }
            else
            {
                rdoToMatrixDev.Checked = true;
                rdoToMatrixDev.Enabled = true;
                rdoToMatrix.Enabled = true;
            }
        }
        private void PopulateInputFiles()
        {
            dgrdFiles.Rows.Clear();
            string lookPath;
            try
            {
                DirectoryInfo dirInfo = new DirectoryInfo(_ftpPath);
                DirectoryInfo[] dyrectoryes = dirInfo.GetDirectories();
                foreach (DirectoryInfo dirFnd in dyrectoryes)
                {
                    if (dirFnd.Name.Trim().ToUpper() != "LOGS")
                    {
                        lookPath = $@"{_ftpPath}{dirFnd}\in\";
                        string[] fyles;

                        if (dirFnd.Name.Trim().ToUpper() == "ECOLAB")
                        {
                            fyles = Directory.GetFiles(lookPath, "*.xlsx");
                        }
                        else if (dirFnd.Name.Trim().ToUpper() == "NUCO2")
                        {
                            fyles = Directory.GetFiles(lookPath, "*.csv");
                            if (fyles.GetUpperBound(0) < 0)
                            {
                               fyles = Directory.GetFiles(lookPath, "*.xlsx");
                            }
                        }
                        else if (dirFnd.Name.Trim().ToUpper() == "SHOESFORCREWS")
                        {
                            fyles = Directory.GetFiles(lookPath, "*.csv");
                        }
                        else if (dirFnd.Name.Trim().ToUpper() == "MOODMEDIA")
                        {
                           fyles = Directory.GetFiles(lookPath, "*.in");
                        }
                        else
                        {
                            fyles = Directory.GetFiles(lookPath, "*.txt");
                        }
                        if(fyles.GetUpperBound(0) >= 0)
                        {
                            foreach (string fyle in fyles)
                            {
                                FileInfo fInfo = new FileInfo(fyle);
                                if (lookPath.ToUpper().Contains("IFH") && !fInfo.Name.ToUpper().StartsWith("IFH"))
                                {
                                    string nwName = string.Format("{4}IFH_{0}{1:00}{2:00}_{3}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, fInfo.Name, lookPath);
                                    File.Copy(fyle, nwName);
                                    if (File.Exists(nwName))
                                    {
                                        File.Delete(fyle);
                                        fInfo = new FileInfo(nwName);
                                    }
                                }
                                dgrdFiles.Rows.Add(dirFnd, fInfo.FullName, fInfo.CreationTime);
                                // process each file found...
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                string _errmsg = "Import Invoices job failed in PopulateInputFiles():  " + ex.Message.ToString();
                using (CodeEmail clsEmail = new CodeEmail())
                {

                    clsEmail.ErrMsg = _errmsg;
                    bool _sendmail = clsEmail.SendEmail();


                }
                System.Environment.Exit(0);

            }
            Update();

            
            using (CodeEmail clsEmail = new CodeEmail())
            {

                clsEmail.ErrMsg = "ok";
                bool _sendmail = clsEmail.SendEmail();


            }

        }

        private void dgrdFiles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int selRow = dgrdFiles.CurrentCell.RowIndex;
            string fylePth = $"{dgrdFiles.Rows[selRow].Cells["colFile"].Value}";
            tbxImportFile.Text = fylePth;
            if (tbxImportFile.Text.Trim().Length > 0)
            {
                cmdLoadFyle.Visible = true;
            }
        }

        string MoveMinusSigh(string sLyne, int posStart, int posLen)
        {
            string rslt = sLyne.Trim(),
                lookAt = sLyne.Substring(posStart, posLen);
            if (lookAt.Contains("-")
                && !lookAt.StartsWith("-")
                && lookAt.StartsWith("0"))
            {
                lookAt = $"-{lookAt.Replace("-", string.Empty)}";
                rslt = sLyne.Substring(0, posStart) + lookAt + sLyne.Substring(posStart + posLen);
            }
            return rslt;
        }
    }
}
