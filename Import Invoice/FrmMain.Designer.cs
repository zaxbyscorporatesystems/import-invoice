﻿namespace Import_Invoice
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
         System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
         this.tbxImportFile = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.cmdBrowse = new System.Windows.Forms.Button();
         this.cmdLoadFyle = new System.Windows.Forms.Button();
         this.dgrdHeader = new System.Windows.Forms.DataGridView();
         this.dgrdDetail = new System.Windows.Forms.DataGridView();
         this.dgrdSummary = new System.Windows.Forms.DataGridView();
         this.lblHeader = new System.Windows.Forms.Label();
         this.lblDetail = new System.Windows.Forms.Label();
         this.lblSummary = new System.Windows.Forms.Label();
         this.lblSelectedAmts = new System.Windows.Forms.Label();
         this.cbxNoSQL = new System.Windows.Forms.CheckBox();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.rdoToMatrixDev = new System.Windows.Forms.RadioButton();
         this.rdoToMatrix = new System.Windows.Forms.RadioButton();
         this.dgrdFiles = new System.Windows.Forms.DataGridView();
         this.colVendor = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.colFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
         this.label2 = new System.Windows.Forms.Label();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdHeader)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdDetail)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdSummary)).BeginInit();
         this.menuStrip1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdFiles)).BeginInit();
         this.SuspendLayout();
         // 
         // tbxImportFile
         // 
         this.tbxImportFile.Location = new System.Drawing.Point(80, 159);
         this.tbxImportFile.Name = "tbxImportFile";
         this.tbxImportFile.Size = new System.Drawing.Size(534, 20);
         this.tbxImportFile.TabIndex = 9;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.ForeColor = System.Drawing.Color.Navy;
         this.label1.Location = new System.Drawing.Point(28, 163);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(50, 13);
         this.label1.TabIndex = 8;
         this.label1.Text = "Input File";
         // 
         // cmdBrowse
         // 
         this.cmdBrowse.ForeColor = System.Drawing.Color.Navy;
         this.cmdBrowse.Location = new System.Drawing.Point(644, 158);
         this.cmdBrowse.Name = "cmdBrowse";
         this.cmdBrowse.Size = new System.Drawing.Size(75, 23);
         this.cmdBrowse.TabIndex = 7;
         this.cmdBrowse.Text = "Browse";
         this.cmdBrowse.UseVisualStyleBackColor = true;
         this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
         // 
         // cmdLoadFyle
         // 
         this.cmdLoadFyle.ForeColor = System.Drawing.Color.Navy;
         this.cmdLoadFyle.Location = new System.Drawing.Point(734, 150);
         this.cmdLoadFyle.Name = "cmdLoadFyle";
         this.cmdLoadFyle.Size = new System.Drawing.Size(75, 39);
         this.cmdLoadFyle.TabIndex = 10;
         this.cmdLoadFyle.Text = "Load the Invoices";
         this.cmdLoadFyle.UseVisualStyleBackColor = true;
         this.cmdLoadFyle.Visible = false;
         this.cmdLoadFyle.Click += new System.EventHandler(this.cmdLoadFyle_Click);
         // 
         // dgrdHeader
         // 
         this.dgrdHeader.AllowUserToAddRows = false;
         this.dgrdHeader.AllowUserToDeleteRows = false;
         dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
         this.dgrdHeader.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
         this.dgrdHeader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgrdHeader.Location = new System.Drawing.Point(12, 208);
         this.dgrdHeader.Name = "dgrdHeader";
         this.dgrdHeader.RowHeadersVisible = false;
         this.dgrdHeader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgrdHeader.Size = new System.Drawing.Size(932, 221);
         this.dgrdHeader.TabIndex = 11;
         this.dgrdHeader.CurrentCellChanged += new System.EventHandler(this.dgrdHeader_SelectionChanged);
         this.dgrdHeader.SelectionChanged += new System.EventHandler(this.dgrdHeader_SelectionChanged);
         this.dgrdHeader.CursorChanged += new System.EventHandler(this.dgrdHeader_CursorChanged);
         // 
         // dgrdDetail
         // 
         this.dgrdDetail.AllowUserToAddRows = false;
         this.dgrdDetail.AllowUserToDeleteRows = false;
         dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
         this.dgrdDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
         this.dgrdDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgrdDetail.Location = new System.Drawing.Point(12, 448);
         this.dgrdDetail.Name = "dgrdDetail";
         this.dgrdDetail.RowHeadersVisible = false;
         dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
         this.dgrdDetail.RowsDefaultCellStyle = dataGridViewCellStyle7;
         this.dgrdDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgrdDetail.Size = new System.Drawing.Size(932, 221);
         this.dgrdDetail.TabIndex = 12;
         // 
         // dgrdSummary
         // 
         this.dgrdSummary.AllowUserToAddRows = false;
         this.dgrdSummary.AllowUserToDeleteRows = false;
         dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
         this.dgrdSummary.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
         this.dgrdSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgrdSummary.Location = new System.Drawing.Point(12, 695);
         this.dgrdSummary.Name = "dgrdSummary";
         this.dgrdSummary.RowHeadersVisible = false;
         this.dgrdSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgrdSummary.Size = new System.Drawing.Size(932, 221);
         this.dgrdSummary.TabIndex = 13;
         // 
         // lblHeader
         // 
         this.lblHeader.AutoSize = true;
         this.lblHeader.Location = new System.Drawing.Point(12, 192);
         this.lblHeader.Name = "lblHeader";
         this.lblHeader.Size = new System.Drawing.Size(98, 13);
         this.lblHeader.TabIndex = 14;
         this.lblHeader.Text = "Header Data Table";
         // 
         // lblDetail
         // 
         this.lblDetail.AutoSize = true;
         this.lblDetail.Location = new System.Drawing.Point(9, 432);
         this.lblDetail.Name = "lblDetail";
         this.lblDetail.Size = new System.Drawing.Size(90, 13);
         this.lblDetail.TabIndex = 15;
         this.lblDetail.Text = "Detail Data Table";
         // 
         // lblSummary
         // 
         this.lblSummary.AutoSize = true;
         this.lblSummary.Location = new System.Drawing.Point(13, 677);
         this.lblSummary.Name = "lblSummary";
         this.lblSummary.Size = new System.Drawing.Size(106, 13);
         this.lblSummary.TabIndex = 16;
         this.lblSummary.Text = "Summary Data Table";
         // 
         // lblSelectedAmts
         // 
         this.lblSelectedAmts.AutoSize = true;
         this.lblSelectedAmts.Location = new System.Drawing.Point(264, 677);
         this.lblSelectedAmts.Name = "lblSelectedAmts";
         this.lblSelectedAmts.Size = new System.Drawing.Size(113, 13);
         this.lblSelectedAmts.TabIndex = 17;
         this.lblSelectedAmts.Text = "Selected Invoice Amts";
         this.lblSelectedAmts.Visible = false;
         // 
         // cbxNoSQL
         // 
         this.cbxNoSQL.AutoSize = true;
         this.cbxNoSQL.Checked = true;
         this.cbxNoSQL.CheckState = System.Windows.Forms.CheckState.Checked;
         this.cbxNoSQL.ForeColor = System.Drawing.Color.Navy;
         this.cbxNoSQL.Location = new System.Drawing.Point(827, 132);
         this.cbxNoSQL.Name = "cbxNoSQL";
         this.cbxNoSQL.Size = new System.Drawing.Size(117, 17);
         this.cbxNoSQL.TabIndex = 18;
         this.cbxNoSQL.Text = "Do not load to SQL";
         this.cbxNoSQL.UseVisualStyleBackColor = true;
         this.cbxNoSQL.CheckedChanged += new System.EventHandler(this.cbxNoSQL_CheckedChanged);
         // 
         // menuStrip1
         // 
         this.menuStrip1.BackColor = System.Drawing.SystemColors.MenuBar;
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
         this.menuStrip1.ShowItemToolTips = true;
         this.menuStrip1.Size = new System.Drawing.Size(969, 24);
         this.menuStrip1.TabIndex = 19;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // exitToolStripMenuItem
         // 
         this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
         this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
         this.exitToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
         this.exitToolStripMenuItem.Text = "E&xit";
         this.exitToolStripMenuItem.ToolTipText = "Exit the system.";
         this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
         // 
         // rdoToMatrixDev
         // 
         this.rdoToMatrixDev.AutoSize = true;
         this.rdoToMatrixDev.Checked = true;
         this.rdoToMatrixDev.Enabled = false;
         this.rdoToMatrixDev.ForeColor = System.Drawing.Color.Navy;
         this.rdoToMatrixDev.Location = new System.Drawing.Point(832, 155);
         this.rdoToMatrixDev.Name = "rdoToMatrixDev";
         this.rdoToMatrixDev.Size = new System.Drawing.Size(112, 17);
         this.rdoToMatrixDev.TabIndex = 20;
         this.rdoToMatrixDev.TabStop = true;
         this.rdoToMatrixDev.Text = "Load to MatrixDev";
         this.rdoToMatrixDev.UseVisualStyleBackColor = true;
         // 
         // rdoToMatrix
         // 
         this.rdoToMatrix.AutoSize = true;
         this.rdoToMatrix.Enabled = false;
         this.rdoToMatrix.ForeColor = System.Drawing.Color.Red;
         this.rdoToMatrix.Location = new System.Drawing.Point(832, 178);
         this.rdoToMatrix.Name = "rdoToMatrix";
         this.rdoToMatrix.Size = new System.Drawing.Size(92, 17);
         this.rdoToMatrix.TabIndex = 21;
         this.rdoToMatrix.Text = "Load to Matrix";
         this.rdoToMatrix.UseVisualStyleBackColor = true;
         // 
         // dgrdFiles
         // 
         this.dgrdFiles.AllowUserToAddRows = false;
         this.dgrdFiles.AllowUserToDeleteRows = false;
         this.dgrdFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dgrdFiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colVendor,
            this.colFile,
            this.colDate});
         this.dgrdFiles.Location = new System.Drawing.Point(79, 27);
         this.dgrdFiles.Name = "dgrdFiles";
         this.dgrdFiles.RowHeadersVisible = false;
         this.dgrdFiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
         this.dgrdFiles.Size = new System.Drawing.Size(730, 122);
         this.dgrdFiles.TabIndex = 22;
         this.dgrdFiles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrdFiles_CellContentClick);
         // 
         // colVendor
         // 
         this.colVendor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         this.colVendor.HeaderText = "Vendor";
         this.colVendor.Name = "colVendor";
         this.colVendor.Width = 66;
         // 
         // colFile
         // 
         this.colFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
         this.colFile.HeaderText = "File";
         this.colFile.Name = "colFile";
         // 
         // colDate
         // 
         this.colDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
         this.colDate.HeaderText = "Date";
         this.colDate.Name = "colDate";
         this.colDate.Width = 55;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.ForeColor = System.Drawing.Color.Navy;
         this.label2.Location = new System.Drawing.Point(17, 27);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(56, 13);
         this.label2.TabIndex = 23;
         this.label2.Text = "Select file:";
         // 
         // FrmMain
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
         this.ClientSize = new System.Drawing.Size(969, 930);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.dgrdFiles);
         this.Controls.Add(this.rdoToMatrix);
         this.Controls.Add(this.rdoToMatrixDev);
         this.Controls.Add(this.cbxNoSQL);
         this.Controls.Add(this.lblSelectedAmts);
         this.Controls.Add(this.lblSummary);
         this.Controls.Add(this.lblDetail);
         this.Controls.Add(this.lblHeader);
         this.Controls.Add(this.dgrdSummary);
         this.Controls.Add(this.dgrdDetail);
         this.Controls.Add(this.dgrdHeader);
         this.Controls.Add(this.cmdLoadFyle);
         this.Controls.Add(this.tbxImportFile);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.cmdBrowse);
         this.Controls.Add(this.menuStrip1);
         this.MainMenuStrip = this.menuStrip1;
         this.Name = "FrmMain";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Invoice Import";
         this.Load += new System.EventHandler(this.FrmMain_Load);
         ((System.ComponentModel.ISupportInitialize)(this.dgrdHeader)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdDetail)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdSummary)).EndInit();
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.dgrdFiles)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxImportFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdBrowse;
        private System.Windows.Forms.Button cmdLoadFyle;
        private System.Windows.Forms.DataGridView dgrdHeader;
        private System.Windows.Forms.DataGridView dgrdDetail;
        private System.Windows.Forms.DataGridView dgrdSummary;
        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblDetail;
        private System.Windows.Forms.Label lblSummary;
        private System.Windows.Forms.Label lblSelectedAmts;
        private System.Windows.Forms.CheckBox cbxNoSQL;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.RadioButton rdoToMatrixDev;
        private System.Windows.Forms.RadioButton rdoToMatrix;
        private System.Windows.Forms.DataGridView dgrdFiles;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVendor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
    }
}

