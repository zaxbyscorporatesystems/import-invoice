﻿using System;
using System.Text;
using System.IO;
using System.Data;

namespace Import_Invoice
{
    class CodeFile:IDisposable
    {
        int _curPos = 0,
            _numRecs=0;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        // properties-----------------------
        public string InputFile
        {
            set;
            get;
        }
        public string ImportSource
        {
            set;
            get;
        }
        public int NumberRecords
        {
            set;
            get;
        }

        public DataTable DtHeader
        {
            set;
            get;
        }
        public DataTable DtDetail
        {
            set;
            get;
        }
        public DataTable DtSummary
        {
            set;
            get;
        }
        // methods--------------------------
        public string VerifyFileFormat()
        {
            int lyneCount=0;
            string rslt = string.Empty,
                lyneItem = string.Empty,
                // header lines items
                zfiCustNum = string.Empty,
                custPoNum,
                debitCredit,
                dteAdded,
                createdBy = string.Empty,
                invoiceNum = string.Empty,
                dteDelivery = string.Empty,
                dteInvoice = string.Empty,
                invTotal = string.Empty,
                invTax = string.Empty,
                invFreight = string.Empty,
                creditApplyToNum = string.Empty,
                // detail line items 
                dItemNum = string.Empty,
                dItemDesc = string.Empty,
                dQuantity = string.Empty,
                dUnitOfMeasure = string.Empty,
                dPrice = string.Empty,
                dItemTax = string.Empty,
                dOtherNumeric = string.Empty,
                dOptIdentifier = string.Empty,
                dCcustomDefinition = string.Empty,
                dEndOfRecord = string.Empty,
                // summary line items
                sTotInvoiceLines = string.Empty,
                sOptionalInfo = string.Empty,
                sEndOfRecord = string.Empty,
                rejectFyle = string.Empty;

            DataTable dtInvoiceHeader = new DataTable();
            dtInvoiceHeader.Columns.Add("rec_id", typeof(int));
            dtInvoiceHeader.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceHeader.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("customer_po");                   // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("debit_or_credit");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("date_added");                    // CHAR(8),
            dtInvoiceHeader.Columns.Add("added_by");                      // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceHeader.Columns.Add("date_delivered");                // CHAR(8),
            dtInvoiceHeader.Columns.Add("date_invoice");                  // CHAR(8),
            dtInvoiceHeader.Columns.Add("invoice_amt");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_tax");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_freight");               // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("credit_apply_to_num");           // VARCHAR(8),
            dtInvoiceHeader.Columns.Add("record_end_char");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("import_file_id", typeof(int));    // INT,
            dtInvoiceHeader.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("status_flag", typeof(int));       // SMALLINT

            DataTable dtInvoiceDetail = new DataTable();
            dtInvoiceDetail.Columns.Add("rec_id", typeof(int));            // INT,
            dtInvoiceDetail.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceDetail.Columns.Add("sequence_id", typeof(int));       // INT,
            dtInvoiceDetail.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceDetail.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceDetail.Columns.Add("item_number");                   // VARCHAR(6),
            dtInvoiceDetail.Columns.Add("item_desc");                     // VARCHAR(80),
            dtInvoiceDetail.Columns.Add("account_code");                  // VARCHAR(32),
            dtInvoiceDetail.Columns.Add("quantity", typeof(decimal));      // DECIMAL(16,4),
            dtInvoiceDetail.Columns.Add("uom");                           // VARCHAR(2),
            dtInvoiceDetail.Columns.Add("price");                         // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax");                      // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax_percent");              // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("custom_definitions");            // VARCHAR(162),
            dtInvoiceDetail.Columns.Add("d_record_end_char");             // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("import_file_id", typeof(int));    // INT,
            dtInvoiceDetail.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceDetail.Columns.Add("status_flag", typeof(int));       // SMALLINT)

            DataTable dtInvoiceSummary = new DataTable();
            dtInvoiceSummary.Columns.Add("rec_id", typeof(int));           // INT,
            dtInvoiceSummary.Columns.Add("record_type");                  // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("invoice_num");                  // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("vend_customer_num");            // VARCHAR(15),
            dtInvoiceSummary.Columns.Add("vend_supplier_id");             // VARCHAR(5),
            dtInvoiceSummary.Columns.Add("total_invoice_lines");          // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("s_record_end_char");            // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceSummary.Columns.Add("import_source");                // VARCHAR(10))

            if (InputFile.Trim().Length > 0 && File.Exists(InputFile))
            {
                ImportSource=InputFile.ToUpper().Trim();
                ImportSource = ImportSource.Substring(0, ImportSource.IndexOf('.'));
                if (ImportSource.Contains("MOOD"))
                {
//                    rejectFyle = ImportSource.Replace(".IN", "_errors.txt");
                    rejectFyle = ImportSource+"_errors.txt";
                }
                else
                {
                    rejectFyle = ImportSource.Replace(".TXT", "_errors.txt");       // InputFile.Substring(0, InputFile.LastIndexOf("\\") + 1);
                }
                //rejectFyle = string.Format("{0}{1}_errors.txt", rejectFyle, ImportSource.Trim());
                if (File.Exists(rejectFyle))
                {
                    File.Delete(rejectFyle);
                }
                string[] fyleLynes = File.ReadAllLines(InputFile);
                int recId = 0;
                foreach (string fLyne in fyleLynes)
                {
                    if (fLyne.ToUpper().StartsWith("H"))
                    {
                        lyneCount++;
                        //header
                        _numRecs = 0;
                        // check to be sure first header or summary just found????
                        if (fLyne.Trim().Length != 140)
                        {
                            rslt = "Errors found; check reject file.";
                            RecordToLog(rejectFyle,
                                $"Error in header line {fLyne} incorrect length {fLyne.Trim().Length}");
                        }
                        recId++;
                        _curPos = 1;
                        ImportSource = GetData(fLyne, 5);       //zfi supplier id code also import source
                        zfiCustNum = GetData(fLyne, 20);        //zfi customer num
                        custPoNum = GetData(fLyne, 5);          //customer po num
                        debitCredit = GetData(fLyne, 1);        // ??check for i=debit,c=credit
                        dteAdded = GetData(fLyne, 8);           // ??check for added date
                        createdBy = GetData(fLyne, 10);         //created by
                        invoiceNum = GetData(fLyne, 20);        //invoice num
                        dteDelivery = GetData(fLyne, 8);        // ??check for delivery date
                        dteInvoice = GetData(fLyne, 8);         // ??check for invoice date
                        invTotal = GetData(fLyne, 11);          //invoice total
                        invTax = GetData(fLyne, 11);            //invoice tax
                        invFreight = GetData(fLyne, 11);        //invoice freight
                        creditApplyToNum = GetData(fLyne, 20);  //credit apply to number
                        lyneItem = GetData(fLyne, 1);           //* end of record
                        dteDelivery = dteDelivery.Trim().Length == 0 ? dteInvoice : dteDelivery;
                        dteAdded = dteAdded.Trim().Length == 0 || dteAdded.StartsWith("19") ? dteDelivery : dteAdded;
                        // header rec to datatable
                        dtInvoiceHeader.Rows.Add(
                            recId
                            , "H"
                            , ImportSource
                            , zfiCustNum.Trim()
                            , custPoNum.Trim()
                            , debitCredit
                            , dteAdded
                            , createdBy
                            , invoiceNum.Trim()
                            , dteDelivery
                            , dteInvoice
                            , invTotal
                            , invTax
                            , invFreight
                            , creditApplyToNum
                            , lyneItem
                            , 0
                            , ImportSource
                            , 0
                            );
                        //if (lyneItem != "*")
                        //{
                        //    rslt = "Errors found; check reject file.";
                        //    recordToLog(rejectFyle, string.Format("Error in header line {0}", fLyne));
                        //}
                    }
                    else if (fLyne.ToUpper().StartsWith("D"))
                    {
                        lyneCount++;
                        string tmpfLyne = fLyne;
                        //detail
                        // check to be sure header found????
                        if(tmpfLyne.Length>198)
                        {
                            tmpfLyne = tmpfLyne.Substring(0, 198);
                        }
                        if (tmpfLyne.Trim().Length != 198)
                        {
                            rslt = "Errors found; check reject file.";
                            RecordToLog(rejectFyle,
                                $"Error in detail line {tmpfLyne} incorrect length {tmpfLyne.Trim().Length}");
                        }
                        _curPos = 1;
                        dItemNum = GetData(tmpfLyne, 30);    // 6);    //30);              //item num
                        dItemDesc = GetData(tmpfLyne, 80);             //item desc
                        dQuantity = GetData(tmpfLyne, 11);             //quantity
                        dUnitOfMeasure = GetData(tmpfLyne, 2);         //unit of measure
                        dPrice = GetData(tmpfLyne, 11);                //price
                        dItemTax = GetData(tmpfLyne, 11);              //item tax
                        dOtherNumeric = GetData(tmpfLyne, 11);         //other numeric
                        dOptIdentifier = GetData(tmpfLyne, 30);        //optional identifier
                        dCcustomDefinition = GetData(tmpfLyne, 10);    //custom definition
                        dEndOfRecord = GetData(tmpfLyne, 1);           //* end of record
                        _numRecs++;      // only detail lines are counted
                        // detail rec to datatable
                        dtInvoiceDetail.Rows.Add(
                            recId
                            , "D"
                            , invoiceNum.Trim()
                            ,_numRecs
                            , zfiCustNum.Trim()
                            , ImportSource
                            ,dItemNum.Trim()
                            ,dItemDesc.Trim()
                            , "account_code"
                            , MakeDecimal(dQuantity)
                            ,dUnitOfMeasure
                            ,dPrice
                            ,dItemTax
                            , "item_tax_%"
                            ,dCcustomDefinition.Trim()
                            ,dEndOfRecord
                            , 0
                            , ImportSource
                            , 0
                            );
                    }
                    else if (fLyne.ToUpper().StartsWith("S"))
                    {
                        lyneCount++;
                        //summary
                        // check to be sure header & detail found????
                        if (fLyne.Trim().Length != 19)
                        {
                            rslt ="Errors found; check reject file.";
                            RecordToLog(rejectFyle,
                                $"Error in summary line {fLyne} incorrect length {fLyne.Trim().Length}");
                        }
                        _curPos = 1;
                        sTotInvoiceLines = GetData(fLyne, 7);       //total invoice lines
                        // check if detail line count matches
                        sOptionalInfo = GetData(fLyne, 10);    //custom definition
                        sEndOfRecord = GetData(fLyne, 1);           //* end of record
                        dtInvoiceSummary.Rows.Add(
                            recId
                            , "S"
                            ,invoiceNum.Trim()
                            , zfiCustNum.Trim()
                            , ImportSource
                            , sTotInvoiceLines
                            ,sEndOfRecord
                            , 0
                            , ImportSource
                            );

                    }
                    else
                    {
                        //incorrect starting character
                    }
                }
            }
            NumberRecords = lyneCount;      // numRecs;
            DtHeader = dtInvoiceHeader;
            DtDetail = dtInvoiceDetail;
            DtSummary = dtInvoiceSummary;
            return rslt;
        }

        string GetData(string sLyne,int length)
        {
            string sData = string.Empty;
            try
            {
                sData = sLyne.Substring(_curPos, length);
                sData = sData.Replace("'", "");
                _curPos += length;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            // remove leading zeros
            // .trim() in case of leading space(s)
            sData = sData.Trim().TrimStart('0');    //.PadLeft(length, ' ');
            
            return sData;
        }
        decimal MakeDecimal(string decIn)
        {
            decimal rslt = 0;
            try
            {
                string tmp = decIn.Replace("$", "");
                tmp = tmp.Replace(",", "");
                rslt = decimal.TryParse(tmp, out rslt) ? rslt : 0;
            }
            catch
            {
            }
            return rslt;
        }
        string FrmtDateMdy(object oDtein)
        {
            string rslt = string.Empty,
                sTmp;
            try
            {
                sTmp = oDtein.ToString();
                rslt = $"{sTmp.Substring(4, 2):00}/{sTmp.Substring(6):00}/{sTmp.Substring(0, 4):0000}";
                if (rslt.Trim().Length < 8)
                {
                    rslt = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
        private void RecordToLog(string fyle, string strToWryte)
        {
            FileStream fStream = new FileStream(fyle, FileMode.Append);
            StreamWriter twfPath = new StreamWriter(fStream);
            twfPath.WriteLine(strToWryte);
            twfPath.Close();
            fStream.Close();
        }

        //object checkNumber(object objIn)
        //{
        //    object rslt = objIn.ToString().Trim().Length == 0 ? 0 : objIn;
        //    return rslt;
        //}

        public string MakeOldLineFormat(string lyneIn)
        {
            string dteDelivery,
                dteInvoice;
            StringBuilder sbNewLyne = new StringBuilder();
            lyneIn = lyneIn.Replace("'", " ");
            if (lyneIn.ToUpper().StartsWith("H"))
            {
                //header
                _curPos = 1;
                sbNewLyne.Append("H");
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 5, 5));     // ImportSource = getData(lyneIn, 5);       //zfi supplier id code also import source
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 20, 20));     // zfiCustNum = getData(lyneIn, 20);        //zfi customer num
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 5, 5));     // custPONum = getData(lyneIn, 5);          //customer po num
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 1, 1));     // debitCredit = getData(lyneIn, 1);        // ??check for i=debit,c=credit
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 8, 8));     // dteAdded = getData(lyneIn, 8);           // ??check for added date
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 10, 10));     // createdBy = getData(lyneIn, 10);         //created by
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 20, 20));     // invoiceNum = getData(lyneIn, 20);        //invoice num
                dteDelivery = GetOldData(lyneIn,false,' ',8, 8);        // ??check for delivery date
                dteInvoice = GetOldData(lyneIn, false, ' ', 8, 8);         // ??check for invoice date
                dteDelivery = dteDelivery.Trim().Length == 0 ? dteInvoice : dteDelivery;
                sbNewLyne.Append(dteDelivery);
                sbNewLyne.Append(dteInvoice);
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // invTotal = getData(lyneIn, 11);          //invoice total
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // invTax = getData(lyneIn, 11);            //invoice tax
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // invFreight = getData(lyneIn, 11);        //invoice freight
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 20, 20));     // creditApplyToNum = getData(lyneIn, 20);  //credit apply to number
                sbNewLyne.Append("*");     // lyneItem = getData(lyneIn, 1);           //* end of record
            }
            else if (lyneIn.ToUpper().StartsWith("D"))
            {
                //detail
                // check to be sure header found????
                _curPos = 1;
                sbNewLyne.Append("D");
                sbNewLyne.Append(GetOldData(lyneIn, true, ' ', 30, 30));     // dItemNum = getData(lyneIn, 30);              //item num
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 80, 80));     // dItemDesc = getData(lyneIn, 80);             //item desc
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dQuantity = getData(lyneIn, 11);             //quantity
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 2, 2));     // dUnitOfMeasure = getData(lyneIn, 2);         //unit of measure
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dPrice = getData(lyneIn, 11);                //price
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dItemTax = getData(lyneIn, 11);              //item tax
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dOtherNumeric = getData(lyneIn, 11);         //other numeric
                //sbNewLyne.Append(getOldData(lyneIn, true, '0', 30, 7));     // dOptIdentifier = getData(lyneIn, 30);        //optional identifier
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 40, 40));     // dCcustomDefinition = getData(lyneIn, 10);    //custom definition
                sbNewLyne.Append("*");     // dEndOfRecord = getData(lyneIn, 1);           //* end of record
            }
            else if (lyneIn.ToUpper().StartsWith("S"))
            {
                //summary
                _curPos = 1;
                sbNewLyne.Append("S");
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 7, 7));     // getData(lyneIn, 7);       //total invoice lines
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 10, 10));     // gsOptionalInfo = getData(lyneIn, 10);    //custom definition
                sbNewLyne.Append("*");     // sEndOfRecord = getData(lyneIn, 1);           //* end of record
            }
            else
            {
                //incorrect starting character
            }
            string rsltOut = sbNewLyne.ToString();
            return rsltOut;
        }

        string GetOldData(string sLyne,bool justRt,char filChar, int newLen, int oldLen)
        {
            string sData = string.Empty;
            try
            {
                sData = sLyne.Substring(_curPos, newLen);
                sData = sData.Replace("'", " ");
                _curPos += newLen;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            // remove leading zeros
            // .trim() in case of leading space(s)
            sData = sData.Trim().TrimStart('0');    //.PadLeft(length, ' ');
            if (sData.Length > oldLen)
            {
                sData = sData.Substring(0, oldLen);
            }
            else
            {
                sData = justRt ? sData.PadLeft(oldLen, filChar) : sData.PadRight(oldLen, filChar);
            }
            return sData;
        }

    }
}
