﻿using System;
using System.IO;
using System.Data;
using System.Data.OleDb;

namespace Import_Invoice
{
    class CodeEcoLab:IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        // properties-----------------------
        public string InputFile
        {
            set;
            get;
        }
        public string ImportSource
        {
            set;
            get;
        }
        public int NumberRecords
        {
            set;
            get;
        }

        public DataTable DtHeader
        {
            set;
            get;
        }
        public DataTable DtDetail
        {
            set;
            get;
        }
        public DataTable DtSummary
        {
            set;
            get;
        }
        // methods--------------------------
        public string VerifyFileFormat()
        {
            int lyneCount = 0;
            string tmp = string.Empty,
                rslt = string.Empty,
                lyneItem = string.Empty,
                // header lines items
                zfiCustNum = string.Empty,
                custPoNum = string.Empty,
                debitCredit = string.Empty,
                dteAdded = string.Empty,
                createdBy = string.Empty,
                invoiceNum = string.Empty,
                dteDelivery = string.Empty,
                dteInvoice = string.Empty,
                invTotal = string.Empty,
                invTax = string.Empty,
                invFreight = string.Empty,
                creditApplyToNum = string.Empty,
                // detail line items 
                dItemNum = string.Empty,
                dItemDesc = string.Empty,
                dQuantity = string.Empty,
                dUnitOfMeasure = string.Empty,
                dPrice = string.Empty,
                dItemTax = string.Empty,
                dOtherNumeric = string.Empty,
                dOptIdentifier = string.Empty,
                dCcustomDefinition = string.Empty,
                dEndOfRecord = string.Empty,
                // summary line items
                sTotInvoiceLines = string.Empty,
                sOptionalInfo = string.Empty,
                sEndOfRecord = string.Empty,
                rejectFyle = string.Empty;
            decimal quantity = 0,
                price = 0,
                extendedPrice = 0,
                tax = 0,
                totalGross = 0,
                totalInvoice = 0,
                othAmt = 0;

            DataTable dtInvoiceHeader = new DataTable();
            dtInvoiceHeader.Columns.Add("rec_id", typeof(int));
            dtInvoiceHeader.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceHeader.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("customer_po");                   // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("debit_or_credit");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("date_added");                    // CHAR(8),
            dtInvoiceHeader.Columns.Add("added_by");                      // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceHeader.Columns.Add("date_delivered");                // CHAR(8),
            dtInvoiceHeader.Columns.Add("date_invoice");                  // CHAR(8),
            dtInvoiceHeader.Columns.Add("invoice_amt");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_tax");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_freight");               // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("credit_apply_to_num");           // VARCHAR(8),
            dtInvoiceHeader.Columns.Add("record_end_char");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("import_file_id", typeof(int));    // INT,
            dtInvoiceHeader.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("status_flag", typeof(int));       // SMALLINT

            DataTable dtInvoiceDetail = new DataTable();
            dtInvoiceDetail.Columns.Add("rec_id", typeof(int));            // INT,
            dtInvoiceDetail.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceDetail.Columns.Add("sequence_id", typeof(int));       // INT,
            dtInvoiceDetail.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceDetail.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceDetail.Columns.Add("item_number");                   // VARCHAR(6),
            dtInvoiceDetail.Columns.Add("item_desc");                     // VARCHAR(80),
            dtInvoiceDetail.Columns.Add("account_code");                  // VARCHAR(32),
            dtInvoiceDetail.Columns.Add("quantity", typeof(decimal));      // DECIMAL(16,4),
            dtInvoiceDetail.Columns.Add("uom");                           // VARCHAR(2),
            dtInvoiceDetail.Columns.Add("price");                         // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax");                      // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax_percent");              // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("custom_definitions");            // VARCHAR(162),
            dtInvoiceDetail.Columns.Add("d_record_end_char");             // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("import_file_id", typeof(int));    // INT,
            dtInvoiceDetail.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceDetail.Columns.Add("status_flag", typeof(int));       // SMALLINT)

            DataTable dtInvoiceSummary = new DataTable();
            dtInvoiceSummary.Columns.Add("rec_id", typeof(int));           // INT,
            dtInvoiceSummary.Columns.Add("record_type");                  // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("invoice_num");                  // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("vend_customer_num");            // VARCHAR(15),
            dtInvoiceSummary.Columns.Add("vend_supplier_id");             // VARCHAR(5),
            dtInvoiceSummary.Columns.Add("total_invoice_lines");          // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("s_record_end_char");            // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceSummary.Columns.Add("import_source");                // VARCHAR(10))


            DataTable dtFyleData = new DataTable();
            dtFyleData.Columns.Add("customer");
            dtFyleData.Columns.Add("ponum");
            dtFyleData.Columns.Add("credit");
            dtFyleData.Columns.Add("addDate");
            dtFyleData.Columns.Add("addBy");
            dtFyleData.Columns.Add("invoiceNum");
            dtFyleData.Columns.Add("delDate");
            dtFyleData.Columns.Add("invDate");
            dtFyleData.Columns.Add("invTot", typeof(decimal));
            dtFyleData.Columns.Add("invTax", typeof(decimal));
            dtFyleData.Columns.Add("invOther", typeof(decimal));
            dtFyleData.Columns.Add("prodID");
            dtFyleData.Columns.Add("description");
            dtFyleData.Columns.Add("quantity", typeof(decimal));
            dtFyleData.Columns.Add("price", typeof(decimal));
            dtFyleData.Columns.Add("extendedPrice", typeof(decimal));
            dtFyleData.Columns.Add("tax", typeof(decimal));
            dtFyleData.Columns.Add("totalGross", typeof(decimal));


            if (InputFile.Trim().Length > 0 && File.Exists(InputFile))
            {
                //ImportSource = InputFile.Substring(InputFile.LastIndexOf("\\") + 1);
                //ImportSource = ImportSource.Substring(0, ImportSource.IndexOf('.'));
                ImportSource = "ECLAB";
                rejectFyle = InputFile.Substring(0, InputFile.LastIndexOf("\\") + 1);
                rejectFyle = $"{rejectFyle}{ImportSource.Trim()}_errors.txt";
                if (File.Exists(rejectFyle))
                {
                    File.Delete(rejectFyle);
                }
                OleDbConnection con = new OleDbConnection(
                    $@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={InputFile};Extended Properties=Excel 12.0");
                con.Open();
                DataTable dtExSheets = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string wSheet = "Zaxbys$";       // dtExSheets.Rows[0]["TABLE_NAME"].ToString();
                OleDbDataAdapter daExcel = new OleDbDataAdapter($"select * from [{wSheet}]", con);
                System.Data.DataTable dtExcel = new System.Data.DataTable();
                daExcel.Fill(dtExcel);
                con.Close();

                //string[] fyleLynes = File.ReadAllLines(InputFile);
                int recId = 0;
                foreach (DataRow dRow in dtExcel.Rows)       // (string fLyne in fyleLynes)
                {
                    string[] lynes = new string[dtExcel.Columns.Count];
                    int cntElem = 0;
                    foreach (object oElement in dRow.ItemArray)
                    {
                        lynes[cntElem] = $"{oElement}";
                        cntElem++;
                    }
                    recId++;

                    // header lines items
                    zfiCustNum = $"{lynes[(int) EcoItems.Custnum]}-{lynes[(int) EcoItems.Loc].PadLeft(4, '0')}";
                    custPoNum = string.Empty;
                    debitCredit = "I";
                    dteAdded = lynes[(int)EcoItems.ServDate];
                    createdBy = string.Empty;
                    invoiceNum = lynes[(int)EcoItems.InvNum];

                    dteDelivery = lynes[(int)EcoItems.ServDate];
                    dteInvoice = lynes[(int)EcoItems.ServDate];

                    invTotal = lynes[(int)EcoItems.InvTot];
                    invTax = lynes[(int)EcoItems.ItemTax];
                    invFreight = string.Empty;
                    creditApplyToNum = string.Empty;
                    // detail line items 
                    dItemNum = lynes[(int)EcoItems.PestCode];
                    dItemDesc = lynes[(int)EcoItems.ServName];
                    dQuantity = "1";
                    dUnitOfMeasure = string.Empty;
                    dPrice = lynes[(int)EcoItems.UnitCost];
                    dItemTax = lynes[(int)EcoItems.ItemTax];
                    dOtherNumeric = string.Empty;
                    dOptIdentifier = string.Empty;
                    dCcustomDefinition = string.Empty;
                    dEndOfRecord = string.Empty;

                    // summary line items
                    sTotInvoiceLines = string.Empty;
                    sOptionalInfo = string.Empty;
                    sEndOfRecord = string.Empty;
                    quantity = decimal.TryParse(dQuantity.Trim(), out quantity) ? quantity : 0;
                    price = decimal.TryParse(dPrice.Trim(), out price) ? price : 0;
                    tmp = lynes[(int)EcoItems.LineAmt].ToString();
                    extendedPrice = decimal.TryParse(tmp.Trim(), out extendedPrice) ? extendedPrice : 0;
                    tax = decimal.TryParse(dItemTax.Trim(), out tax) ? tax : 0;
                    totalGross = extendedPrice;// +tax;
                    totalInvoice = decimal.TryParse(invTotal.Trim(), out totalInvoice) ? totalInvoice : 0;
                    dtFyleData.Rows.Add(
                        zfiCustNum
                        , custPoNum
                        , debitCredit
                        , dteAdded
                        , createdBy
                        , invoiceNum
                        , dteDelivery
                        , dteInvoice
                        , totalInvoice
                        , tax
                        , othAmt
                        , dItemNum
                        , dItemDesc
                        , quantity
                        , price
                        , extendedPrice
                        , tax
                        , extendedPrice
                        );
                }
            }
            NumberRecords = lyneCount;      // numRecs;


            // get the invoice numbers
            int recIDnum = 0;

            DataTable dtInvoices = new DataTable();
            dtInvoices = dtFyleData.DefaultView.ToTable(true, "invoiceNum");
            // for each invoice, create a header rec 
            string invoice = string.Empty;
            foreach (DataRow drInvNum in dtInvoices.Rows)
            {
                recIDnum++;
                invoice = drInvNum["invoiceNum"].ToString().Trim();
                if (invoice.Trim().Length == 0)
                {
                    break;
                }
                DataRow[] drLineItems = dtFyleData.Select($"invoiceNum='{invoice}'");
                // get the invoice totals
                tax = 0;
                totalGross = 0;
                foreach (DataRow drLyneItem in drLineItems)
                {
                    tax += decimal.TryParse(drLyneItem["tax"].ToString().Trim(), out extendedPrice) ? extendedPrice : 0;
                    totalGross += decimal.TryParse(drLyneItem["totalGross"].ToString().Trim(), out extendedPrice) ? extendedPrice : 0;
                }

                // header rec to datatable
                dteAdded = drLineItems[0]["addDate"].ToString().Trim();
                dteDelivery = drLineItems[0]["delDate"].ToString().Trim();
                dteInvoice = drLineItems[0]["invDate"].ToString().Trim();

                dtInvoiceHeader.Rows.Add(
                    recIDnum
                    , "H"
                    , ImportSource
                    , drLineItems[0]["customer"].ToString().Trim()
                    , drLineItems[0]["ponum"].ToString().Trim()      //custPONum.Trim()
                    , "I"       //debitCredit
                    , FixDte(dteAdded)      //string.Format("{0:0000}{1:00}{2:00}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)        //dteAdded
                    , String.Empty      //createdBy
                    , invoice
                    , FixDte(dteDelivery)     //.ToString().Trim()       // dteDelivery
                    , FixDte(dteInvoice)     //.ToString().Trim()       // dteInvoice
                    , totalGross.ToString()
                    , tax.ToString()
                    , "0"               //invFreight
                    , string.Empty      //creditApplyToNum
                    , string.Empty      //lyneItem
                    , 0
                    , ImportSource
                    , 0
                    );

                // load detail line for invoice...
                int numRecs = 0;
                foreach (DataRow drLyneItem in drLineItems)
                {
                    numRecs++;

                    dtInvoiceDetail.Rows.Add(
                            recIDnum
                            , "D"
                            , drLyneItem["invoiceNum"].ToString().Trim()      //invoiceNum
                            , numRecs
                            , drLyneItem["customer"].ToString().Trim()      //  zfiCustNum.Trim()
                            , ImportSource
                            , drLyneItem["prodID"].ToString().Trim()      //dItemNum.Trim()
                            , drLyneItem["description"].ToString().Trim()      //dItemDesc.Trim()
                            , string.Empty
                            , MakeDecimal(drLyneItem["quantity"].ToString().Trim())
                            , "EA"                      //dUnitOfMeasure
                            , MakeDecimal(drLyneItem["price"].ToString().Trim())     //dPrice
                            , MakeDecimal(drLyneItem["tax"].ToString().Trim())     //dItemTax
                            , 0.0
                            , dCcustomDefinition.Trim()
                            , string.Empty      //dEndOfRecord
                            , 0
                            , ImportSource
                            , 0
                            );
                }
                dtInvoiceSummary.Rows.Add(
                            recIDnum
                            , "S"
                            , drLineItems[0]["invoiceNum"].ToString().Trim()      //invoiceNum
                            , drLineItems[0]["customer"].ToString().Trim()      //zfiCustNum.Trim()
                            , ImportSource
                            , numRecs.ToString()
                            , string.Empty      //sEndOfRecord
                            , 0
                            , ImportSource
                            );
            }
            DtHeader = dtInvoiceHeader;
            DtDetail = dtInvoiceDetail;
            DtSummary = dtInvoiceSummary;

            return rslt;
        }

        decimal MakeDecimal(string decIn)
        {
            decimal rslt = 0;
            try
            {
                string tmp = decIn.Replace("$", "");
                tmp = tmp.Replace(",", "");
                rslt = decimal.TryParse(tmp, out rslt) ? rslt : 0;
            }
            catch
            {
            }
            return rslt;
        }
        private string FixDte(object oDte)
        {
            string rslt = string.Empty;
            try
            {
                DateTime dte = DateTime.TryParse(oDte.ToString(), out dte) ? dte : DateTime.MinValue;
                rslt = string.Format("{1:00}/{2:00}/{0:0000}", dte.Year, dte.Month, dte.Day);
            }
            catch
            {
                rslt = "01/01/1900";
            }

            return rslt;
        }

        enum EcoItems
        {
            Custnum,
            Loc,
            Name,
            Unit,
            Add1,
            Add2,
            City,
            State,
            Zip,
            ServDate,
            InvNum,
            UnitCost,
            ItemTax,
            LineAmt,
            InvTot,
            ServName,
            PestCode,
            RfNum
        }
    }
}
