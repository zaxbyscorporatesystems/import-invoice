﻿using System;
using System.Data;
using System.IO;

namespace Import_Invoice
{
    class CodeCintas:IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        private enum FyleItems
        {
            Fld1
            ,Vendor
                ,LocNum
                ,InvoiceNum
                ,CustNum
                ,Date
                ,ItemNum
                ,ItemDescription
                ,Qty
                ,Fld5
                ,InvPrice
                ,LineAmount
                ,UsgCode
               ,InvAmt
                , EndofRecord
        };
            
        //private enum fyleItems
        //{
        //    Parent
        //    ,Customer
        //    ,Invoice
        //    ,Order
        //    ,Item	
        //    ,Description	
        //    ,Quantity	
        //    ,Price	
        //    ,ExtendedPrice	
        //    ,InvoiceDate	
        //    ,DeliverShipDate	
        //    ,ZFISupplierID
        //};

        // properties
        public string InputFile
        {
            set;
            get;
        }
        public string ImportSource
        {
            set;
            get;
        }
        public int NumberRecords
        {
            set;
            get;
        }

        public DataTable DtHeader
        {
            set;
            get;
        }
        public DataTable DtDetail
        {
            set;
            get;
        }
        public DataTable DtSummary
        {
            set;
            get;
        }

        // methods
        public string VerifyFile()
        {
            string rsltVerify = string.Empty,
                parent = string.Empty,
                customer = string.Empty,
                invoice = string.Empty,
                order = string.Empty,
                item = string.Empty,
                invoiceDate = string.Empty,
                transDate = string.Empty,
                description = string.Empty,
                lyneItem = string.Empty,
                //rejectFyle = @"C:\Apps\Import Invoice\Import Invoice\Rejects\",
                // header lines items-------------------------------------
                zfiCustNum = string.Empty,
                custPoNum = string.Empty,
                debitCredit = string.Empty,
                dteAdded = string.Empty,
                createdBy = string.Empty,
                invoiceNum = string.Empty,
                dteDelivery = string.Empty,
                dteInvoice = string.Empty,
                invTotal = string.Empty,
                invTax = string.Empty,
                invFreight = string.Empty,
                creditApplyToNum = string.Empty,
                // detail line items-------------------------------------- 
                dItemNum = string.Empty,
                dItemDesc = string.Empty,
                dQuantity = string.Empty,
                dUnitOfMeasure = string.Empty,
                dPrice = string.Empty,
                dItemTax = string.Empty,
                dOtherNumeric = string.Empty,
                dOptIdentifier = string.Empty,
                dCcustomDefinition = string.Empty,
                dEndOfRecord = string.Empty,
                // summary line items-------------------------------------
                sTotInvoiceLines = string.Empty,
                sOptionalInfo = string.Empty,
                sEndOfRecord = string.Empty;
            decimal quantity = 0,
                price = 0,
                extendedPrice = 0,
                tax = 0,
                totalGross = 0;



            DataTable dtFyleData = new DataTable();
            dtFyleData.Columns.Add("customer");
            dtFyleData.Columns.Add("invoice");
            dtFyleData.Columns.Add("order");
            dtFyleData.Columns.Add("item");
            dtFyleData.Columns.Add("description");
            dtFyleData.Columns.Add("quantity", typeof(decimal));
            dtFyleData.Columns.Add("price", typeof(decimal));
            dtFyleData.Columns.Add("extendedPrice", typeof(decimal));
            dtFyleData.Columns.Add("tax", typeof(decimal));
            dtFyleData.Columns.Add("totalGross", typeof(decimal));
            dtFyleData.Columns.Add("invoiceDate");
            dtFyleData.Columns.Add("transDate");


            DataTable dtInvoiceHeader = new DataTable();
            dtInvoiceHeader.Columns.Add("rec_id", typeof(int));
            dtInvoiceHeader.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceHeader.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("customer_po");                   // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("debit_or_credit");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("date_added");                    // CHAR(8),
            dtInvoiceHeader.Columns.Add("added_by");                      // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceHeader.Columns.Add("date_delivered");                // CHAR(8),
            dtInvoiceHeader.Columns.Add("date_invoice");                  // CHAR(8),
            dtInvoiceHeader.Columns.Add("invoice_amt");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_tax");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_freight");               // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("credit_apply_to_num");           // VARCHAR(8),
            dtInvoiceHeader.Columns.Add("record_end_char");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceHeader.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("status_flag", typeof(int));      // SMALLINT

            DataTable dtInvoiceDetail = new DataTable();
            dtInvoiceDetail.Columns.Add("rec_id", typeof(int));           // INT,
            dtInvoiceDetail.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceDetail.Columns.Add("sequence_id", typeof(int));      // INT,
            dtInvoiceDetail.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceDetail.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceDetail.Columns.Add("item_number");                   // VARCHAR(6),
            dtInvoiceDetail.Columns.Add("item_desc");                     // VARCHAR(80),
            dtInvoiceDetail.Columns.Add("account_code");                  // VARCHAR(32),
            dtInvoiceDetail.Columns.Add("quantity", typeof(decimal));     // DECIMAL(16,4),
            dtInvoiceDetail.Columns.Add("uom");                           // VARCHAR(2),
            dtInvoiceDetail.Columns.Add("price");                         // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax");                      // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax_percent");              // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("custom_definitions");            // VARCHAR(162),
            dtInvoiceDetail.Columns.Add("d_record_end_char");             // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceDetail.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceDetail.Columns.Add("status_flag", typeof(int));      // SMALLINT)

            DataTable dtInvoiceSummary = new DataTable();
            dtInvoiceSummary.Columns.Add("rec_id", typeof(int));          // INT,
            dtInvoiceSummary.Columns.Add("record_type");                  // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("invoice_num");                  // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("vend_customer_num");            // VARCHAR(15),
            dtInvoiceSummary.Columns.Add("vend_supplier_id");             // VARCHAR(5),
            dtInvoiceSummary.Columns.Add("total_invoice_lines");          // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("s_record_end_char");            // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("import_file_id", typeof(int));  // INT,
            dtInvoiceSummary.Columns.Add("import_source");                // VARCHAR(10))

            if (InputFile.Trim().Length > 0 && File.Exists(InputFile))
            {
                string usgCode = string.Empty;
                string[] fyleLynes = File.ReadAllLines(InputFile);
                int recId = 0,
                    numRecs = 0;
                //ImportSource = InputFile.Substring(InputFile.LastIndexOf("\\"));        //"CINTA"
                ImportSource = "CINTAS"; // ImportSource.Substring(1, 5).ToUpper();
                //bool pastFirst = false;
                foreach (string fLyne in fyleLynes)
                {
                    //                    string[] datas = fLyne.Split('\t');
                    string[] datas = fLyne.Split('\t');
                    if (datas[(int)FyleItems.LocNum].ToString().ToUpper().Trim() != "LOC. #")        //(pastFirst)
                    {
                        // usg code R,O,X????????
                        usgCode = datas[(int)FyleItems.UsgCode].Trim();
                        //parent = datas[(int)fyleItems.Fld1].Trim();
                        customer = datas[(int)FyleItems.CustNum].Trim();
                        if (customer.Length == 0)
                        {
                            break;
                        }
                        invoice = datas[(int)FyleItems.InvoiceNum].Trim();
                        order = string.Empty;       // datas[(int)fyleItems.Order].Trim();
                        item = datas[(int)FyleItems.ItemNum].Trim();
                        description = datas[(int)FyleItems.ItemDescription].Trim();
                        quantity = decimal.TryParse(datas[(int)FyleItems.Qty].Trim(), out quantity) ? quantity : 0;
                        price = MakeDecimal(datas[(int)FyleItems.InvPrice].Trim());
                        extendedPrice = MakeDecimal(datas[(int)FyleItems.LineAmount].Trim());
                        if (price == 0 && extendedPrice>0)
                        {
                            price = Math.Round(extendedPrice / quantity, 2);
                        }
                        if (extendedPrice == 0)
                        {
                            extendedPrice = Math.Round(quantity * price, 2);
                        }
                        tax = 0;        // decimal.TryParse(datas[(int)fyleItems.TaxTotal].Trim(), out tax) ? tax : 0;
                        totalGross = extendedPrice;     // decimal.TryParse(datas[(int)fyleItems.Gross].Trim(), out totalGross) ? totalGross : 0;
                        invoiceDate = datas[(int)FyleItems.Date].Trim();
                        string[] dtes = invoiceDate.Split('/');
                        invoiceDate = $"{dtes[2]}{dtes[0].PadLeft(2, '0')}{dtes[1].PadLeft(2, '0')}";
                        // Excel serial date to datetime
                        //int serialDate = int.Parse(invoiceDate);
                        //if (serialDate > 59)
                        //{
                        //    serialDate -= 1; //Excel/Lotus 2/29/1900 bug   
                        //}
                        //DateTime dte=new DateTime(1899, 12, 31).AddDays(serialDate);
                        //invoiceDate = string.Format("{2:0000}{0:00}{1:00}", dte.Month, dte.Day, dte.Year);
                        transDate = invoiceDate;        // datas[(int)fyleItems.TransDate].Trim();
                        dtFyleData.Rows.Add(
                            customer
                            , invoice
                            , order
                            , item
                            , description
                            , quantity
                            , price
                            , extendedPrice
                            , tax
                            , totalGross
                            , invoiceDate
                            , transDate
                            );
                    }

                    //pastFirst = true;
                }
                // get the invoice numbers
                DataTable dtInvoices = new DataTable();
                dtInvoices = dtFyleData.DefaultView.ToTable(true, "invoice");
                // for each invoice, create a header rec 
                foreach (DataRow drInvNum in dtInvoices.Rows)
                {
                    recId++;
                    invoice = drInvNum["invoice"].ToString().Trim();
                    DataRow[] drLineItems = dtFyleData.Select($"invoice='{invoice}'");
                    // get the invoice totals
                    tax = 0;
                    totalGross = 0;
                    foreach (DataRow drLyneItem in drLineItems)
                    {
                        tax += decimal.TryParse(drLyneItem["tax"].ToString().Trim(), out extendedPrice) ? extendedPrice : 0;
                        totalGross += decimal.TryParse(drLyneItem["totalGross"].ToString().Trim(), out extendedPrice) ? extendedPrice : 0;
                    }

                    // header rec to datatable
                    dteAdded = drLineItems[0]["transDate"].ToString().Trim();
                    dteDelivery = drLineItems[0]["transDate"].ToString().Trim();
                    dteInvoice = drLineItems[0]["invoiceDate"].ToString().Trim();

                    dtInvoiceHeader.Rows.Add(
                        recId
                        , "H"
                        , ImportSource
                        , drLineItems[0]["customer"].ToString().Trim()
                        , drLineItems[0]["order"].ToString().Trim()      //custPONum.Trim()
                        , "I"       //debitCredit
                        , dteAdded      //string.Format("{0:0000}{1:00}{2:00}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)        //dteAdded
                        , String.Empty      //createdBy
                        , invoice
                        , dteDelivery      //.ToString().Trim()       // dteDelivery
                        , dteInvoice     //.ToString().Trim()       // dteInvoice
                        , totalGross.ToString()
                        , tax.ToString()
                        , "0"               //invFreight
                        , string.Empty      //creditApplyToNum
                        , string.Empty      //lyneItem
                        , 0
                        , ImportSource
                        , 0
                        );

                    // load detail line for invoice...
                    numRecs = 0;
                    foreach (DataRow drLyneItem in drLineItems)
                    {
                        numRecs++;

                        dtInvoiceDetail.Rows.Add(
                                recId
                                , "D"
                                , drLyneItem["invoice"].ToString().Trim()      //invoiceNum
                                , numRecs
                                , drLyneItem["customer"].ToString().Trim()      //  zfiCustNum.Trim()
                                , ImportSource
                                , drLyneItem["item"].ToString().Trim()      //dItemNum.Trim()
                                , drLyneItem["description"].ToString().Trim()      //dItemDesc.Trim()
                                , "account_code"
                                , MakeDecimal(drLyneItem["quantity"].ToString().Trim())
                                , "EA"                      //dUnitOfMeasure
                                , MakeDecimal(drLyneItem["price"].ToString().Trim())     //dPrice
                                , MakeDecimal(drLyneItem["tax"].ToString().Trim())     //dItemTax
                                , 0.0
                                , dCcustomDefinition.Trim()
                                , string.Empty      //dEndOfRecord
                                , 0
                                , ImportSource
                                , 0
                                );
                    }
                    dtInvoiceSummary.Rows.Add(
                                recId
                                , "S"
                                , drLineItems[0]["invoice"].ToString().Trim()      //invoiceNum
                                , drLineItems[0]["customer"].ToString().Trim()      //zfiCustNum.Trim()
                                , ImportSource
                                , numRecs.ToString()
                                , string.Empty      //sEndOfRecord
                                , 0
                                , ImportSource
                                );
                }
            }

            DtHeader = dtInvoiceHeader;
            DtDetail = dtInvoiceDetail;
            DtSummary = dtInvoiceSummary;
            return rsltVerify;
        }

        string FrmtDateMdy(object oDtein)
        {
            string rslt = string.Empty,
                sTmp = string.Empty;
            try
            {
                sTmp = oDtein.ToString();
                rslt = $"{sTmp.Substring(4, 2):00}/{sTmp.Substring(6):00}/{sTmp.Substring(0, 4):0000}";
                if (rslt.Trim().Length < 8)
                {
                    rslt = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
        public string DteToEight(object oDte)
        {
            string rslt = string.Empty;
            try
            {
                rslt = oDte.ToString();
                if (rslt.Contains("/"))
                {
                    string[] dtes = rslt.Split('/');
                    rslt = dtes[2].PadLeft(4, '0') + dtes[0].PadLeft(2, '0') + dtes[1].PadLeft(2, '0');
                }
            }
            catch
            {
                rslt = string.Empty;
            }

            return rslt;
        }
        decimal MakeDecimal(string decIn)
        {
            decimal rslt = 0;
            try
            {
                string tmp = decIn.Replace("$", "");
                tmp = tmp.Replace(",", "");
                if (tmp != "-")
                {
                    rslt = decimal.TryParse(tmp, out rslt) ? rslt : 0;
                }
            }
            catch
            {
            }
            return rslt;
        }

    }
}
