﻿using System;
using System.Text;
using System.IO;
using System.Data;

namespace Import_Invoice
{
    class CodeIfh:IDisposable
    {
        //ifh only import
        int _curPos = 0,
            _numRecs=0;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        // properties-----------------------
        public string InputFile
        {
            set;
            get;
        }
        public string ImportSource
        {
            set;
            get;
        }
        public int NumberRecords
        {
            set;
            get;
        }

        public DataTable DtHeader
        {
            set;
            get;
        }
        public DataTable DtDetail
        {
            set;
            get;
        }
        public DataTable DtSummary
        {
            set;
            get;
        }
        // methods--------------------------
        public string VerifyFileFormat()
        {
            int lyneCount=0;
            string rslt = string.Empty,
                lyneItem = string.Empty,
                // header lines items
                zfiCustNum = string.Empty,
                custPoNum = string.Empty,
                debitCredit = string.Empty,
                dteAdded = string.Empty,
                createdBy = string.Empty,
                invoiceNum = string.Empty,
                dteDelivery = string.Empty,
                dteInvoice = string.Empty,
                invTotal = string.Empty,
                invTax = string.Empty,
                invFreight = string.Empty,
                creditApplyToNum = string.Empty,
                // detail line items 
                dItemNum = string.Empty,
                dItemDesc = string.Empty,
                dQuantity = string.Empty,
                dUnitOfMeasure = string.Empty,
                dPrice = string.Empty,
                dItemTax = string.Empty,
                dOtherNumeric = string.Empty,
                dOptIdentifier = string.Empty,
                dCcustomDefinition = string.Empty,
                dEndOfRecord = string.Empty,
                // summary line items
                sTotInvoiceLines = string.Empty,
                sOptionalInfo = string.Empty,
                sEndOfRecord = string.Empty,
                rejectFyle = string.Empty;

            DataTable dtInvoiceHeader = new DataTable();
            dtInvoiceHeader.Columns.Add("rec_id", typeof(int));
            dtInvoiceHeader.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceHeader.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("customer_po");                   // VARCHAR(15),
            dtInvoiceHeader.Columns.Add("debit_or_credit");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("date_added");                    // CHAR(8),
            dtInvoiceHeader.Columns.Add("added_by");                      // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceHeader.Columns.Add("date_delivered");                // CHAR(8),
            dtInvoiceHeader.Columns.Add("date_invoice");                  // CHAR(8),
            dtInvoiceHeader.Columns.Add("invoice_amt");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_tax");                   // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("invoice_freight");               // VARCHAR(11),
            dtInvoiceHeader.Columns.Add("credit_apply_to_num");           // VARCHAR(8),
            dtInvoiceHeader.Columns.Add("record_end_char");               // VARCHAR(1),
            dtInvoiceHeader.Columns.Add("import_file_id", typeof(int));    // INT,
            dtInvoiceHeader.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceHeader.Columns.Add("status_flag", typeof(int));       // SMALLINT

            DataTable dtInvoiceDetail = new DataTable();
            dtInvoiceDetail.Columns.Add("rec_id", typeof(int));            // INT,
            dtInvoiceDetail.Columns.Add("record_type");                   // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("invoice_num");                   // VARCHAR(7),
            dtInvoiceDetail.Columns.Add("sequence_id", typeof(int));       // INT,
            dtInvoiceDetail.Columns.Add("vend_customer_num");             // VARCHAR(15),
            dtInvoiceDetail.Columns.Add("vend_supplier_id");              // VARCHAR(5),
            dtInvoiceDetail.Columns.Add("item_number");                   // VARCHAR(6),
            dtInvoiceDetail.Columns.Add("item_desc");                     // VARCHAR(80),
            dtInvoiceDetail.Columns.Add("account_code");                  // VARCHAR(32),
            dtInvoiceDetail.Columns.Add("quantity", typeof(decimal));      // DECIMAL(16,4),
            dtInvoiceDetail.Columns.Add("uom");                           // VARCHAR(2),
            dtInvoiceDetail.Columns.Add("price");                         // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax");                      // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("item_tax_percent");              // VARCHAR(11),
            dtInvoiceDetail.Columns.Add("custom_definitions");            // VARCHAR(162),
            dtInvoiceDetail.Columns.Add("d_record_end_char");             // VARCHAR(1),
            dtInvoiceDetail.Columns.Add("import_file_id", typeof(int));    // INT,
            dtInvoiceDetail.Columns.Add("import_source");                 // VARCHAR(10),
            dtInvoiceDetail.Columns.Add("status_flag", typeof(int));       // SMALLINT)

            DataTable dtInvoiceSummary = new DataTable();
            dtInvoiceSummary.Columns.Add("rec_id", typeof(int));           // INT,
            dtInvoiceSummary.Columns.Add("record_type");                  // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("invoice_num");                  // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("vend_customer_num");            // VARCHAR(15),
            dtInvoiceSummary.Columns.Add("vend_supplier_id");             // VARCHAR(5),
            dtInvoiceSummary.Columns.Add("total_invoice_lines");          // VARCHAR(7),
            dtInvoiceSummary.Columns.Add("s_record_end_char");            // VARCHAR(1),
            dtInvoiceSummary.Columns.Add("import_file_id", typeof(int));   // INT,
            dtInvoiceSummary.Columns.Add("import_source");                // VARCHAR(10))

            if (InputFile.Trim().Length > 0 && File.Exists(InputFile))
            {
                ImportSource=InputFile.Substring(InputFile.LastIndexOf("\\") + 1);
                ImportSource = ImportSource.Substring(0, ImportSource.IndexOf('.'));
                rejectFyle = InputFile.Substring(0, InputFile.LastIndexOf("\\") + 1);
                rejectFyle = $"{rejectFyle}{ImportSource.Trim()}_errors.txt";
                ImportSource = "PFG_IFH";
                if (File.Exists(rejectFyle))
                {
                    File.Delete(rejectFyle);
                }
                string[] fyleLynes = File.ReadAllLines(InputFile);
                int recId = 0;
                foreach (string fLyne in fyleLynes)
                {
                    if (fLyne.ToUpper().StartsWith("H"))
                    {
                        lyneCount++;
                        //header
                        _numRecs = 0;
                        // check to be sure first header or summary just found????
                        if (fLyne.Trim().Length != 114)
                        {
                            rslt = "Errors found; check reject file.";
                            RecordToLog(rejectFyle,
                                $"Error in header line {fLyne} incorrect length {fLyne.Trim().Length}");
                        }
                        recId++;
                        _curPos = 1;
                        ImportSource = GetData(fLyne, 5);       //zfi supplier id code also import source
                        zfiCustNum = GetData(fLyne, 10);        //zfi customer num
                        custPoNum = GetData(fLyne, 15);          //customer po num
                        debitCredit = GetData(fLyne, 1);        // ??check for i=debit,c=credit
                        dteAdded = GetData(fLyne, 8);           // ??check for added date
                        createdBy = GetData(fLyne, 10);         //created by
                        invoiceNum = GetData(fLyne, 7);        //invoice num
                        dteDelivery = GetData(fLyne, 8);        // ??check for delivery date
                        dteInvoice = GetData(fLyne, 8);         // ??check for invoice date
                        invTotal = GetData(fLyne, 11);          //invoice total
                        invTax = GetData(fLyne, 11);            //invoice tax
                        invFreight = GetData(fLyne, 11);        //invoice freight
                        creditApplyToNum = GetData(fLyne, 7);  //credit apply to number
                        lyneItem = GetData(fLyne, 1);           //* end of record
                        dteDelivery = dteDelivery.Trim().Length == 0 ? dteInvoice : dteDelivery;
                        // header rec to datatable
                        dtInvoiceHeader.Rows.Add(
                            recId
                            , "H"
                            , ImportSource
                            , zfiCustNum.Trim()
                            , custPoNum.Trim()
                            , debitCredit
                            , dteAdded
                            , createdBy
                            , invoiceNum.Trim()
                            , dteDelivery
                            , dteInvoice
                            , invTotal
                            , invTax
                            , invFreight
                            , creditApplyToNum
                            , lyneItem
                            , 0
                            , ImportSource
                            , 0
                            );
                    }
                    else if (fLyne.ToUpper().StartsWith("D"))
                    {
                        lyneCount++;
                        //detail
                        // check to be sure header found????
                        if (fLyne.Trim().Length != 138 && fLyne.Trim().Length != 154 && fLyne.Trim().Length != 249)
                        {
                            rslt = "Errors found; check reject file.";
                            RecordToLog(rejectFyle,
                                $"Error in detail line {fLyne} incorrect length {fLyne.Trim().Length}");
                        }
                        _curPos = 1;
                        if (InputFile.ToUpper().Contains("MIL")
                           ||InputFile.ToUpper().Contains("FOODSTAR")
                           || InputFile.ToUpper().Contains("LESTER"))
                        {
                           dItemNum = GetData(fLyne, 6);              //item num
                           dItemDesc = GetData(fLyne, 80);             //item desc
                           dQuantity = GetData(fLyne, 11);             //quantity
                           dUnitOfMeasure = GetData(fLyne, 2);         //unit of measure
                           dPrice = GetData(fLyne, 11);                //price
                           dItemTax = GetData(fLyne, 11);              //item tax
                           dOtherNumeric = GetData(fLyne, 9);  //11);         //other numeric
                           dOptIdentifier = string.Empty;  // getData(fLyne, 30);        //optional identifier
                           dCcustomDefinition = string.Empty;  //getData(fLyne, 10);    //custom definition
                           dEndOfRecord = GetData(fLyne, 1);           //* end of record
                        }
                        else
                        {
                           dItemNum = GetData(fLyne, 10);              //item num
                           dItemDesc = GetData(fLyne, 80);             //item desc
                           dQuantity = GetData(fLyne, 11);             //quantity
                           dUnitOfMeasure = GetData(fLyne, 2);         //unit of measure
                           dPrice = GetData(fLyne, 11);                //price
                           dItemTax = GetData(fLyne, 11);              //item tax
                           dOtherNumeric = GetData(fLyne, 9);  //11);         //other numeric
                           dOptIdentifier = string.Empty;  // getData(fLyne, 30);        //optional identifier
                           dCcustomDefinition = string.Empty;  //getData(fLyne, 10);    //custom definition
                           dEndOfRecord = GetData(fLyne, 1);           //* end of record
                        }
                            _numRecs++;      // only detail lines are counted
                        // detail rec to datatable
                        dtInvoiceDetail.Rows.Add(
                            recId
                            , "D"
                            , invoiceNum.Trim()
                            ,_numRecs
                            , zfiCustNum.Trim()
                            , ImportSource
                            ,dItemNum.Trim()
                            ,dItemDesc.Trim()
                            ,string.Empty
                            , MakeDecimal(dQuantity)
                            ,dUnitOfMeasure
                            ,dPrice
                            ,dItemTax
                            , dOtherNumeric
                            ,dCcustomDefinition.Trim()
                            ,dEndOfRecord
                            , 0
                            , ImportSource
                            , 0
                            );
                    }
                    else if (fLyne.ToUpper().StartsWith("S"))
                    {
                        lyneCount++;
                        //summary
                        // check to be sure header & detail found????
                        if (fLyne.Trim().Length != 9)
                        {
                            rslt ="Errors found; check reject file.";
                            RecordToLog(rejectFyle,
                                $"Error in summary line {fLyne} incorrect length {fLyne.Trim().Length}");
                        }
                        _curPos = 1;
                        sTotInvoiceLines = GetData(fLyne, 7);       //total invoice lines
                        // check if detail line count matches
                        sOptionalInfo = string.Empty;  //getData(fLyne, 10);    //custom definition
                        sEndOfRecord = GetData(fLyne, 1);           //* end of record
                        dtInvoiceSummary.Rows.Add(
                            recId
                            , "S"
                            ,invoiceNum.Trim()
                            , zfiCustNum.Trim()
                            , ImportSource
                            , sTotInvoiceLines
                            ,sEndOfRecord
                            , 0
                            , ImportSource
                            );

                        //if (lyneItem != "*")
                        //{
                        //    rslt = "Errors found; check reject file.";
                        //    recordToLog(rejectFyle, string.Format("Error in summary line {0}", fLyne));

                        //}
                    }
                    else
                    {
                        //incorrect starting character
                    }
                }
            }
            NumberRecords = lyneCount;      // numRecs;
            DtHeader = dtInvoiceHeader;
            DtDetail = dtInvoiceDetail;
            DtSummary = dtInvoiceSummary;
            ImportSource = "PFG_IFH";
//            StringBuilder sbSql = new StringBuilder();
//            decimal invTot = 0,
//                detailTot=0,
//                dTemp=0,
//                dUnitPrice=0,
//                dQty=0;
//            int trxType=0;
//            string sCon = Properties.Settings.Default.conView,
//                invNum = string.Empty,
//                msg = string.Empty;
//            DataRow[] drDetails;
//            using (CodeSQL clsSql = new CodeSQL())
//            {
////                clsSql.CmdNonQuery(sCon ,"delete from [MatrixDev].[dbo].[imaphdr] where [company_code]='coCode'");
////                clsSql.CmdNonQuery(sCon, "delete from [MatrixDev].[dbo].[imapdtl] where [company_code]='coCode'");
//                foreach (DataRow dRow in dtInvoiceHeader.Rows)
//                {
//                    // check if header total matches detail
//                    invTot = makeDecimal(dRow["invoice_amt"].ToString()) - makeDecimal(dRow["invoice_tax"].ToString()) - makeDecimal(dRow["invoice_freight"].ToString());
////                    invTot = makeDecimal(dRow["invoice_amt"].ToString()) - makeDecimal(dRow["invoice_tax"].ToString());
//                    invNum = string.Format("invoice_num='{0}'", dRow["invoice_num"].ToString().Trim());
//                    drDetails = dtInvoiceDetail.Select(invNum);
//                    detailTot = 0;
//                    foreach (DataRow drDetail in drDetails)
//                    {
//                        dUnitPrice = decimal.TryParse(drDetail["price"].ToString().Trim(), out dTemp) ? dTemp : 0;
//                        dQty = decimal.TryParse(drDetail["quantity"].ToString().Trim(), out dTemp) ? dTemp : 0;
//                        detailTot += Math.Round(dUnitPrice * dQty, 2);
//                    }
//                    if (detailTot != invTot)
//                    {
//                        msg = string.Format("Invoice num {0} header total {1} does not match detail total {2}", dRow["invoice_num"], invTot, detailTot);
//                        recordToLog(rejectFyle, msg);
//                    }
//                    else
//                    {
//                        // insert in header table
//                        trxType = dRow["debit_or_credit"].ToString().ToUpper() == "C" ? 4092 : 4091;
//                        custPONum = dRow["customer_po"].ToString().Trim();
//                        sbSql.Clear();
//                        sbSql.Append("INSERT INTO [MatrixDev].[dbo].[imaphdr] (");
//                        sbSql.Append("[company_code]");
//                        sbSql.Append(",[process_ctrl_num]");
//                        sbSql.Append(",[source_trx_ctrl_num]");
//                        sbSql.Append(",[trx_ctrl_num]");
//                        sbSql.Append(",[trx_type]");
//                        sbSql.Append(",[doc_ctrl_num]");
//                        sbSql.Append(",[apply_to_num]");
//                        sbSql.Append(",[po_ctrl_num]");
//                        sbSql.Append(",[ticket_num]");
//                        //sbSql.Append(",[date_applied]");
//                        //sbSql.Append(",[date_aging]");
//                        //sbSql.Append(",[date_due]");
//                        sbSql.Append(",[date_doc]");
//                        sbSql.Append(",[date_received]");
//                        //sbSql.Append(",[date_required]");
//                        //sbSql.Append(",[date_discount]");
//                        sbSql.Append(",[posting_code]");
//                        sbSql.Append(",[vendor_code]");
//                        sbSql.Append(",[pay_to_code]");
//                        sbSql.Append(",[branch_code]");
//                        sbSql.Append(",[comment_code]");
//                        sbSql.Append(",[tax_code]");
//                        sbSql.Append(",[terms_code]");
//                        sbSql.Append(",[payment_code]");
//                        sbSql.Append(",[hold_flag]");
//                        sbSql.Append(",[doc_desc]");
//                        sbSql.Append(",[hold_desc]");
//                        sbSql.Append(",[intercompany_flag]");
//                        sbSql.Append(",[nat_cur_code]");
//                        sbSql.Append(",[rate_type_home]");
//                        sbSql.Append(",[rate_type_oper]");
//                        sbSql.Append(",[rate_home]");
//                        sbSql.Append(",[rate_oper]");
//                        sbSql.Append(",[pay_to_addr1]");
//                        sbSql.Append(",[pay_to_addr2]");
//                        sbSql.Append(",[pay_to_addr3]");
//                        sbSql.Append(",[pay_to_addr4]");
//                        sbSql.Append(",[pay_to_addr5]");
//                        sbSql.Append(",[pay_to_addr6]");
//                        sbSql.Append(",[attention_name]");
//                        sbSql.Append(",[attention_phone]");
//                        sbSql.Append(",[approval_code]");
//                        sbSql.Append(",[approval_flag]");
//                        //sbSql.Append(",[add_cost_flag]");
//                        sbSql.Append(",[amt_freight]");
//                        sbSql.Append(",[amt_misc]");
//                        //sbSql.Append(",[amt_paid]");
//                        //sbSql.Append(",[amt_restock]");
//                        sbSql.Append(",[amt_tax_included]");
//                        //sbSql.Append(",[class_code]");
//                        //sbSql.Append(",[cms_flag]");
//                        sbSql.Append(",[date_entered]");
//                        //sbSql.Append(",[date_recurring]");
//                        //sbSql.Append(",[drop_ship_flag]");
//                        //sbSql.Append(",[fob_code]");
//                        //sbSql.Append(",[frt_calc_tax]");
//                        //sbSql.Append(",[location_code]");
//                        //sbSql.Append(",[one_check_flag]");
//                        //sbSql.Append(",[recurring_code]");
//                        //sbSql.Append(",[recurring_flag]");
//                        //sbSql.Append(",[times_accrued]");
//                        sbSql.Append(",[user_trx_type_code]");
//                        //sbSql.Append(",[vend_order_num]");
//                        //sbSql.Append(",[date_processed]");
//                        sbSql.Append(",[processed_flag]");
//                        //sbSql.Append(",[Import Identifier]");
//                        //sbSql.Append(",[batch_no]");
//                        //sbSql.Append(",[User_ID]");

//                        sbSql.Append(")VALUES(");
//                        sbSql.Append(string.Format("'{0}'", "coCode")); //      <company_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty)); //      ,<process_ctrl_num, varchar(16),>
//                        sbSql.Append(string.Format(",'{0}'", dRow["invoice_num"].ToString().Trim())); //      ,<source_trx_ctrl_num, varchar(16),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty)); //      ,<trx_ctrl_num, varchar(16),>
//                        sbSql.Append(string.Format(",{0}", trxType)); //      ,<trx_type, smallint,>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<doc_ctrl_num, varchar(16),>
//                        sbSql.Append(string.Format(",'{0}'", dRow["debit_or_credit"].ToString().ToUpper() == "D" ? dRow["credit_apply_to_num"].ToString().Trim() : string.Empty));   //      ,<apply_to_num, varchar(16),>
//                        sbSql.Append(string.Format(",'{0}'", dRow["customer_po"])); //      ,<po_ctrl_num, varchar(16),>
//                        sbSql.Append(string.Format(",'{0}'",  "ticket_num")); //      ,<ticket_num, varchar(20),>
//                        //      ,<date_applied, varchar(10),>
//                        //      ,<date_aging, varchar(10),>
//                        //      ,<date_due, varchar(10),>
//                        sbSql.Append(string.Format(",'{0}'", frmtDateMDY(dRow["date_invoice"])));//      ,<date_doc, varchar(10),>
//                        sbSql.Append(string.Format(",'{0}'", frmtDateMDY(dRow["date_delivered"])));//      ,<date_received, varchar(10),>
//                        //      ,<date_required, varchar(10),>
//                        //      ,<date_discount, varchar(10),>
//                        sbSql.Append(string.Format(",'{0}'", "postCode"));//      ,<posting_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", dRow["vend_supplier_id"].ToString().Trim())); //      ,<vendor_code, varchar(12),>
//                        sbSql.Append(string.Format(",'{0}'", "pay_to"));//      ,<pay_to_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", dRow["vend_customer_num"].ToString().Trim()));//      ,<branch_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<comment_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<tax_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<terms_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<payment_code, varchar(8),>
                        
//                        sbSql.Append(",1"); //      ,<hold_flag, smallint,>
                        
//                        sbSql.Append(string.Format(",'{3} Electronic Invoice {0}/{1}/{2}'", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, dRow["vend_supplier_id"].ToString().Trim()));//      ,<doc_desc, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<hold_desc, varchar(40),>
//                        sbSql.Append(",0"); //      ,<intercompany_flag, smallint,>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<nat_cur_code, varchar(8),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<rate_type_home, varchar(8),>
//                        sbSql.Append(",1"); //      ,<rate_type_oper, varchar(8),>
//                        sbSql.Append(",1"); //      ,<rate_home, float,>
//                        sbSql.Append(",1"); //      ,<rate_oper, float,>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<pay_to_addr1, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<pay_to_addr2, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<pay_to_addr3, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<pay_to_addr4, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<pay_to_addr5, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<pay_to_addr6, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<attention_name, varchar(40),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<attention_phone, varchar(30),>
//                        sbSql.Append(string.Format(",'{0}'", string.Empty));//      ,<approval_code, varchar(8),>
//                        sbSql.Append(",0");     //      ,<approval_flag, smallint,>
//                        //      ,<add_cost_flag, smallint,>
//                        sbSql.Append(string.Format(",{0}", checkNumber(dRow["invoice_freight"]).ToString().Trim()));   //      ,<amt_freight, float,>
//                        sbSql.Append(string.Format(",{0}", invTot));   //      ,<amt_misc, float,>
//                        //      ,<amt_paid, float,>
//                        //      ,<amt_restock, float,>
//                        sbSql.Append(string.Format(",{0}", checkNumber(dRow["invoice_amt"]).ToString().Trim()));        //      ,<amt_tax_included, float,>
//                        //      ,<class_code, varchar(8),>
//                        //      ,<cms_flag, smallint,>
//                        sbSql.Append(string.Format(",'{0}'", DateTime.Now.ToShortDateString()));     //      ,<date_entered, varchar(10),>
//                        //      ,<date_recurring, varchar(10),>
//                        //      ,<drop_ship_flag, smallint,>
//                        //      ,<fob_code, varchar(8),>
//                        //      ,<frt_calc_tax, float,>
//                        //      ,<location_code, varchar(8),>
//                        //      ,<one_check_flag, smallint,>
//                        //      ,<recurring_code, varchar(8),>
//                        //      ,<recurring_flag, smallint,>
//                        //      ,<times_accrued, smallint,>
//                        sbSql.Append(string.Format(",'{0}'", dRow["debit_or_credit"].ToString().ToUpper() == "C" ? "DBMEMO" : "VOUCH")); //      ,<user_trx_type_code, varchar(8),>
//                        //      ,<vend_order_num, varchar(20),>
//                        //sbSql.Append(string.Format(",'{0}'", DateTime.Now));//      ,<date_processed, datetime,>
//                        sbSql.Append(string.Format(",{0}", 0));//      ,<processed_flag, int,>
//                        //      ,<Import Identifier, int,>
//                        //      ,<batch_no, int,>
//                        //      ,<User_ID, int,>
//                        sbSql.Append(")");
//                        clsSql.CmdNonQuery(sCon, sbSql.ToString());

//                        // insert in detail table
//                        foreach (DataRow dtRow in drDetails)
//                        {
//                            //trxType = clsSql.CmdScalar(sCon, string.Format("select [trx_type] from [MatrixDev].[dbo].[imaphdr] where [source_trx_ctrl_num]='{0}'", dtRow["invoice_num"].ToString().Trim())).ToString();
//                            sbSql.Clear();
//                            sbSql.Append("INSERT INTO [MatrixDev].[dbo].[imapdtl](");
//                            sbSql.Append("[company_code]");
//                            sbSql.Append(",[process_ctrl_num]");
//                            sbSql.Append(",[source_trx_ctrl_num]");
//                            sbSql.Append(",[trx_ctrl_num]");
//                            sbSql.Append(",[trx_type]");
//                            sbSql.Append(",[sequence_id]");
//                            //           ,[location_code]
//                            sbSql.Append(",[item_code]");
//                            sbSql.Append(",[qty_ordered]");
//                            //           ,[qty_received]
//                            //           ,[qty_returned]
//                            //           ,[tax_code]
//                            //           ,[return_code]
//                            //           ,[code_1099]
//                            //           ,[po_ctrl_num]
//                            sbSql.Append(",[unit_code]");
//                            sbSql.Append(",[unit_price]");
//                            //           ,[amt_discount]
//                            sbSql.Append(",[amt_tax]");
//                            //           ,[gl_exp_acct]
//                            sbSql.Append(",[line_desc]");
//                            //           ,[reference_code]
//                            //           ,[rec_company_code]
//                            //           ,[approval_code]
//                            //           ,[amt_freight]
//                            //           ,[amt_misc]
//                            //           ,[bulk_flag]
//                            //           ,[calc_tax]
//                            //           ,[date_entered]
//                            //           ,[new_gl_exp_acct]
//                            //           ,[new_reference_code]
//                            //           ,[po_orig_flag]
//                            //           ,[qty_prev_returned]
//                            //           ,[rma_num]
//                            //           ,[processed_flag]
//                            //           ,[Import Identifier]
//                            //           ,[batch_no]
//                            //           ,[User_ID]
//                            sbSql.Append(")VALUES(");
//                            sbSql.Append("'coCode'");//          <company_code, varchar(8),>
//                            sbSql.Append(string.Format(",'{0}'", string.Empty)); //           ,<process_ctrl_num, varchar(16),>
//                            sbSql.Append(string.Format(",'{0}'", dtRow["invoice_num"].ToString().Trim()));//           ,<source_trx_ctrl_num, varchar(16),>
//                            sbSql.Append(string.Format(",'{0}'", string.Empty)); //           ,<trx_ctrl_num, varchar(16),>
//                            sbSql.Append(string.Format(",{0}", trxType)); //           ,<trx_type, smallint,>
//                            sbSql.Append(string.Format(",{0}", dtRow["sequence_id"]));//           ,<sequence_id, int,>
//                            //           ,<location_code, varchar(8),>
//                            sbSql.Append(string.Format(",'{0}'", dtRow["item_number"].ToString().Trim()));//           ,<item_code, varchar(30),>
//                            sbSql.Append(string.Format(",{0}", checkNumber(dtRow["quantity"])));//           ,<qty_ordered, float,>
//                            //           ,<qty_received, float,>
//                            //           ,<qty_returned, float,>
//                            //           ,<tax_code, varchar(8),>
//                            //           ,<return_code, varchar(8),>
//                            //           ,<code_1099, varchar(8),>
//                            //           ,<po_ctrl_num, varchar(16),>
//                            sbSql.Append(string.Format(",'{0}'", dtRow["uom"]));//           ,<unit_code, varchar(8),>
//                            sbSql.Append(string.Format(",{0}", checkNumber(dtRow["price"])));//           ,<unit_price, float,>
//                            //           ,<amt_discount, float,>
//                            sbSql.Append(string.Format(",{0}", checkNumber(dtRow["item_tax"])));//           ,<amt_tax, float,>
//                            //           ,<gl_exp_acct, varchar(32),>
//                            sbSql.Append(string.Format(",'{0}'", dtRow["item_desc"].ToString().Trim()));//           ,<line_desc, varchar(60),>
//                            //           ,<reference_code, varchar(32),>
//                            //           ,<rec_company_code, varchar(8),>
//                            //           ,<approval_code, varchar(8),>
//                            //           ,<amt_freight, float,>
//                            //           ,<amt_misc, float,>
//                            //           ,<bulk_flag, smallint,>
//                            //           ,<calc_tax, float,>
//                            //           ,<date_entered, varchar(10),>
//                            //           ,<new_gl_exp_acct, varchar(32),>
//                            //           ,<new_reference_code, varchar(32),>
//                            //           ,<po_orig_flag, smallint,>
//                            //           ,<qty_prev_returned, float,>
//                            //           ,<rma_num, varchar(20),>
//                            //           ,<processed_flag, int,>
//                            //           ,<Import Identifier, int,>
//                            //           ,<batch_no, int,>
//                            //           ,<User_ID, int,>
//                            sbSql.Append(")");
//                            clsSql.CmdNonQuery(sCon, sbSql.ToString());
//                        }
//                    }
//                }
//            }
            return rslt;
        }

        string GetData(string sLyne,int length)
        {
            string sData = string.Empty;
            try
            {
                sData = sLyne.Substring(_curPos, length);
                sData = sData.Replace("'", "");
                _curPos += length;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            // remove leading zeros
            // .trim() in case of leading space(s)
            sData = sData.Trim().TrimStart('0');    //.PadLeft(length, ' ');
            
            return sData;
        }
        decimal MakeDecimal(string decIn)
        {
            decimal rslt = 0;
            try
            {
                string tmp = decIn.Replace("$", "");
                tmp = tmp.Replace(",", "");
                rslt = decimal.TryParse(tmp, out rslt) ? rslt : 0;
            }
            catch
            {
            }
            return rslt;
        }
        string FrmtDateMdy(object oDtein)
        {
            string rslt = string.Empty,
                sTmp=string.Empty;
            try
            {
                sTmp = oDtein.ToString();
                rslt = $"{sTmp.Substring(4, 2):00}/{sTmp.Substring(6):00}/{sTmp.Substring(0, 4):0000}";
                if (rslt.Trim().Length < 8)
                {
                    rslt = string.Empty;
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
        private void RecordToLog(string fyle, string strToWryte)
        {
            FileStream fStream = new FileStream(fyle, FileMode.Append);
            StreamWriter twfPath = new StreamWriter(fStream);
            twfPath.WriteLine(strToWryte);
            twfPath.Close();
            fStream.Close();
        }

        object CheckNumber(object objIn)
        {
            object rslt = objIn.ToString().Trim().Length == 0 ? 0 : objIn;
            return rslt;
        }

        public string MakeOldLineFormat(string lyneIn)
        {
            string rsltOut = string.Empty,
                dteDelivery=string.Empty,
                dteInvoice=string.Empty;
            StringBuilder sbNewLyne = new StringBuilder();
            lyneIn = lyneIn.Replace("'", " ");
            if (lyneIn.ToUpper().StartsWith("H"))
            {
                //header
                _curPos = 1;
                sbNewLyne.Append("H");
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 5, 5));     // ImportSource = getData(lyneIn, 5);       //zfi supplier id code also import source
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 20, 20));     // zfiCustNum = getData(lyneIn, 20);        //zfi customer num
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 5, 5));     // custPONum = getData(lyneIn, 5);          //customer po num
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 1, 1));     // debitCredit = getData(lyneIn, 1);        // ??check for i=debit,c=credit
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 8, 8));     // dteAdded = getData(lyneIn, 8);           // ??check for added date
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 10, 10));     // createdBy = getData(lyneIn, 10);         //created by
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 20, 20));     // invoiceNum = getData(lyneIn, 20);        //invoice num
                dteDelivery = GetOldData(lyneIn,false,' ',8, 8);        // ??check for delivery date
                dteInvoice = GetOldData(lyneIn, false, ' ', 8, 8);         // ??check for invoice date
                dteDelivery = dteDelivery.Trim().Length == 0 ? dteInvoice : dteDelivery;
                sbNewLyne.Append(dteDelivery);
                sbNewLyne.Append(dteInvoice);
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // invTotal = getData(lyneIn, 11);          //invoice total
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // invTax = getData(lyneIn, 11);            //invoice tax
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // invFreight = getData(lyneIn, 11);        //invoice freight
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 20, 20));     // creditApplyToNum = getData(lyneIn, 20);  //credit apply to number
                sbNewLyne.Append("*");     // lyneItem = getData(lyneIn, 1);           //* end of record
            }
            else if (lyneIn.ToUpper().StartsWith("D"))
            {
                //detail
                // check to be sure header found????
                _curPos = 1;
                sbNewLyne.Append("D");
                sbNewLyne.Append(GetOldData(lyneIn, true, ' ', 30, 30));     // dItemNum = getData(lyneIn, 30);              //item num
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 80, 80));     // dItemDesc = getData(lyneIn, 80);             //item desc
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dQuantity = getData(lyneIn, 11);             //quantity
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 2, 2));     // dUnitOfMeasure = getData(lyneIn, 2);         //unit of measure
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dPrice = getData(lyneIn, 11);                //price
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dItemTax = getData(lyneIn, 11);              //item tax
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 11, 11));     // dOtherNumeric = getData(lyneIn, 11);         //other numeric
                //sbNewLyne.Append(getOldData(lyneIn, true, '0', 30, 7));     // dOptIdentifier = getData(lyneIn, 30);        //optional identifier
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 40, 40));     // dCcustomDefinition = getData(lyneIn, 10);    //custom definition
                sbNewLyne.Append("*");     // dEndOfRecord = getData(lyneIn, 1);           //* end of record
            }
            else if (lyneIn.ToUpper().StartsWith("S"))
            {
                //summary
                _curPos = 1;
                sbNewLyne.Append("S");
                sbNewLyne.Append(GetOldData(lyneIn, true, '0', 7, 7));     // getData(lyneIn, 7);       //total invoice lines
                sbNewLyne.Append(GetOldData(lyneIn, false, ' ', 10, 10));     // gsOptionalInfo = getData(lyneIn, 10);    //custom definition
                sbNewLyne.Append("*");     // sEndOfRecord = getData(lyneIn, 1);           //* end of record
            }
            else
            {
                //incorrect starting character
            }
            rsltOut = sbNewLyne.ToString();
            return rsltOut;
        }

        string GetOldData(string sLyne,bool justRt,char filChar, int newLen, int oldLen)
        {
            string sData = string.Empty;
            try
            {
                sData = sLyne.Substring(_curPos, newLen);
                sData = sData.Replace("'", " ");
                _curPos += newLen;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            // remove leading zeros
            // .trim() in case of leading space(s)
            sData = sData.Trim().TrimStart('0');    //.PadLeft(length, ' ');
            if (sData.Length > oldLen)
            {
                sData = sData.Substring(0, oldLen);
            }
            else
            {
                sData = justRt ? sData.PadLeft(oldLen, filChar) : sData.PadRight(oldLen, filChar);
            }
            return sData;
        }


    }
}
